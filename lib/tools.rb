# encoding: utf-8

	def save_cpoint_meter( meter, value, date )
		saver = CPointSaver.new
		saver.store_meter( meter, value, date )
	end


	def filter_by_date interval_kind, vals

		res_vals = 
		case interval_kind 

		when INTERVAL_ALL_VALUES then vals

		when INTERVAL_DAY_VALUES then 

			first_dates = {}
			vals.sort{|a,b| a[:datetime] <=> b[:datetime] }.each do |v|
				first_dates[ v[:datetime] ] ||= v[:value]
			end
		
			first_dates.map{|f| {:datetime => f[0], :value => f[1]}} 

		when INTERVAL_HOUR_VALUES then vals

		when INTERVAL_HALF_HOUR_VALUES then vals

		end
	end


	def change_date_range_for_period_kind

			case @period_kind 
			when PERIOD_ANY then # do nothing
			when PERIOD_CURRENT_DAY then 
				@date_start = DateTime.now.to_date
				@date_end = DateTime.now.to_date + 1
			when PERIOD_PREVIOUS_DAY then
				@date_start = DateTime.now.to_date - 1
				@date_end = DateTime.now.to_date
			when PERIOD_CURRENT_MONTH then
				@date_start = Date.new( DateTime.now.year, DateTime.now.month, 1 ) 
				@date_end = Date.new( DateTime.now.year, DateTime.now.month, -1 )
			when PERIOD_PREVIOUS_MONTH then
				@date_start = Date.new( DateTime.now.year, DateTime.now.month - 1, 1 )
				@date_end = Date.new( DateTime.now.year, DateTime.now.month - 1, -1 )
			end
	end

	def interval_title( interval_kind )
		@fm_data_rcv.interval_caption( interval_kind )
	end


	#
	#	== Load the status of bam meters
	#
	def load_bam_meters_status
		bam_meters_h = load_cached_flowmeters(PROJ_KAITPAS,DateTime.now-1, DateTime.now)

		@bam_meters = {}
		@bam_meters = Hash[*bam_meters_h.map{|m| [m.address,m]}.flatten!]  unless bam_meters_h.nil?

		@bam_meters.default = NullFlowMeter.instance 
	rescue 
		$logger.error $!.message
		$logger.ap $@[0..4]
	end

	#
	#	== Load the status of notrar meters
	#
	def load_notrar_meters_status
		@notrar_meters = Hash[*load_cached_flowmeters(PROJ_KAITPAS,DateTime.now-1, DateTime.now).map{|m| [m.address,m]}.flatten!] 
		@notrar_meters.default = NullFlowMeter.instance 
	rescue 
		$logger.error $!.message
		$logger.ap $@[0..4]
	end
	#todo: commonize meters status receival code


	#
	# == Load the status of taraz meters
	#
	def load_taraz_meters_status

		meters = load_cached_flowmeters(PROJ_KAITPAS,DateTime.now-1, DateTime.now)
		meters_hash = Hash[*meters.map{|m| [m.address,m]}.flatten!]

		@taraz_meters = meters_hash 
		@taraz_meters.default = NullFlowMeter.instance 
	rescue 
		$logger.error $!.message
		$logger.ap $@[0..4]
	end
	#
	# == Load the status of kaitpas meters
	#
	def load_kaitpas_meters_status

		meters = load_cached_flowmeters(PROJ_KAITPAS,DateTime.now-1, DateTime.now)

		if (meters.class == Hash)
			meters_hash = Hash[*meters.map{|m| [m.address,m]}.flatten!]

			@kaitpas_meters = meters_hash 
			$logger.error "Received nil meters from kipq4s db" if meters_hash.nil?
		end

		@kaitpas_meters = {} if @kaitpas_meters.nil?

		@kaitpas_meters.default = NullFlowMeter.instance 

    
	rescue 
		$logger.error $!.message
		$logger.ap $@[0..4]
	end

	# == Load the ergomera meters from db
	#
	def load_ergomera_meters
		@base_fm = @cp_data_rcv.read_energomera_meters_table() 
		ap @base_fm.inspect
	rescue 
		ap $!.message
		ap $@[0..6]

		$logger.error $!.message
		$logger.ap $@[0..14]
	end

	# == Load the base meters from db
	#
	def load_base_meters
		@base_fm = @cp_data_rcv.read_flow_meters_table( PROJ_METERS )

	rescue 
		$logger.error $!.message
		$logger.ap $@[0..4]
	end

	# == Get cached flowmeters for project
	#
	def get_cached_flowmeters( proj_id )
		load_cached_flowmeters( proj_id, get_parms_date_start, get_parms_date_end )
	rescue 
		$logger.error $!.message
		$logger.ap $@[0..4]
	end


	# == Load cached flowmeter for date range and project
	#
	def load_cached_flowmeters( proj_id, date_start, date_end )

		@fm_data_rcv.date_start = date_start
		@fm_data_rcv.date_end = date_end 

		@meter_data = []

		@cached_main_fm = @fm_data_rcv.load_cached_flowmeters( proj_id, "%d-%m-%Y %H:%M")

		@cached_main_fm.each_with_index do |meter, i|

			@meter_data << meter
		end

		@meter_data
	rescue 
		$logger.error $!.message
		$logger.ap $@[0..4]
	end

	# == Calculate consumption for flowmeters
	#
	def calc_consumption( fmeters )
		# get meter vals
		vals = @rep_main_fm.map{|m| 
			{datetime: m.values[0].datetime, 
    value: m.values[0].value}}

		#calculate consumption
		tvals = []
		vals.each_with_index do|v,i| 
			tv = {}
			tv[:datetime] = v[:datetime]
			tv[:value] = v[:value] - vals[i-1][:value] if i > 0
			next if i == 0

			tvals << tv
		end

		tvals
	rescue 
		$logger.error $!.message
		$logger.ap $@[0..4]
	end

	# == Receive date start from params
	#
	def get_parms_date_start
		DateTime.new(params[:ev][:start][:y].to_i, 
			 params[:ev][:start][:m].to_i, 
			 params[:ev][:start][:d].to_i,
			 params[:ev][:start][:h].to_i,
			 params[:ev][:start][:i].to_i
		)

	rescue
		return DateTime.now.to_date
		$logger.ap $@[0..4]
	end

	# == Receive date end from params
	#
	def get_parms_date_end
		DateTime.new(params[:ev][:end][:y].to_i, 
			 params[:ev][:end][:m].to_i, 
			 params[:ev][:end][:d].to_i,
			 params[:ev][:end][:h].to_i,
			 params[:ev][:end][:i].to_i
			 )

	rescue => error
		return DateTime.now.to_date
		$logger.ap $@[0..4]
	end

	CURRENT_LEVEL_COMMAND = 1
	OVERFLOW_STATE_COMMAND = 2
	EMPTY_STATE_COMMAND = 3

	# == Read last tank params
	# Find param by tank code and command, sorted by date (last one)
	# Return: result in format {too_aged: true} - if more than 10 minutes of reading, {too_aged: false, value: 1} - if has value, 
	#		for alarms: 1 - set, for level - integer value of level
	def read_last_tank_param( code, command )

		case command 
			when CURRENT_LEVEL_COMMAND
				read_last_current_level( code )
			when OVERFLOW_STATE_COMMAND
				read_last_overflow_alarm( code )
			when EMPTY_STATE_COMMAND
				read_last_empty_alarm( code )
		end
	end

	NORMAL = 0
	STALE = 1
	INVALID = 2

	# == Read current level in last 10 minutes and return it
	# Info: if level is received more than 10 minutes, return it with flag
	#		STALE, else NORMAL
	#	Returns: [level_value, flag, date_query]
	#		where level_value - float value of level (1-6 position)
	#					flag - NORMAL (if in 10 minutes), STALE if later
	#					date_query - date of last level storage
	#
	def read_last_current_level( code )

		db = connect_to_tanks_db

		vals = db[:tank_states].where(state_command_id: CURRENT_LEVEL_COMMAND).order(:date_stored).reverse.first
		db.disconnect

		puts "Read last current level for #{code}"
		ap vals

		if( vals == nil )
			return [-1, INVALID, DateTime.now]
	  end

		if( TimeDifference.between(DateTime.now, vals[:date_stored] ).in_minutes > 5 )
						return [vals[:value_int], STALE, vals[:date_stored]]
		else
						return [vals[:value_int], NORMAL, vals[:date_stored]]
		end
	end


	# == Read last overflow alarm
	# Info: if overflow alarm is set in 5 minutes, then return it else return false
	# Returns: true - alarm set, false - no alarm
	def read_last_overflow_alarm( code )
	end

	# == Read last empty alarm
	# Info: if empty alarm is set in 5 minutes, then return true
	# Returns: true - alarm is set, false - no alarm
	def read_last_empty_alarm( code )
	end

	# == Save tank params
	#  format: params = {code:1, command:1, value:4}
	#  where
	#		code - of tank
	#		command - (1) level of tank, (2) high level (overflow), (3) low level (empty tank)
	def save_tank_params( params )
		tank_db = connect_to_tanks_db()

		val =  params.keys[0]
		parmhash = val.gsub(/[{}]/,'').split(', ').map{|h| h1,h2 = h.split(':'); {h1 => h2}}.reduce(:merge)
		ap parmhash

		code = parmhash["code"].to_i
		command = parmhash["command"].to_i
		val_int = parmhash["value"].to_i

		ap " Vals: code #{code} : command #{command} : value #{val_int}"


		tank_db[:tank_states].insert( tank_id: code, date_stored: DateTime.now, state_command_id: command, value_int: val_int )

		tank_db.disconnect
	end


	# == Connect to tanks on cpoint
	#
	def connect_to_tanks_db
		Sequel.tinytds(
			user: 'puser', password: 'Win3Ts', host: '192.168.10.8', database: 'tanks', encoding: 'cp1251' 
			)
	end



	# == Load the settings from yaml config
	#
	def load_cfg
		@cfg = YAML.load_file("#{File.dirname(__FILE__)}/cfg/ms.cfg.yaml")
	end


	def read_modes_from_service

		uri = URI.parse('http://192.168.10.8:3000/last/cpmodes.json')
		http = Net::HTTP.new( uri.host, uri.port )
		request = Net::HTTP::Get.new(uri.request_uri)

		http.request( request )
	end
