# encoding: utf-8
require 'sinatra'
require 'time_difference'
require 'date'
require 'logger'
require 'ap'
require_relative 'strict_tsv'


$logger = Logger.new ("db#{DateTime.now.strftime('%Y%m%d%H%M')}.log")


before do
	headers "Content-Type" => "text/html; charset=utf-8"
end

use Rack::Static, :urls => %w[/static/scripts/dropbox.js /static/styles/dropbox.css /static/styles/theme.css]

set :bind, '192.168.10.8'
set :port, 9011

set :static, true
set :root, File.dirname(__FILE__)

	configure do
		set :public_folder, File.dirname(__FILE__) + '/static' 
	end

get '/' do
  erb :dropbox
end 

def fix_str( str )
	str.chars.map do |c|
		if( c.valid_encoding?  ) 
			c
		else
			puts "Invalid bytes: #{c.bytes.count} :: #{c.bytes.to_a[0]}"
			''
		end
	end.join
end

post '/drop' do
begin
	file_name = ".\\tmp\\upload-#{params[:filename]}"
  File.open(file_name, 'wb:UTF-16LE') do |f|
    f.binmode
    while buffer = request.body.read(51200) # read in 50 kB chunks
			@buf = buffer

      f << (@buf)
		end
  end

	text = File.open(file_name,"rb:UTF-16LE"){|file| file.readlines}.join.encode("utf-8")

	@empty_meters = []
	tsv = StrictTsv.new('')
	tsv.parse_str( text )do |row|
		@empty_meters << row
	end

	#@lost_meters.select!{|v|  TimeDifference.between(DateTime.parse(v["Время считывания"]), DateTime.now).in_days > 2 unless v["Время считывания"].nil?; false } 

	[200,{'Content-Type' => 'text/html', 'Accept-Charset' => 'utf-8'}, erb( :empty_cell_meters)]
	rescue Exception => ex
		puts "Error:: #{ex}"
		puts $@[0..6]
		$logger.error ex
		[500, {'Content-Type' => 'text/plain'}, "Error:: #{ex}"]
	end

end


def decode_rus( value )
	return value
		$ecr1 ||= Encoding::Converter.new "UTF-8","CP1251",:invalid=>:replace,:undef=>:replace,:replace=>""
		$ecr2 ||= Encoding::Converter.new "CP1251","UTF-8",:invalid=>:replace,:undef=>:replace,:replace=>""

		$ecr1.convert $ecr2.convert value
end

__END__

@@ layout
<!doctype html>
<html>
  <head>
    <title>Dropbox</title>
    <link href="/styles/dropbox.css" media="screen" rel="stylesheet" type="text/css" />
    <link href="/styles/theme.css" media="screen" rel="stylesheet" type="text/css" />
    <script src="/scripts/dropbox.js" type="text/javascript"></script>
  </head>
  <body>
<%= yield %>
  </body>
</html>

@@ dropbox
<div id="dropbox">Drag and drop files, or click here...</div>
<div id="log"></div>

<script>
  var box = document.getElementById('dropbox');
  var log = document.getElementById('log');

  var dropbox = new Dropbox(box, '/drop');
  // Optional settings and callbacks
  dropbox.max_size = 10; // MB
  dropbox.max_concurrent = 2; // Do not upload more than two files at a time
  dropbox.mime_types = /.*/i; // Only allow pngs, jpgs, and gifs
  dropbox.success = function(file, response) {
    log.innerHTML += '<p>' + file.name + ' was successfully uploaded: ' + response + '</p>'
  }
  dropbox.error = function(file, status, response) {
    log.innerHTML += '<p>' + file.name + ' failed to upload: ' + error + '</p>'
  }
</script>
