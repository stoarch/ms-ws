# encoding: utf-8

require 'tiny_tds' 
require_relative 'meter_receiver'

## ......... ## 
#  CONSTANTS  # 
############### 
PROJ_KAITPAS = 28
PROJ_CPOINTS = 28

ONE_MINUTE_IN_SECS = 60 
ONE_HOUR_IN_SECS = 3600 

KABISKO_CH1_FLOWRATE = 9333353;
KABISKO_CH2_FLOWRATE = 9333354;
KABISKO_CH1_PRESSURE = 9333355;
KABISKO_CH2_PRESSURE = 9333356;

KARASU_CH1_FLOWRATE = 9333343;
KARASU_CH2_FLOWRATE = 9333344;
KARASU_CH1_PRESSURE = 9333345;
KARASU_CH2_PRESSURE = 9333346;

KYZYLSAY_CH1_FLOWRATE = 9333333;
KYZYLSAY_CH2_FLOWRATE = 9333334;
KYZYLSAY_CH1_PRESSURE = 9333335;
KYZYLSAY_CH2_PRESSURE = 9333336;


CURRENT_DAY = 0
PREVIOUS_DAY = 1
CURRENT_MONTH = 2
PREVIOUS_MONTH = 3
UNIVERSAL_PERIOD = 4

ERGOMERA_METERS = [ :kabisko, :karasu, :kyzylsay ];

ERGO_NAME = { kabisko: "Кабиско", karasu: "Карасу 3", kyzylsay: "Кызыл-Сай" }

ERGOMERA_METERS_CODES = {
kabisko: [KABISKO_CH1_FLOWRATE, KABISKO_CH2_FLOWRATE], 
				 karasu: [KARASU_CH1_FLOWRATE, KARASU_CH2_FLOWRATE],
				 kyzylsay: [KYZYLSAY_CH1_FLOWRATE, KYZYLSAY_CH2_FLOWRATE]
};

ERGOMERA_PRESSURE_CODES = {
kabisko: [KABISKO_CH1_PRESSURE, KABISKO_CH2_PRESSURE],
				 karasu: [KARASU_CH1_PRESSURE, KARASU_CH2_PRESSURE],
				 kyzylsay: [KYZYLSAY_CH1_PRESSURE, KYZYLSAY_CH2_PRESSURE]
};

## CPointDataReceiver ##
#
# goal: Receive data from cpoint database: 
#	1. Control points
#	2. Main flowmeters
#	3. Ergomera flowmeters
#
class CPointDataReceiver
	include MeterReceiver

def initialize( cfg )
	@cfg = cfg
end

	attr :fmeters

# == Get ergomera last values from db 
#
# returns: array of FlowMeter's with values for egomera
#
def get_ergomera_vals

		retval = {} 

		ERGOMERA_METERS.each do |m|

		codes = ERGOMERA_METERS_CODES[ m ]

		res = retval[m] || {} 

		codes.each do |code|

data = load_fm_value( code, ERGO_NAME[m])

				flowrate = {code: code, value: data.values[0].value, date: data.values[0].datetime.strftime("%H:%M %d-%m-%Y")}

				res ||= {}
				res[:flowrate] ||= [] 

				res[:flowrate] << flowrate 
		end

		codes = ERGOMERA_PRESSURE_CODES[ m ]

		codes.each do |code|
			meter = FlowMeter.new( address: code )

			data = load_fm_value( code, ERGO_NAME[m])

			pressure = {code: code, value: data.values[0].value/100, date: data.values[0].datetime.strftime("%H:%M %d-%m-%Y")}

			res[:pressure] ||= []
			res[:pressure] << pressure
		end

		retval[m] = res unless retval[m];
	end

	return retval
end

# == Connect to cpoint meters db
#
def connect_to_meters_db
	@db = Sequel.tinytds(
			user: 'puser', password: 'Win3Ts', host: '192.168.10.8', database: 'counters', 
			encoding: 'cp1251'
			)
end

def connect_to_cpoint_db
	connect_to_meters_db
end


# == Decrypt the value
#
# @param
#   val - encrypted value
#
# @return
#   unencrypted value
#
def decrypt( val, cfg_part = "dest" )
	c = OpenSSL::Cipher::Cipher.new("aes-256-cbc")

	c.decrypt
	c.key = @cfg[cfg_part]["key"]
	c.iv = Base64.decode64( @cfg[cfg_part]["iv"] )

	uval = Base64.decode64( val )

	res = c.update( uval )
	res << c.final
	res
end


# == Load meters consumption for project for internal db
#
#  Read meter consumption in form: hourly vals for day interval, daily for months, or all for each meter in project
#	 Db is specified in another place. We only use it.
#
# Params:
#		1. proj_id - project from which meters is received
#		2. period_kind - which period to use (CURRENT_DAY, PREVIOUS_DAY, CURRENT_MONTH, PREVIOUS_MONTH, UNIVERSAL_PERIOD)
#		3. interval_kind - which values to receive (ALL_VALS, HALFHOUR_VALS, HOUR_VALS, DAILY_VALS)
#		4. date_start - inclusive date from which to receive data
#		5. date_end - inclusive date to which receive data
# 
#	Returns:
#		List of FlowMeters with values
#
def load_meters_consumption_ex( proj_id, period_kind, interval_kind, date_starty, date_end )

	meters = get_meter_dict( @db, proj_id )

	 load_meters_consumption( @db, meters.map{|m| m[:sn].to_i }, period_kind, interval_kind )
end


# == Load energomera meters consumption 
#
def load_energomera_meters_consumption( period_kind, interval_kind, date_start, date_end )

	evals = load_meters_consumption_ex( ENERGOMERA_PROJECT, period_kind, interval_kind, date_start, date_end )

	evals[0].each do |meter|
		meter.caption = $energo_codes[meter.address]
	end
	evals[0]
end


# == Load energomera single meter consumption 
#
def load_energomera_single_meter_consumption( meter_id, period_kind, interval_kind, date_start, date_end )

	evals = load_meters_consumption( @db, [meter_id], period_kind, interval_kind )

	evals[0].each do |meter|
		meter.caption = $energo_codes[meter.address]
	end
	evals[0]
end


# == Load list of meters for project
#
def load_root_energomera_meters()
	load_root_fm( @db, ENERGOMERA_PROJECT )
end

# == Load the last value for flowmeter from db
#
# params: 
#	acode - flowmeter code to receive
#	acaption - caption for this flowmeter
# retval:
#	flowmeter with this value and datetime set
#
def load_fm_value( acode, acaption )

	@db ||= connect_to_meters_db
	vals = @db[:_data].filter( :address => acode ).order(:datetime).last
	@db.disconnect
	@db = nil

	return NullFlowMeter.instance if vals.nil?

	result = FlowMeter.new( address: acode, caption: acaption )
result.values << FlowMeterValue.make( meter: result, value: vals[:value], datetime: vals[:datetime] )

	return result
end



# == Load the hourly consumption of flow meters
#
def load_fm_consumption
	@db ||= connect_to_meters_db

	$fm.each do |fm|
	vals = @db[:_data].filter( :address => fm.address ).order(:datetime).last

	if vals.nil? or ( vals.size == 0 )
		fm.value_range.final = FlowMeterValue.make( :meter => fm, :datetime => DateTime.new(1970,1,1), :value => 0)
	fm.value_range.start = FlowMeterValue.make( :meter => fm, :datetime => DateTime.new(1970,1,1), :value => 1)

		next
	end

	vals_prev = @db[:_data].filter( :address => fm.address ).filter{ datetime <= ( vals[:datetime] - 360 ) }.order(:datetime).last


	fm.value_range.final = FlowMeterValue.make( :meter => fm, :datetime => vals[:datetime], :value => vals[:value])
fm.value_range.start = FlowMeterValue.make( :meter => fm, :datetime => vals_prev[:datetime], :value => vals_prev[:value])
	end

rescue
	ap $!
	$logger.error $!
	$logger.info $@.split("\n")
ensure
	@db.disconnect if @db
	@db = nil
end


def read_vzljot_flowmeters_table(  )

	@db ||= connect_to_meters_db

	@fmeters = []

	$fm_vzljot.each do |fm|

			vals = @db[:_data].filter( :address => fm.address )
			.order(:datetime)
		.last(10)

			fm.values.clear

			@fm = fm

			prev_val = 0

			new_vals = vals.group_by{|v| v[:address]}.map{|v| v[1]}.map{|c|  [c[0], c.detect{|i| c[0][:datetime].to_time - i[:datetime].to_time   > 55*ONE_MINUTE_IN_SECS }]}.flatten


			new_vals.each_with_index do |v, i|

				date = v[:datetime] if v
				val = v[:value] if v

				date = DateTime.new(1970,1,1) if v.nil? or v[:datetime].nil?
				val = 0 if v.nil? or v[:value].nil?

				@fm.values << FlowMeterValue.make( :meter => @fm, 
						:datetime => date, 
						:value => val )
			end

		@fmeters << @fm
	end

	@fmeters
rescue
	ap $!
	$logger.error $!
	$logger.info $@.split("\n")
ensure
	@db.disconnect if @db
	@db = nil
end	

ENERGOMERA_PROJECT = 101

	def read_energomera_meters_table()
		read_flow_meters_table( ENERGOMERA_PROJECT, $energomera_meters )	
	end

# == Load the table of flowmeters values for current day
#
# === Info
#  Read the base flowmeters for control. Read values split on hour, discarding internal (10 max)
#
# @params
#   proj_id - from which flowmeters to load
# @return
#   array of pairs [flowmeter, array of values]
#
def read_flow_meters_table( proj_id = 33, fms = $fm )

	@db = connect_to_meters_db

	@fmeters = []

	fms.each do |fm|

		vals = @db[:_data].filter( :address => fm.address )
		.order(:datetime)
		.last(120)

		fm.values.clear

		@fm = fm

		prev_val = 0

		new_vals = vals.group_by{|v| v[:address]}.map{|v| v[1]}.map{|c|  [c[0], c.detect{|i| c[0][:datetime].to_time - i[:datetime].to_time   > 55*ONE_MINUTE_IN_SECS }]}.flatten


		new_vals.each_with_index do |v, i|
			
			next if v.nil?

			@fm.values << FlowMeterValue.make( 
					:meter => @fm, 
					:datetime => v[:datetime], 
					:value => v[:value] )

			if @fm.point_id > -1 && i == 0
				@fm.near_point_value = read_near_cpoint_value( @fm.point_id, v[:datetime] )
			end
		end

		@fmeters << @fm
	end

	@fmeters
rescue
	ap $!
	$logger.error $!
	$logger.info $@.split("\n")
ensure
	@db.disconnect if @db
	@db = nil
end	


#== read_near_cpoint_value
# Read cpoint value near datetime
#
# parms:
#		point_id - id of point to receive
#		datetime - anchor date for point
# returns:
#		value nearest to datetime or -1 if nothing found 
#		(hash with :datetime and :value)
def read_near_cpoint_value( point_id, datetime )

	datetime = datetime.to_datetime if datetime.is_a? Time

cpoint_vals = load_cp_period_vals_for( point_id, datetime - 120/1440.0, datetime )

	cpoint_vals.reverse.each do |cv|
		if( ((cv[:datetime].to_datetime - datetime).to_f*1440.0).abs < 15 ) #minutes
			return cv
		end
	end

	if( cpoint_vals.length > 0 ) #we have more time shifted value (control point does not give values for hour and more
		return cpoint_vals.reverse[0]
	end


	{datetime: DateTime.new(1970,1,1), value: -1.0}		
end

# == Load values for control point for current day
#
# @params: 
#     code - control point code (address) in db
#
def load_cp_period_vals_for( code, date_start, date_end )

	tries = 3

	begin

		@db ||= connect_to_meters_db

		@cur_cp_period_vals = @db[:_data].filter(:address => code)
			.where{ ( datetime > date_start) & ( datetime <= date_end )}
			.select(:datetime, :value)
			.order( :datetime )
			.all

	rescue TinyTds::Error => ex

		tries -= 1

		@db.disconnect if @db
		@db = nil

		retry if tries > 0

		raise ex if tries == 0
	end

end

# == Load values for control point for current day
#
# @params: 
#     code - control point code (address) in db
#
def load_day_vals_for( code )
	load_cp_period_vals_for code, (DateTime.now - 1), DateTime.now
end


def normalize_value( val )
	return { value: -1.0, datetime: DateTime.new(1970,1,1)} if  val.nil?
	val
end



# == Get the control points (pressure) from table
#
def get_cpoints_vals

	@db ||= connect_to_meters_db

	$cps.each do |cp|
		vals = @db[:_data].filter( :address => cp.address ).order(:datetime).last
		unless vals.nil?
			cp.value = {:datetime => vals[:datetime], :value => vals[:value]}
		else
			puts "ERROR: Vals nil for #{cp.address}"
			$logger.error "Vals is nil #{cp.address}"
			cp.value =  {datetime: DateTime.new(1970,1,1), value: -1.0}
		end  
	end

rescue
	ap $!
	$logger.error $!
	$logger.info $@.split("\n")
ensure

	@db.disconnect if @db
	@db = nil
end


#todo: remove global variables binding

# == Get last values for meters, excludes specified codes 
#	info: (i.e. meters without cpoints)
#
def get_values_without( codes )
	@db ||= connect_to_meters_db

	vals = @db[:_data].select( :address ).distinct.all
	@db.disconnect unless @db.nil?
	@db = nil

	addr = vals.map{|v| v[:address] }


	get_cpoint_values( addr - codes, all_vals: false, min_val: 0.0, error_vals: false )
end

# == Get value for multi cpoint values in hash 
#
# info: 
#	 1. return value and previous value for date (last value)
#			(min, max) for day, (min, max) for hour
#	
#
def get_cpoint_values( codes, options = {all_vals: true, min_val: 0.0, error_vals: true} )

	@db ||= connect_to_meters_db

	vals = []

	values = {}
	if( options[:all_vals] )
		vals = @db[:_data].where( address: codes ).order(:datetime).last(3000)
	else
		min_val = options[:min_val]
		p "Filtering for #{min_val}"
		vals = @db[:_data].where( address: codes ).where{ value >= min_val }.order(:datetime).last(3000)
	end

	@db.disconnect unless @db.nil?
	@db = nil

		vals.each do |v|
			values[v[:address]] ||= []
			values[v[:address]] << {value: v[:value], datetime: v[:datetime]} if values[v[:address]].size < 2
		end

	filtered_vals = codes.map{|c| [c.to_i, normalize_value( values[c.to_i] ) ]}

	resval = filtered_vals.map do |fv| 
		if fv.nil? || fv[1].nil? || fv[1][0].nil? || fv[0].nil? || fv[1][1].nil?

			address = 0
			address = fv[0] unless fv[0].nil?

			value = -1.0
			date = DateTime.new(1970,1,1)

			if fv.nil?
				$logger.error 'CPointDataReceiver::get_cpoint_values :: No data'
			elsif fv[1].nil?
				$logger.error 'CPointDataReceiver::get_cpoint_values ::Values array is nil'
				$logger.ap fv
			elsif fv[1][0].nil?
				$logger.error 'CPointDataReceiver::get_cpoint_values :: Values not an array'
				$logger.ap fv[1]
				if fv[1].is_a? Hash
					value = fv[1][:value]
					date = fv[1][:datetime]
				end
			elsif fv[1][1].nil?
				$logger.error 'CPointDataReceiver::get_cpoint_values :: Next val is nil'
				$logger.ap fv[1]
			elsif fv[0].nil?
				address = 0
				$logger.error 'CPointDataReceiver::get_cpoint_values :: Code is undefined'
				$logger.ap fv
			end

			next unless options[:error_vals]

			{datetime: date, value: value, code: address, prev_value: 0.0}
		else
			{datetime: fv[1][0][:datetime], value: fv[1][0][:value], code: fv[0], prev_value: fv[1][1][:value]}
		end
	end

	resval.reject(&:nil?)

	rescue
		ap $!
		ap $@.split("\n")[0..4]
		$logger.error $!
		$logger.info $@.split("\n")[0..4]
	end
end
