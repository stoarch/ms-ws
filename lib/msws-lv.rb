# encoding: utf-8
#
require 'sinatra/base'
require 'rack-flash'
require 'sinatra/contrib'
require 'net/http'
require 'uri'
require 'time_difference'
require 'sinatra/cross_origin'
require 'open-uri'

require 'bundler/setup'
Bundler.require

require_relative 'cpoint'
require_relative 'cpoint-data-receiver'
require_relative 'flowmeter'
require_relative 'flowmeter-data-receiver'
require_relative 'flowmeter_value'
require_relative 'flowmeter_value_range'
require_relative 'sql_strings'
require_relative 'os-module'
require_relative 'timer'
require_relative 'graph_calculator'
require_relative 'graph_generator'
require_relative 'pulsar_saver'
require_relative 'latch_manager'
require_relative 'cpoint_saver'




###############
## CONSTANTS ##
###############

	KEY_IND = 0
	VAL_IND = 1

	REP_FILE = "#{File.dirname(__FILE__)}/static/rep_kaipas_analysis.xls"
	REP_ERGOMERA_FILE = "#{File.dirname(__FILE__)}/static/rep_ergomera_analysis.xls"

	#todo: load from iran-meters settings file
	IRAN_FMETERS_IDS = [
		["Забадам 1", 2_500_000],
		["Забадам 2", 2_500_001],
		["БСВ", 2_500_002],
		["ШНОС", 2_500_003],
		["СЕВЕР", 2_500_004],
		["Кызыл Ту", 2_500_005]
	]

	IRAN_FMETERS = IRAN_FMETERS_IDS.map{|id| FlowMeter.new( :address => id[1], :caption => id[0] ) }

	ENERGOMERA_FMETERS_IDS = [
		["Фитнес Тулпар", 150_114],
		["Роддом Алдиярова", 150_120],
		["СШ №7", 150_146],
		["Шымкентпиво", 150_160]
	]

	$energomera_meters = ENERGOMERA_FMETERS_IDS.map{|id| FlowMeter.new( :address => id[1], :caption => id[0] ) }
	$energo_codes = Hash[*ENERGOMERA_FMETERS_IDS.map{|v| v.reverse }.flatten]  
	AKBAY_WELLS_FM_IDS = [
		["18 скважина", 112_217_391],
		["32 скважина", 112_217_392],
		["21 скважина", 112_217_393],
		["24 скважина", 112_217_394],
		["22 скважина", 112_217_395],
		["27 скважина", 112_217_396],
		["28 скважина", 112_217_397],
		["16 скважина", 112_217_398],
		["29 скважина", 112_217_399],
		["25 скважина", 112_217_400]
	]

	$akbay_wells_fm = AKBAY_WELLS_FM_IDS.map{|id| FlowMeter.new( :address => id[1], :caption => id[0] ) }

	INTERVAL_ALL_VALUES = 0
	INTERVAL_HALF_HOUR_VALUES = 1
	INTERVAL_HOUR_VALUES = 2
	INTERVAL_DAY_VALUES = 3

	REP_SINGLE_XLS = "./static/rep-single.xls"
	NEED_PNG = true

	PROJ_ARAD = 28
	PROJ_PROM_GROUP = 33
	PROJ_IVR = 34
	PROJ_KAITPAS = 28
	PROJ_METERS = 29 # from cpoint
	PROJ_BAM = 32
	NO_PROJECT = -1

SECONDS = 1
MINUTES = 60*SECONDS
HOUR = 60*MINUTES

MINUTES_IN_HOUR = 60
HOUR_IN_DAY = 24.0

MINUTES_IN_DAY = MINUTES_IN_HOUR * HOUR_IN_DAY

TEN_MINUTES = 10 * MINUTES 


	PERIOD_CURRENT_DAY = 0
	PERIOD_PREVIOUS_DAY = 1
	PERIOD_CURRENT_MONTH = 2
	PERIOD_PREVIOUS_MONTH = 3
	PERIOD_ANY = 4



	KYZYLSAY_VHOD = 100150
	KYZYLSAY_POSLE_ZADV = 100160
	FOSFOR = 9904014
	SEVER = 400960
	SPORTIVNY = 861922
	TEC3 = 55555

	LUMUMBA = 9903999
	KOROTKI = 9902405
	NURSAT = 66666
	SAULE = 400970
	BSV_SAULE_VYHOD = 123002
	BSV_SAULE_VHOD = 123004
	BSV_COLLECTOR = 123005

  KARASU_900 = 123003
	KARASU_1200 = 123001
	NEKRASOVA_216 = 123006
	KAZGYRT = 123008
	SAMAL2 = 88888
	SAMAL3 = 99990

	PL_ALFARABY = 100180
	POJARKA = 100190
	ALFARABI_POSLE_REG = 123007
	ALFARABI_DO_REG = 125001
	TURLANOVKA = 77777
	KRYG_NPZ = 100177
	DOSTYK = 44444


	#== meters
	KYZYLTU_600_METER = 500560
	KALININA_900_METER = 500501
	KALININA_1200_METER = 500500
	TEC3_800_METER = 800753
	TEC3_METER = 402624
	TEC3_LEFT_500_METER = 800750
	TEC3_RIGHT_500_METER = 800751
	KARASU_1200_METER = 409440
	KARASU_900_METER = 409441
	KAITPAS_METER = 500560
	SNOS_METER = 500571
	KARASU_3N_METER = 700650
	KYZYLSAY_3N_METER = 700750
	SIFON_1_METER = 330750
	SIFON_2_METER = 340750
	SIFON_3_METER = 317750
	SIFON_4_METER = 318750
	SIFON_5_METER = 350750
	SIFON_6_METER = 360750
	SIFON_7_METER = 319750
	SIFON_8_METER = 320750


## VZLJOT ##
############

 METER_SNOS_FLOWRATE1 = 1_000_006 
 METER_SNOS_FLOWRATE2 = 1_000_007 
 METER_SNOS_LEVEL1 = 1_000_008 
 METER_SNOS_LEVEL2 = 1_000_009 
 METER_TEC3_FLOWRATE1 = 1_000_010
 METER_TEC3_FLOWRATE2 = 1_000_011
 METER_TEC3_VOLPLUS1 = 1_000_012
 METER_TEC3_VOLPLUS2 = 1_000_013
 METER_CEMZAVOD_VOLPLUS1 = 1_000_014
 METER_HIMFARM_VOLPLUS1 = 1_000_015
 METER_PIVZAVOD_VOLPLUS1 = 1_000_016

$vzljot_meters = {
   METER_SNOS_FLOWRATE1 => 'Шнос расход 1',
   METER_SNOS_LEVEL1 => 'Шнос уров 1',
   METER_TEC3_FLOWRATE1 => 'ТЭЦ3 расход 1',
   METER_TEC3_VOLPLUS1 => 'ТЭЦ3 объем 1',
   METER_HIMFARM_VOLPLUS1 => 'Химфарм объем 1',
   METER_CEMZAVOD_VOLPLUS1 => 'Цем Завод объем 1',
   METER_PIVZAVOD_VOLPLUS1 => 'Пив Завод объем 1'
	
}

$fm_vzljot = $vzljot_meters.map do | key, caption |
	FlowMeter.new( address: key, caption: caption )
end


if OS.linux? || OS.mingw?
	YAML::ENGINE.yamler = 'syck'
end #

class NilClass
	def value
		@val ||= FlowMeterValue.make( :meter => nil, :datetime => Time.now, :value => 0)
	end
end

AwesomePrint.defaults = {
	plain: true
}

$logger = Logger.new( "msws#{Thread.current.object_id}.log", 5, 10*1024*1024)

if OS.linux? 
	cp_cfg = File.read("#{File.dirname(__FILE__)}/cfg/points.cfg.yaml")[5..-1]
	$points = YAML.load( StringIO.new(cp_cfg ))
elsif OS.windows? || OS.mingw?
	$points = YAML.load( File.read("#{File.dirname(__FILE__)}/cfg/points.cfg.yaml"))
else
	puts "Error: Unsupported OS"
	exit
end


$cps = $points.to_a.flatten!.each_slice(2).to_a.map do |key, caption| 
	key = key[5..-1] if key =~ /---/
	puts "#{key} : #{caption}"

	CPoint.new( :address => key.to_i, :caption => caption ) 
end
ENFORA_COLOR = '#EFBEF7'
ECOMATRIX_COLOR = '#BEE3F7'
ENERGY_COLOR = '#CDF0C0'
	$point_colors = {
		#ENFORA
		9904014=> ENFORA_COLOR,
		400960=> ENFORA_COLOR,
		861922=> ENFORA_COLOR,
		55555=> ENFORA_COLOR,
		9903999=> ENFORA_COLOR,
		9902405=> ENFORA_COLOR,
		66666=> ENFORA_COLOR,
		400970=> ENFORA_COLOR,
		123002=> ENFORA_COLOR,
		123004=> ENFORA_COLOR,
		123005=> ENFORA_COLOR,
		44444=> ENFORA_COLOR,
		123001=> ENFORA_COLOR,
		123003=> ENFORA_COLOR,
		123006=> ENFORA_COLOR,
		123008=> ENFORA_COLOR,
		99990=> ENFORA_COLOR,
		100180=> ENFORA_COLOR,
		100190=> ENFORA_COLOR,
		123007=> ENFORA_COLOR,
		125001=> ENFORA_COLOR,
		77777=> ENFORA_COLOR,
		100177=> ENFORA_COLOR,
		100150=> ENFORA_COLOR,
		20151=> ENFORA_COLOR,
		20152=> ENFORA_COLOR,
		20155=> ENFORA_COLOR,
		20156=> ENFORA_COLOR,
		#ENERGY#
		4116=> ENERGY_COLOR,
		4114=> ENERGY_COLOR,
		4403=> ENERGY_COLOR,
		4113=> ENERGY_COLOR,
		4115=> ENERGY_COLOR,	
		#ECOMATRIX#
		241715=> ECOMATRIX_COLOR,	
		241700=> ECOMATRIX_COLOR,	
		28062016=> ECOMATRIX_COLOR	
	}




$meters = YAML.load( File.read( "#{File.dirname(__FILE__)}/cfg/meters.cfg.yaml" )) 
$fm = $meters.to_a.flatten!.each_slice(2).to_a.map do |key, val| 
	FlowMeter.new( :address => key, :caption => val[:caption], :point_id => val[:point_id] )
end

$siphon_meters = YAML.load( File.read( "#{File.dirname(__FILE__)}/cfg/siphons.cfg.yaml" )) 
$siphon_fm = $siphon_meters.to_a.flatten!.each_slice(2).to_a.map do |key, caption| 
	FlowMeter.new( :address => key, :caption => caption ) 
end

#
# Main web application for water service
#
class Application < Sinatra::Base
    enable :sessions
    register Sinatra::Flash
		register Sinatra::CrossOrigin

    use Rack::Session::Cookie, secret: 'Fi5ty thou$and monkey pu1l banana in it a5s'
    use Rack::Flash
    
    set :static, true
    set :root, File.dirname(__FILE__)

    configure :development do
        register Sinatra::Reloader
    end

	#set :environment => :production #:development

	disable :show_exceptions

	configure do
		set :public_folder, File.dirname(__FILE__) + '/static' 

	end

	before do
		expires 600, :public, :must_revalidate
	end

	class << self
		attr_accessor :control_points_values, :flowmeter_values
		attr_accessor :flowmeter_range
		attr_accessor :control_points_dates
	end

		
	helpers do

		def decode_str( in_str )
			ec1 = Encoding::Converter.new "UTF-8", "Windows-1251", :invalid => :replace, :undef => :replace, :replace => ""

			ec2 = Encoding::Converter.new "Windows-1251", "UTF-8", :invalid => :replace, :undef => :replace, :replace => ""

			ec1.convert ec2.convert in_str 
		end

		def get_cp_mode( code )
			@mmode ||= {}
			@mmode.default = {min:0,max:11}
		
			if !(defined? @last_read)||( Time.now - @last_read > 60*5 )
							@last_read = Time.now

							modes = read_modes_from_service()
							if( modes.code == "200" )
								mmode = JSON.parse(modes.body)
								mm = {}
								mmode.each_pair{|k,v| 
									vv = {}; 
									v.each_pair{|k1,v1| vv[k1.to_sym] = v1};
									mm[k.to_i] = vv }
								@mmode = mm 
								@mmode[code]
							else
								{min:"1",max:"12",caption:"error"}
							end
			else
				@mmode[code]				
			end
		end

		def get_cp_value( code )

			settings.control_points_values[ code ]
		end
		
		def get_cp_date( code )

			settings.control_points_dates[ code ]
		end

		def get_flowmeter_value( code )

			settings.flowmeter_values[ code ]
		end

		def get_flowmeter_range( code )

			settings.flowmeter_range[ code ]
		end
	end #helpers


	# == Init instance of webapp
	#
	def initialize
		super()
		load_cfg

		@cp_data_rcv = CPointDataReceiver.new( @cfg )
		@fm_data_rcv = FlowMeterDataReceiver.new( @cfg )
	end

	attr :logger


	###################
	## SERVER ROUTES ##
	###################

	# == Main menu and start page
	#
	get '/' do  
		begin 
			erb :msws_main
		rescue 
			$logger.error $!.message	     
			$logger.ap $@[0..4]
		end
	end


	# == Mnemoschemes for internal use
	#
	get '/mskip' do
		erb :mskip
	end



	# == Return json-ed version of cpoint ranges
	#
	get '/cp-modes' do
		resp = read_modes_from_service()
	  resp.body	
	end


	post '/cp-modes' do
		resp = read_modes_from_service()
	  resp.body	
	end

	# == Display control points mnemoscheme
	#
	get '/cp' do
		begin		
			@cp_data_rcv.get_cpoints_vals

			last_modified Time.now

			erb :cp
		rescue 
			$logger.error $!.message
			$logger.ap $@[0..4]
		end
	end

	# == Display control points mnemoscheme (packed for optimal view for 1366x768)
	#
	get '/cp_int2' do
		begin		
			@cp_data_rcv.get_cpoints_vals

			last_modified Time.now

			erb :cp_int2
		rescue 
			$logger.error $!.message
			$logger.ap $@[0..4]
		end
	end

	# == Display control points mnemoscheme (packed for optimal view for 1920x1080)
	#
	get '/cp_int' do
		begin		
			@cp_data_rcv.get_cpoints_vals

			last_modified Time.now

			erb :cp_int
		rescue 
			$logger.error $!.message
			$logger.ap $@[0..4]
		end
	end


	# == Display control points mnemoscheme for cell phones
	#
	get '/cp.wml'  do
		begin
			@cp_data_rcv.get_cpoints_vals

			last_modified Time.now

			erb :"cp.wml"
		rescue 
			$logger.error $!.message
			$logger.ap $@[0..4]
		end
	end



	# == Display flowmeter mnemoscheme
	#
	get '/fm' do
		begin
			@cp_data_rcv.read_flow_meters_table

			last_modified Time.now

			erb :fm
		rescue 
			$logger.error $!.message
			$logger.ap $@[0..4]
		end
	end

	get '/rep/fm/energomera' do
		begin
			@cp_data_rcv.connect_to_cpoint_db()
			@rep_main_fm = @cp_data_rcv.load_root_energomera_meters 
			erb :rep_fm_energomera_form
		rescue 
			ap $!.message
			ap $@[0..5]

			$logger.error $!.message
			$logger.ap $@
		end
	end

	# == Energomera fm
	post '/rep/fm/energomera' do
		begin
			@date_start = get_parms_date_start
			@date_end = get_parms_date_end

			@cp_data_rcv.connect_to_cpoint_db()

ap params

			@ergo_fm_vals = @cp_data_rcv.load_energomera_single_meter_consumption( params[:form][:mainfm], params[:period_kind].to_i, params[:interval_kind].to_i, @date_start, @date_end )

			last_modified Time.now

			ap "Energomera for #{@date_start} to #{@date_end} has #{@ergo_fm_vals.count} meters with #{@ergo_fm_vals[0].values.count} values"

			erb :rep_fm_energomera
		rescue 
			ap $!.message
			ap $@[0..5]

			$logger.error $!.message
			$logger.ap $@
		end
	end

	# == Display siphon flowmeter mnemoscheme
	#
	get '/siphon-fm' do
		begin
			@cp_data_rcv.read_flow_meters_table( 33, $siphon_fm )

			last_modified Time.now

			erb :siphon_fm
		rescue 
			$logger.error $!.message
			$logger.ap $@[0..4]
		end
	end


	# == Select parms:: Consumption report for period for meter
	#	for prom group
	#
	get '/rep_cons' do
		begin
			@main_fm = @fm_data_rcv.load_flowmeters( PROJ_PROM_GROUP )

			erb :rep_cons
		rescue 
			$logger.error $!.message
			$logger.ap $@[0..4]
		end
	end

	# == Generate:: Consumption report for period for specified meter
	#	for prom group
	#
	post '/rep_cons' do
		begin
			fmeter = params[:mainfm]

			@fm_data_rcv.date_start = get_parms_date_start
			@fm_data_rcv.date_end = get_parms_date_end

			meter = FlowMeter.new( :address => fmeter, :caption => "meter" )

			@meter = @fm_data_rcv.read_meter( meter, PROJ_PROM_GROUP )

			erb :rep_cons_gen
		rescue 
			$logger.error $!.message
			$logger.ap $@[0..4]
		end 
	end


	# == Generate table with vzljot flowmeters as mnemoscheme last val
	#
	get '/rep-all-vzljot' do
		begin

			@fm_vzljot = @cp_data_rcv.read_vzljot_flowmeters_table

			$logger.ap @fm_vzljot

			erb :vzljot_fm_ms

		rescue
			$logger.error $!.to_s
			$logger.ap $@[0..4]
			'Sorry. Server error occured'
			$!.message + "\n" + $@[0]
		end
	end

	# == Generate table with siphon flowmeters as mnemoscheme last val
	#
	get '/siphon_flowmeters_ms' do
		begin

			@fmeters = @cp_data_rcv.read_flow_meters_table( 33, $siphon_fm )

			erb :siphon_flowmeters_ms

		rescue
			$logger.error $!.to_s
			$logger.ap $@[0..4]
			'Sorry. Server error occured'
		end
	end

	# == Show cds mnemoscheme: flowmeters main, flowmeters siphon, flowmeters akbay
	get '/cdsms' do
		begin
			erb :'cdsms'
		rescue
			$logger.error $!.to_s
			$logger.ap $@[0..4]
			'Sorry. Server error occured'
		end
	end


	# == Generate table with flowmeters as mnemoscheme last val
	#
	get '/flowmeters_ms' do
		begin

			@fmeters = @cp_data_rcv.read_flow_meters_table

			erb :flowmeter_ms

		rescue
			$logger.error $!.to_s
			$logger.ap $@[0..4]
			'Sorry. Server error occured'
		end
	end


	# == Select period for:: Meters consumption report for prom group
	#
	get '/rep_cns_prom__all' do
		begin
			erb :rep_cns_prom__all
		rescue 
			$logger.error $!.message
			$logger.ap $@[0..4]
		end
	end


	# == Generate:: Meters consumption report for prom group
	#
	post '/rep_cns_prom__all' do
		begin
			get_cached_flowmeters( PROJ_PROM_GROUP )

			erb :rep_cns_prom_all_gen
		rescue 
			$logger.error $!.message
			$logger.ap $@[0..4]
		end
	end 



	# == Select period for:: Meters consumption report for IVR
	#
	get '/rep_cns_ivr_all' do
		begin
			erb :rep_cns_ivr_all
		rescue 
			$logger.error $!.message
			$logger.ap $@[0..4]
		end
	end

	# == Generate:: Meters consumption report for IVR
	#
	post '/rep_cns_ivr_all' do
		begin
			get_cached_flowmeters( PROJ_IVR )

			erb :rep_cns_ivr_all_gen
		rescue 
			$logger.error $!.message
			$logger.ap $@[0..4]
		end 
	end 



	# == Show water city map
	#
	get '/water-map' do
		begin
			@cp_data_rcv.get_cpoints_vals

			erb :'water-city-map.svg'
		rescue 
			$logger.error $!.message
			$logger.ap $@[0..4]
		end 
	end

	# == Show water city map on google map
	#
	get '/water-map-google' do
		begin
			@cp_data_rcv.get_cpoints_vals

			erb :'water-city-google-map'

		rescue 
			$logger.error $!.message
			$logger.ap $@[0..4]
		end
	end


	# == Show detailed map of city with meters on it
	get '/ms-compact' do
		begin
			@cp_data_rcv.get_cpoints_vals
			@fmeters = @cp_data_rcv.read_flow_meters_table

			settings.control_points_values = Hash[*$cps.map{|i| [i.address, i.value.value]}.flatten] 
			settings.control_points_dates = Hash[*$cps.map{|i| [i.address, i.value.datetime]}.flatten]
			settings.flowmeter_values = Hash[*@fmeters.map{|i| [i.address, i.values[0].value - i.values[1].value]}.flatten] 

			settings.flowmeter_range = Hash[*@fmeters.map{|i| [i.address, {start: i.values[0].datetime, finish:  i.values[1].datetime}]}.flatten] 


			erb :'ms-compact-all'
			
		rescue

			$logger.error $!.message
			$logger.ap $@[0..4]

		end
	end

	# == Show graph images on control points
	#
	get '/cp-graphs/:code' do
		begin
			cp_code = params[:code].to_i

			file_name = "/images/#{cp_code}.png"

			redirect file_name

		rescue 
			$logger.error $!.message
			$logger.error $@.inspect
		end
	end



	# == Select the control point and range to generate graph
	#
	get '/cp-gr' do
		begin
			@selected_cp = params[:cp] || 0
			@selected_cp = @selected_cp.to_i
			erb :"cp-gr-select"

		rescue 
			$logger.error $!.message
			$logger.ap $@[0..4]
		end
	end

	# == Generate graph for get request
	get '/cp-gr-fl' do
		begin
			ap "Generate cpoint graphs: #{params}"

			@date_start = DateTime.now - 1 
			@date_end = DateTime.now + 1 


			cps_addr = params[:cps].to_i

			@sel_cp = $cps.detect{ |cp| cp.address == cps_addr }

			@cp_mode_min = get_cp_mode(cps_addr)[:min]
			@cp_mode_max = get_cp_mode(cps_addr)[:max]

			halt 400 if @sel_cp.nil?

			@cur_cp_period_vals = @cp_data_rcv.load_cp_period_vals_for( @sel_cp.address, @date_start, @date_end ).map{|v| v[:value] = v[:value].round(1); v}
	
			ap 'generating'

			erb :cp_gr_rep_min

		rescue 
			$logger.error $!.message
			$logger.ap $@[0..4]
		end
	end

	# == Generate:: graph for selected cpoint and date range
	#
	post '/cp-gr' do
		begin
			ap "Generate cpoint graphs: #{params}"

			@date_start = get_parms_date_start
			@date_end = get_parms_date_end


			cps_addr = params[:cps].to_i

			@sel_cp = $cps.detect{ |cp| cp.address == cps_addr }

			@cp_mode_min = get_cp_mode(cps_addr)[:min]
			@cp_mode_max = get_cp_mode(cps_addr)[:max]



			halt 404 if @sel_cp.nil?

			@cur_cp_period_vals = @cp_data_rcv.load_cp_period_vals_for( @sel_cp.address, @date_start, @date_end ).map{|v| v[:value] = v[:value].round(1); v}

			erb :cp_gr_rep

		rescue 
			$logger.error $!.message
			$logger.ap $@[0..4]
		end
	end



	# 
	# Select control point and period for table
	#
	get '/cp-tab' do
		begin
			@selected_cp = params[:cp] || 0
			@selected_cp = @selected_cp.to_i

			erb :"cp-tab-select"

		rescue 
			$logger.error $!.message
			$logger.ap $@[0..4]
		end
	end

	#
	# Generate the table for control point for period
	#
	post '/cp-tab' do
		begin
			@date_start = get_parms_date_start
			@date_end = get_parms_date_end

			cps_addr = params[:cps].to_i

			@sel_cp = $cps.detect{ |cp| cp.address == cps_addr }

			halt 404 if @sel_cp.nil?

			@cur_cp_period_vals = @cp_data_rcv.load_cp_period_vals_for @sel_cp.address, @date_start, @date_end

			erb :"cp-tab-gen"

		rescue 
			$logger.error $!.message
			$logger.ap $@[0..4]
		end
	end



	#
	# Consumption report for kaitpas (param selection form)
	#
	get '/rep-cns-single' do
		begin
			@main_fm = @fm_data_rcv.load_flowmeters( PROJ_KAITPAS )

			erb :"rep-cns-single-form"
		rescue 
			$logger.error $!.message
			$logger.ap $@[0..4]
		end
	end

	#
	# Consumption report generation for kaitpas
	#
	post '/rep-cns-single' do
		begin
			@date_start = get_parms_date_start
			@date_end = get_parms_date_end

			@fm_data_rcv.date_start = @date_start
			@fm_data_rcv.date_end = @date_end

			@meter_id = params[:mainfm].to_i

			@period_kind = params[:period_kind].to_i

			@interval_kind = params[:interval_kind].to_i

			@interval_caption = interval_title(@interval_kind)

			@rep_meter = @fm_data_rcv.get_meter_info( @meter_id, PROJ_KAITPAS )

			@rep_main_fm, @date_start, @date_end = @fm_data_rcv.load_meter_consumption( PROJ_KAITPAS, @meter_id, @period_kind, @interval_kind )           


			if (@rep_main_fm.nil?) || (@rep_main_fm.size == 0)
				return "Нет данных для отчёта"
			end

			@file_name = "kp_meter_cons"


			consumption = []
			@rep_main_fm.each_with_index do |fm,i|
				cur_val = OpenStruct.new
				cur_val.date = fm.values[0].datetime
				cur_val.value = fm.values[0].value
				cur_val.flowrate = ( i > 0 )?(fm.values[0].value - @rep_main_fm[i-1].values[0].value):(0)

				consumption << cur_val
			end


			ToXls::Writer.new(consumption, 
					  :name => "#{@rep_meter.caption}", 
					  :columns => [:date, :value, :flowrate],
					  :headers => ['Адрес', 'Наименование', 'Начало', 'Конец', 'Расход']

					 )
			.write_io(REP_SINGLE_XLS)

			erb :rep_cns_single_gen      
		rescue 
			$logger.error $!.message
			$logger.ap $@[0..4]
		end
	end


	get "/rep-cns-single-xls" do
		begin
			send_file REP_SINGLE_XLS
		rescue 
			$logger.error $!.message
			$logger.ap $@[0..4]
		end
	end

	get "/rep-all-cp" do
		begin
			erb :rep_cp_all 
		rescue
			$logger.error $!.message
			$logger.ap $@[0..4]
		end
	end


	post '/rep-all-cp' do
		begin
			@date_start = get_parms_date_start
			@date_end = get_parms_date_end
			@period_kind = params[:period_kind].to_i
			@interval_kind = params[:interval_kind].to_i
			
			change_date_range_for_period_kind

			#//todo: 2. Filter data for interval kind
			temp_vals = {}

			$cps.each do |cp|

				cur_cp_val = @cp_data_rcv.load_cp_period_vals_for cp.address, @date_start, @date_end

				cur_cp_val = filter_by_date @interval_kind, cur_cp_val

				cur_cp_val.each do |cv|

					temp_vals[cv[:datetime]] ||= {}
					temp_vals[cv[:datetime]][cp] = cv[:value]
				end
			end

			@all_cp_vals = []
			temp_vals.keys.sort.each do |date_key|

				value = [date_key, {}]

				temp_vals[date_key].keys.sort{|a,b| a.address <=> b.address }.each do |cp_key|

					value[1][cp_key] = temp_vals[date_key][cp_key]
				end

				@all_cp_vals << value
			end


			erb :'rep-all-cp-gen'

		rescue
			$logger.error $!.message
			$logger.ap $@[0..4]
		end
	end


	#todo: refactor report generation to common place
	#todo: make class for report generation



	#
	# Consumption report for iran fms (param selection form)
	#
	get '/rep-cns-single-iran' do
		begin
			@main_fm = IRAN_FMETERS

			erb :"rep-cns-single-iran-form"
		rescue 
			$logger.error $!.message
			$logger.ap $@[0..4]
		end
	end

	#
	# Consumption report generation for iran fms
	#
	post '/rep-cns-single-iran' do
		begin
			@date_start = get_parms_date_start
			@date_end = get_parms_date_end

			@fm_data_rcv.date_start = @date_start

			@fm_data_rcv.date_end = @date_end

			@meter_id = params[:mainfm].to_i

			@period_kind = params[:period_kind].to_i

			@interval_kind = params[:interval_kind].to_i

			@interval_caption = interval_title(@interval_kind)

			@rep_meter = IRAN_FMETERS.find{|m| m.address == @meter_id }


			@rep_main_fm, @date_start, @date_end = @fm_data_rcv.load_meter_consumption( PROJ_KAITPAS, @meter_id, @period_kind, @interval_kind )           

			if @rep_main_fm.size == 0
				return "Нет данных для отчёта"
			end

			@file_name = "iran_meter_cons"

			generate_graph_for  @rep_meter.caption, calc_consumption( @rep_main_fm ), @file_name, NEED_PNG

			@file_name += ".png"

			erb :"rep-cns-single-iran-gen"
		rescue 
			$logger.error $!.message
			$logger.ap $@[0..4]
		end
	end




	PROJ_ERGOMERA = 101

	get '/rep/ergomera/analysis' do
		begin
			if( @curr_time.nil? )or(( DateTime.now.to_date - @curr_time ) > 0)
				@curr_time ||= DateTime.now.to_date

				@fm_data_rcv.connect_to_cpoint_db

				@rep_main_fm = @fm_data_rcv.load_base_fm_forest( PROJ_ERGOMERA )      
			end

			erb:"rep-ergomera-analysis-form"
		rescue 
			$logger.error $!.message
			$logger.ap $@[0..4]
		end
	end

	post '/rep/ergomera/analysis' do
		begin
			@date_start = get_parms_date_start
			@date_end = get_parms_date_end

			@fm_data_rcv.date_start = @date_start

			@fm_data_rcv.date_end = @date_end

			@meter_id = params[:form][:mainfm].to_i

			@period_kind = params[:form][:period_kind].to_i

			@rep_meter = @fm_data_rcv.get_meter_info( @meter_id, PROJ_ERGOMERA)

			@rep_main_data = @fm_data_rcv.read_meter_for_period( @rep_meter, PROJ_ERGOMERA, @period_kind )

			@date_start = @fm_data_rcv.date_start
			@date_end = @fm_data_rcv.date_end

			@rep_main_cons = @rep_main_data.values[1].value - @rep_main_data.values[0].value

			@rep_fms = @fm_data_rcv.load_submeters_consumption( PROJ_ERGOMERA, @meter_id, @period_kind )           

			puts "Ergomera Report ***** >>\n"
			puts @rep_fms.map{|r| [r.address,r.caption] }.sort

			@rep_scons_tot = @rep_fms.map{|r| r.values[1].value - r.values[0].value }.reduce(:+)
			@rep_scons_tot ||= 0


			@rep_fms.sort!{|a,b| a.address <=> b.address }

			@rep_fms.each do |fm|
				fm.start_value = fm.values[0].value
				fm.end_value = fm.values[1].value
				fm.flowrate = (fm.values[1].value - fm.values[0].value)
			end


			ToXls::Writer.new(@rep_fms, 
					  :name => "#{@rep_meter.caption}", 
					  :columns => [:address, :caption, :start_value, :end_value, :flowrate],
					  :headers => ['Адрес', 'Наименование', 'Начало', 'Конец', 'Расход']

					 )
			.write_io(REP_ERGOMERA_FILE)

			erb :"rep-ergomera-analysis-gen"
		rescue 
			$logger.error $!.message
			$logger.ap $@[0..14]
		end
	end

	# == Analysis report for kaitpas master meters
	#

	#
	# Form for select period and meter for work with
	#
	get '/rep/kaipas/analysis' do
		begin
			if( @curr_time.nil? )or(( DateTime.now.to_date - @curr_time ) > 0)
				@curr_time ||= DateTime.now.to_date

				@fm_data_rcv.connect_to_kipq4s_db

				@rep_main_fm = @fm_data_rcv.load_base_fm_forest( PROJ_KAITPAS )      
			end

			erb:"rep-kaipas-analysis-form"
		rescue 
			$logger.error $!.message
			$logger.ap $@[0..4]
		end
	end
	#
	# Report generation on user params selection
	#
	post '/rep/kaipas/analysis' do
		begin
			@date_start = get_parms_date_start
			@date_end = get_parms_date_end

			@fm_data_rcv.date_start = @date_start

			@fm_data_rcv.date_end = @date_end

			@meter_id = params[:form][:mainfm].to_i

			@period_kind = params[:form][:period_kind].to_i


			@rep_meter = @fm_data_rcv.get_meter_info( @meter_id, PROJ_KAITPAS )


			@rep_main_data = @fm_data_rcv.read_meter_for_period( @rep_meter, PROJ_KAITPAS, @period_kind )

			@date_start = @fm_data_rcv.date_start
			@date_end = @fm_data_rcv.date_end

			@rep_main_cons = @rep_main_data.values[1].value - @rep_main_data.values[0].value

			@rep_fms = @fm_data_rcv.load_submeters_consumption( PROJ_KAITPAS, @meter_id, @period_kind )           

			puts "***** >>\n"
			puts @rep_fms.map{|r| [r.address,r.caption] }.sort

			@rep_scons_tot = @rep_fms.map{|r| r.values[1].value - r.values[0].value }.reduce(:+)
			@rep_scons_tot ||= 0


			@rep_fms.sort!{|a,b| a.address <=> b.address }

			@rep_fms.each do |fm|
				fm.start_value = fm.values[0].value
				fm.end_value = fm.values[1].value
				fm.flowrate = (fm.values[1].value - fm.values[0].value)
			end



			ToXls::Writer.new(@rep_fms, 
					  :name => "#{@rep_meter.caption}", 
					  :columns => [:address, :caption, :start_value, :end_value, :flowrate],
					  :headers => ['Адрес', 'Наименование', 'Начало', 'Конец', 'Расход']

					 )
			.write_io(REP_FILE)

			erb :"rep-kaipas-analysis-gen"
		rescue 
			$logger.error $!.message
			$logger.ap $@[0..4]
		end
	end


	#
	# Excel generation for kaipas analysis
	#
	get '/rep/kaipas/analysis-xls' do
		begin
			send_file REP_FILE
		rescue 
			$logger.error $!.message
			$logger.ap $@[0..4]
		end
	end

	get '/rep/ergomera/analysis-xls-ergomera' do
		begin
			send_file REP_ERGOMERA_FILE
		rescue 
			$logger.error $!.message
			$logger.ap $@[0..4]
		end
	end

	#
	# Get params for main flowmeters report
	#
	get '/rep/meters/base' do
		begin
			load_base_meters
			erb :rep_base_meters_form
		rescue 
			$logger.error $!.message
			$logger.ap $@[0..4]
		end
	end

	post '/rep/meters/base' do
		begin
		  "Main meters base"	
		rescue 
			$logger.error $!.message
			$logger.ap $@[0..4]
		end
	end

# == Ergomera report
#
	get '/rep/meters/ergomera' do
		begin
			load_ergomera_meters
			erb :rep_ergomera_meters_form
		rescue 
			$logger.error $!.message
			$logger.ap $@[0..4]
		end
	end

	post '/rep/meters/ergomera' do
		begin
			params.to_s
		rescue 
			$logger.error $!.message
			$logger.ap $@[0..4]
		end
	end



	#
	# Get kaitpas2 meter status mnemoscheme
	#
	get '/ms/kaitpas2/status' do
		begin
			load_kaitpas_meters_status

			erb :"ms-kaitpas2"
		rescue
			$logger.error $!.message
			$logger.ap $@[0..4]
		end
	end

	#
	# Get taraz meter status mnemoscheme
	#
	get '/ms/taraz/status' do
		begin
			load_taraz_meters_status

			erb :"ms-taraz"
		rescue
			$logger.error $!.message
			$logger.ap $@[0..4]
		end
	end

	# 
	# Get kaitpas meter status mnemoscheme
	#
	get '/ms/kaitpas/status' do
		begin
			load_kaitpas_meters_status

			erb :"kaitpas-ms"
		rescue
			$logger.error $!.message
			$logger.ap $@[0..4]
		end
	end

	#
	# Get bam meter status mnemoscheme
	#
	get '/ms/bam/status' do
		begin
			load_bam_meters_status
			erb :"bam-ms"
		rescue 
			$logger.error $!.message
			$logger.ap $@[0..4]
		end
	end

	#
	# Get low otrar status mnemoscheme
	#
	get '/ms/notrar/status' do
		begin
			load_notrar_meters_status
			erb :ms_notrar_status
		rescue 
			$logger.error $!.message
			$logger.ap $@[0..4]
		end
	end


	#
	# Work manometer animation: all work manometers
	#
	get '/manometer/work/all' do
		@scale_x = 0.75
		@scale_y = 0.75
		@manometer_width = 300*@scale_x 
		@manometer_height = 350*@scale_y 
		erb :'manometer-work-all'
	end

	#
	# Work manometer animation: all work manometers
	#
	get '/manometer/work/dis' do
		@scale_x = 0.75
		@scale_y = 0.75
		@manometer_width = 300*@scale_x 
		@manometer_height = 350*@scale_y 
		erb :'manometer-work-dis'
	end

	#
	# Work manometer animation: akbay manometers
	#
	get '/manometer/work/partial' do
		@scale_x = 0.85
		@scale_y = 0.85
		@manometer_width = 300*@scale_x 
		@manometer_height = 350*@scale_y 


		@partial_cp_codes = [55555, 9904014, 400960, 9903999, 861922, 9902405, 100177, 123001, 123003, 100190, 100150]
		@partial_cps = $cps.select{|cp| @partial_cp_codes.include? cp.address }
		erb :'manometer-work-partial'
	end

	#
	# Get json manometer value 
	#
	post '/meter/:code' do
		@cp_data_rcv.get_cpoint_values([params[:code]]).to_json
	end

	get '/meter/:code' do
		@cp_data_rcv.get_cpoint_values([params[:code]]).to_json
	end

	#
	# Get json manometers values
	#
	post '/meters/:codes' do
		begin
			vals = @cp_data_rcv.get_cpoint_values( params[:codes].split(',') )
			vals.to_json
		rescue
			$logger.error $!.message
			$logger.ap $@[0..4]
			ap $!.message
			ap $@[0..4]
		end
	end

	get '/pressure/meters' do
		begin
			@cp_data_rcv.get_cpoint_values( $cps.map{|cp| cp.address } ).to_json
		rescue
			$logger.error $!.message
			$logger.ap $@[0..4]
			ap $!.message
			ap $@[0..4]
		end
	end

	get '/flowrate/meters' do
		begin
			@cp_data_rcv.get_values_without( $cps.map{|cp| cp.address } ).to_json
		rescue
			$logger.error $!.message
			$logger.ap $@[0..4]
			ap $!.message
			ap $@[0..4]
		end
	end

	#
	# Get json manometers values
	#
	get '/meters/:codes' do
		begin
			$logger.debug params
			vals = @cp_data_rcv.get_cpoint_values( params[:codes].split(',') )
			vals.to_json
		rescue
			$logger.error $!.message
			$logger.ap $@[0..4]
			ap $!.message
			ap $@[0..4]
		end
	end


    #
    # Edit modes of control points
    #
    get '/edit-modes' do

        redirect '/auth/login' unless env['warden'].authenticated?

        @current_user = env['warden'].user

        erb :'edit-modes'
    end

    get '/auth/login' do
        erb :login2
    end

    post '/auth/login' do
        env['warden'].authenticate!

        if session[:return_to].nil?
            redirect '/edit-modes'
        else 
            redirect session[:return_to]
        end 
    end

    get '/auth/logout' do
        env['warden'].raw_session.inspect
        env['warden'].logout
        redirect '/'
    end

    post '/auth/unauthenticated' do
        session[:return_to] = env['warden.options'][:attempted_path]
        puts env['warden.options'][:attempted_path]
        redirect '/auth/login'
    end


	#
	# View ergomera values
	#
	get '/ergomera' do
		begin
			@ergo_name = { :kabisko => "Кабиско", :karasu => "Карасу 3", :kyzylsay => "Кызыл-Сай" }
			@ergo_vals = @cp_data_rcv.get_ergomera_vals

			erb :ergomera_view
			#@ergo_vals.to_s
		rescue
			$logger.error $!.message
			$logger.ap $@[0..4]
			ap $!.message
			ap $@[0..4]
		end
	end

	post '/pulsar' do
		begin
			$logger.debug "Params #{params}"
			query_body = request.body.read
			$logger.debug query_body

			json_body = JSON.parse query_body
			@pulsar_saver ||= PulsarDataSaver.new
			@pulsar_saver.logger = $logger
			if @pulsar_saver.store( json_body ) 
				[200,"Pulsar data saved"]
			else
				[500, @pulsar_saver.last_error]
			end
		rescue
			$logger.error 'Error:: ' + $!.message
			ap $@[0..7]
			$logger.error 'Message:: ' + $@[0..12]
		end
	end


	get '/latch_pressure' do
		status 200
		body LatchManager.get_pressure.to_s
	end

	get '/latch_pressure_lim' do
		status 200
		body LatchManager.get_pressure_lim.to_s
	end

	# == Akbay wells flowmeters mnemoscheme
	#
	get '/fm-akbay-wells' do
		begin
				@cp_data_rcv.read_flow_meters_table( NO_PROJECT, $akbay_wells_fm)

				last_modified Time.now

				erb :fm_akbay_wells
		rescue
			ap $!.message
			ap $@[0..5]
			$logger.error $!
			$logger.info $@
		end
	end

	get '/fm-akbay-wells-ms' do
		begin
				@cp_data_rcv.read_flow_meters_table( NO_PROJECT, $akbay_wells_fm)

				last_modified Time.now

				erb :fm_akbay_wells_ms
		rescue
			ap $!.message
			ap $@[0..5]
			$logger.error $!
			$logger.info $@
		end
	end

	set :sockets, []
	set :tank_sockets, []

	# == Akbay flowmeters params == #
	#
	get '/akbay_meter' do
			status 200
			erb :akbay_ms
	end

	VALUE_FLOWMETER_SUM_VALUE = 1
	VALUE_FLOWMETER_CURFLOW = 2

	post '/akbay_meter' do
		reqbody = request.body.read

		json = JSON.parse reqbody

		date = DateTime.parse(json['date'])
		value = json['value'].to_i
		meter = json['meter'].to_i
		value_kind = json['value_kind'].to_i

		# save only sum value (current value does not saved) #
		if( value_kind == VALUE_FLOWMETER_CURFLOW )
			save_cpoint_meter( meter, value, date )
		end

		status 200
		body "post akbay meter state ::#{value_kind}:#{value}"
	end

	# == Sever tank params == #
	#
	get '/tank_param' do
		cross_origin
		tank_code = params["code"].to_i
		tank_command =  params["command"].to_i

		res = read_last_tank_param( tank_code, tank_command )

		status 200
		body res.to_s
	end

	get '/tank_archive/last' do
		cross_origin 
		tank_code = params['code'].to_i
		last_count = params['count'].to_i

		res = read_last_tank_archive( tank_code, last_count )

		status 200
		body res.to_s
	end

	post '/tank_param' do
		cross_origin

		save_tank_params(params)

		puts "Sending new tank param: to #{settings.tank_sockets.size} clients"
		ap params

		status 200
		body "Tank params received: #{params}"
	end


	get '/akbayms' do
		erb :akbayms
	end

###########
## TOOLS ##
###########
	def save_cpoint_meter( meter, value, date )
		saver = CPointSaver.new
		saver.store_meter( meter, value, date )
	end


	def filter_by_date interval_kind, vals

		res_vals = 
		case interval_kind 

		when INTERVAL_ALL_VALUES then vals

		when INTERVAL_DAY_VALUES then 

			first_dates = {}
			vals.sort{|a,b| a[:datetime] <=> b[:datetime] }.each do |v|
				first_dates[ v[:datetime] ] ||= v[:value]
			end
		
			first_dates.map{|f| {:datetime => f[0], :value => f[1]}} 

		when INTERVAL_HOUR_VALUES then vals

		when INTERVAL_HALF_HOUR_VALUES then vals

		end
	end


	def change_date_range_for_period_kind

			case @period_kind 
			when PERIOD_ANY then # do nothing
			when PERIOD_CURRENT_DAY then 
				@date_start = DateTime.now.to_date
				@date_end = DateTime.now.to_date + 1
			when PERIOD_PREVIOUS_DAY then
				@date_start = DateTime.now.to_date - 1
				@date_end = DateTime.now.to_date
			when PERIOD_CURRENT_MONTH then
				@date_start = Date.new( DateTime.now.year, DateTime.now.month, 1 ) 
				@date_end = Date.new( DateTime.now.year, DateTime.now.month, -1 )
			when PERIOD_PREVIOUS_MONTH then
				@date_start = Date.new( DateTime.now.year, DateTime.now.month - 1, 1 )
				@date_end = Date.new( DateTime.now.year, DateTime.now.month - 1, -1 )
			end
	end

	def interval_title( interval_kind )
		@fm_data_rcv.interval_caption( interval_kind )
	end

	###############
	## ERROR HANDLER ##
	###############
	error do
		@error = env['sinatra.error']
		$logger.error @error.inspect
		$logger.ap caller.join("\n")

		"Sorry. Internal server error occured \n" 
	end

	not_found do
		"Sorry we can not found that page"
		$logger.error $!.to_s
	end

	private

	#####################   
	## PRIVATE SECTION ##
	####################

	#
	#	== Load the status of bam meters
	#
	def load_bam_meters_status
		bam_meters_h = load_cached_flowmeters(PROJ_KAITPAS,DateTime.now-1, DateTime.now)

		@bam_meters = {}
		@bam_meters = Hash[*bam_meters_h.map{|m| [m.address,m]}.flatten!]  unless bam_meters_h.nil?

		@bam_meters.default = NullFlowMeter.instance 
	rescue 
		$logger.error $!.message
		$logger.ap $@[0..4]
	end

	#
	#	== Load the status of notrar meters
	#
	def load_notrar_meters_status
		@notrar_meters = Hash[*load_cached_flowmeters(PROJ_KAITPAS,DateTime.now-1, DateTime.now).map{|m| [m.address,m]}.flatten!] 
		@notrar_meters.default = NullFlowMeter.instance 
	rescue 
		$logger.error $!.message
		$logger.ap $@[0..4]
	end
	#todo: commonize meters status receival code


	#
	# == Load the status of taraz meters
	#
	def load_taraz_meters_status

		meters = load_cached_flowmeters(PROJ_KAITPAS,DateTime.now-1, DateTime.now)
		meters_hash = Hash[*meters.map{|m| [m.address,m]}.flatten!]

		@taraz_meters = meters_hash 
		@taraz_meters.default = NullFlowMeter.instance 
	rescue 
		$logger.error $!.message
		$logger.ap $@[0..4]
	end
	#
	# == Load the status of kaitpas meters
	#
	def load_kaitpas_meters_status

		meters = load_cached_flowmeters(PROJ_KAITPAS,DateTime.now-1, DateTime.now)

		if (meters.class == Hash)
			meters_hash = Hash[*meters.map{|m| [m.address,m]}.flatten!]

			@kaitpas_meters = meters_hash 
			$logger.error "Received nil meters from kipq4s db" if meters_hash.nil?
		end

		@kaitpas_meters = {} if @kaitpas_meters.nil?

		@kaitpas_meters.default = NullFlowMeter.instance 

    
	rescue 
		$logger.error $!.message
		$logger.ap $@[0..4]
	end

	# == Load the ergomera meters from db
	#
	def load_ergomera_meters
		@base_fm = @cp_data_rcv.read_energomera_meters_table() 
		ap @base_fm.inspect
	rescue 
		ap $!.message
		ap $@[0..6]

		$logger.error $!.message
		$logger.ap $@[0..14]
	end

	# == Load the base meters from db
	#
	def load_base_meters
		@base_fm = @cp_data_rcv.read_flow_meters_table( PROJ_METERS )

	rescue 
		$logger.error $!.message
		$logger.ap $@[0..4]
	end

	# == Get cached flowmeters for project
	#
	def get_cached_flowmeters( proj_id )
		load_cached_flowmeters( proj_id, get_parms_date_start, get_parms_date_end )
	rescue 
		$logger.error $!.message
		$logger.ap $@[0..4]
	end


	# == Load cached flowmeter for date range and project
	#
	def load_cached_flowmeters( proj_id, date_start, date_end )

		@fm_data_rcv.date_start = date_start
		@fm_data_rcv.date_end = date_end 

		@meter_data = []

		@cached_main_fm = @fm_data_rcv.load_cached_flowmeters( proj_id, "%d-%m-%Y %H:%M")

		@cached_main_fm.each_with_index do |meter, i|

			@meter_data << meter
		end

		@meter_data
	rescue 
		$logger.error $!.message
		$logger.ap $@[0..4]
	end

	# == Calculate consumption for flowmeters
	#
	def calc_consumption( fmeters )
		# get meter vals
		vals = @rep_main_fm.map{|m| 
			{datetime: m.values[0].datetime, 
    value: m.values[0].value}}

		#calculate consumption
		tvals = []
		vals.each_with_index do|v,i| 
			tv = {}
			tv[:datetime] = v[:datetime]
			tv[:value] = v[:value] - vals[i-1][:value] if i > 0
			next if i == 0

			tvals << tv
		end

		tvals
	rescue 
		$logger.error $!.message
		$logger.ap $@[0..4]
	end

	# == Receive date start from params
	#
	def get_parms_date_start
		DateTime.new(params[:ev][:start][:y].to_i, 
			 params[:ev][:start][:m].to_i, 
			 params[:ev][:start][:d].to_i,
			 params[:ev][:start][:h].to_i,
			 params[:ev][:start][:i].to_i
		)

	rescue
		return DateTime.now.to_date
		$logger.ap $@[0..4]
	end

	# == Receive date end from params
	#
	def get_parms_date_end
		DateTime.new(params[:ev][:end][:y].to_i, 
			 params[:ev][:end][:m].to_i, 
			 params[:ev][:end][:d].to_i,
			 params[:ev][:end][:h].to_i,
			 params[:ev][:end][:i].to_i
			 )

	rescue => error
		return DateTime.now.to_date
		$logger.ap $@[0..4]
	end

	CURRENT_LEVEL_COMMAND = 1
	OVERFLOW_STATE_COMMAND = 2
	EMPTY_STATE_COMMAND = 3
	INVALID_COMMAND = 4
	POWER_DOWN_COMMAND = 5
	POWER_UP_COMMAND = 6

	def read_last_tank_archive( tank_code, rec_count)
		db = connect_to_tanks_db

		vals = db[:tank_states].where(tank_id: tank_code).order(:date_stored).last(rec_count)
		db.disconnect

		vals
	end

	# == Read last tank params
	# Find param by tank code and command, sorted by date (last one)
	# Params:
	#  * tank_code - from which to find values
	# Return: result in format {too_aged: true} - if more than 10 minutes of reading, {too_aged: false, value: 1} - if has value, 
	#		for alarms: 1 - set, for level - integer value of level
	def read_last_tank_param( tank_code, command )

		case command 
			when CURRENT_LEVEL_COMMAND
				read_last_current_level( tank_code )
			when OVERFLOW_STATE_COMMAND
				read_last_overflow_alarm( tank_code )
			when EMPTY_STATE_COMMAND
				read_last_empty_alarm( tank_code )
			when POWER_UP_COMMAND 
				read_last_command_with_time_status( tank_code, POWER_UP_COMMAND )
			when POWER_DOWN_COMMAND 
				read_last_command_with_time_status( tank_code, POWER_DOWN_COMMAND )
		end
	end

	NORMAL = 0
	STALE = 1
	INVALID = 2

	def read_tank_state( tank_code, state_id )
		db = connect_to_tanks_db
		vals = db[:tank_states].where(tank_id: tank_code, state_command_id: state_id).order(:date_stored).reverse.first
		db.disconnect

		vals
	end

	# == Read current level in last 10 minutes and return it
	# Info: if level is received more than 10 minutes, return it with flag
	#		STALE, else NORMAL
	#
	# Params:
	#		* tank_code - code of tank in db
	#	Returns: [level_value, flag, date_query]
	#		where level_value - float value of level (1-6 position)
	#					flag - NORMAL (if in 10 minutes), STALE if later
	#					date_query - date of last level storage
	def read_last_current_level( tank_code )
#todo: check if I can use it as read_last_command_with_time_status but without last check
		vals = read_tank_state( tank_code, CURRENT_LEVEL_COMMAND ) 

		if( vals == nil )
			return [-1, INVALID, DateTime.now.strftime("%Y-%m-%d %H:%M:%S"), tank_code]
	  end

		time_diff = TimeDifference.between(DateTime.now, vals[:date_stored] ).in_minutes
		if( time_diff < 3 )
						return [vals[:value_int], NORMAL, vals[:date_stored].strftime("%Y-%m-%d %H:%M:%S"), tank_code]
		else
						return [vals[:value_int], STALE, vals[:date_stored].strftime("%Y-%m-%d %H:%M:%S"), tank_code]
		end
	end

	ON = 1
	OFF = 0

	# == Read last command value and check if it was stale
	#
	# Info: Command can be received in past. So 0-2 minutes - is fresh data
	#			2-5 is stale data, and >5 is irrelevant and can be off (for 
	#			commands, that have no mirror counterpart on/off) and can be off
	#
	# Params:
	#			tank_code - for which need to get command status
	#			command_code - which command to check (level, low, alarm, off elec)
	#			time_normal - until data is fresh
	#			time_stale - until data is stale but command is On
	#
	def read_last_command_with_time_status( tank_code, command_code, time_normal, time_stale )

		vals = read_tank_state( tank_code, command_code ) 

		if( vals == nil )
			return [OFF, INVALID, DateTime.now.strftime("%Y-%m-%d %H:%M:%S")]
	  end

		time_diff = TimeDifference.between(DateTime.now, vals[:date_stored] ).in_minutes

		if( time_diff < time_normal )
			[ON, NORMAL, vals[:date_stored].strftime("%Y-%m-%d %H:%M:%S"), tank_code]
		else
			[ON, STALE, vals[:date_stored].strftime("%Y-%m-%d %H:%M:%S"), tank_code]
		end
	end

	# == Read last overflow alarm
	# Info: if overflow alarm is set in 5 minutes, then return it else return false
	# Returns: true - alarm set, false - no alarm
	def read_last_overflow_alarm( code )
		read_last_command_with_time_status( code, OVERFLOW_STATE_COMMAND, 2, 5 )
	end

	# == Read last empty alarm
	# Info: if empty alarm is set in 5 minutes, then return true
	# Returns: true - alarm is set, false - no alarm
	def read_last_empty_alarm( code )
		read_last_command_with_time_status( code, EMPTY_STATE_COMMAND, 2, 5 )
	end

	# == Save tank params
	#  format: params = {code:1, command:1, value:4}
	#  where
	#		code - of tank
	#		command - (1) level of tank, (2) high level (overflow), (3) low level (empty tank)
	def save_tank_params( params )
		tank_db = connect_to_tanks_db()

		code = params["code"].to_i
		command = params["command"].to_i
		val_int = params["value"].to_i

		tank_db[:tank_states].insert( tank_id: code, date_stored: DateTime.now, state_command_id: command, value_int: val_int )

		tank_db.disconnect
	end


	# == Connect to tanks on cpoint
	#
	def connect_to_tanks_db
		Sequel.tinytds(
			user: 'puser', password: 'Win3Ts', host: '192.168.10.8', database: 'tanks', encoding: 'cp1251' 
			)
	end



	# == Load the settings from yaml config
	#
	def load_cfg
		@cfg = YAML.load_file("#{File.dirname(__FILE__)}/cfg/ms.cfg.yaml")
	end


	def read_modes_from_service

		uri = URI.parse('http://92.47.27.114:3000/last/cpmodes.json')
		http = Net::HTTP.new( uri.host, uri.port )
		request = Net::HTTP::Get.new(uri.request_uri)

		http.request( request )
	end

end
