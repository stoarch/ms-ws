    require 'monitor'
     
    class Timer
       def initialize(interval, run_from_start, join_from_start,  &handler)
         raise ArgumentError, "Interval less than zero" if interval < 0
         extend MonitorMixin
         @handler = handler
         @interval = interval
         @run_from_start = run_from_start
         @join_from_start = join_from_start
       end
     
       def start
        return if @run
        @run = true
        @th = Thread.new do
          @handler.() if @run_from_start
          t = Time.now
          while run?
            t += @interval
            (sleep(t - Time.now) rescue nil) and @handler.call rescue nil
         end
        end
        @th.join if @join_from_start
       end
       
       def stop
         synchronize do
           @run = false
         end
         @th.join
       end

       def join
         @th.join if @th
       end
     
       private
     
       def run?
         synchronize do
           @run
         end
       end
    end

