require 'singleton'

require_relative "flowmeter"

class FlowMeterValue
  attr_accessor :value, :datetime
  attr :meter

  def initialize( meter )
    @meter = meter
  end

  class << self
    def make( args )
      fmv = FlowMeterValue.new( args[ :meter ] )
      fmv.value = args[:value]
      fmv.datetime = args[:datetime]
      fmv
    end
  end

  def to_s
	"(value: #{value} date: #{datetime.strftime('%Y-%m-%d %H-%M') if datetime.is_a? DateTime} #{datetime if datetime.is_a? String} #{datetime.to_datetime if datetime.is_a? Time} #{datetime.class.name})"
  end
end


class NullFlowMeterValue < FlowMeterValue
  include Singleton

  def initialize
    @meter = nil #NullFlowMeter.instance
    @value = -1
    @datetime = 0
  end

  def value=(val)
  end

  def datetime=(val)
  end

  def value
    0.0
  end

  def datetime
    DateTime.now
  end
end

# initialize singleton

NullFlowMeterValue.instance

