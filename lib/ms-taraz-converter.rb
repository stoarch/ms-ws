# encoding: utf-8




LOW = 0
HIGH = 1


#*************************************************#
# METERS
#*************************************************#

#define meter codes
$meters = {
	11600349=> {color: 'ddbbee'},
	7164830=> {color: 'ddbbed'},
	11331138=> {color: 'ddbbec'},
	11347054=> {color: 'ddbbeb'},
	11347054=> {color: 'ddbbeb'},
	11331015=> {color: 'ddbbea'},
	7169819=> {color: 'ddbbe9'},
	11601777=> {color: 'ddbbe8'},
	5699432=> {color: 'ddbbe7'}
}




# load csv file (param str 0)
source_file = ARGV[0]

puts "Converter from svg to erb\n"
puts "Loading file..."
source_string = File.open( source_file, 'r+:utf-8' ){|f| f.read }


# convert file

puts "\nConvert meters..."

$meters.each_pair do |m,v|
	puts "\nConvert #{m}..."
	source_string.gsub!( v[:color], "<%= 'DED71F' if @taraz_meters[#{m}].is_counter_stoped?%><%= 'DE1414' if @taraz_meters[#{m}].is_counter_stalling?%><%= '2023D6' if @taraz_meters[#{m}].is_normal_state?%>" )
end



# export erb file (param str 1)
puts "\n Export result file"

dest_file = ARGV[1]
File.open( dest_file, 'w+' ){|f| f.write( source_string ) }

puts "Done."
