# encoding: utf-8
#
require 'base64'
require 'logger'
require 'gerbilcharts'
require 'rsvg2'
require 'sequel'
require 'yamler'
require 'active_support/core_ext/numeric'
require 'ostruct'
require 'awesome_print'
require 'time_diff'
require 'interpolator'
require 'vmstat'

require_relative 'cpoint'
require_relative 'cpoint-data-receiver'
require_relative 'flowmeter'
require_relative 'flowmeter-data-receiver'
require_relative 'flowmeter_value'
require_relative 'flowmeter_value_range'
require_relative 'sql_strings'
require_relative 'os-module'
require_relative 'timer'
require_relative 'graph_calculator'

###############
## CONSTANTS ##
###############

ONCE_PER_HOUR = 3600
RUN_FROM_START = true
NEED_PNG = true
JOIN_FROM_START = true

if OS.linux? || OS.mingw?
	YAML::ENGINE.yamler = 'syck'
end #


AwesomePrint.defaults = {
	plain: true
}

###############
## FUNCTIONS ##
###############

#
# == Load the settings from yaml config
#
def load_cfg
	YAML.load_file("#{File.dirname(__FILE__)}/cfg/ms.cfg.yaml")
end


#
# == Refresh control point graphs
#
def refresh_cpoints_graphs
	puts "\nrefresh_cpoints_graphs: started..."
	$logger.info "\nrefresh_cpoints_graphs: started..."

	ec1 = Encoding::Converter.new "UTF-8","Windows-1251",:invalid=>:replace,:undef=>:replace,:replace=>""
	ec2 = Encoding::Converter.new "Windows-1251","UTF-8",:invalid=>:replace,:undef=>:replace,:replace=>""


	$cfg ||= load_cfg
	$cp_data_rcv ||= CPointDataReceiver.new( $cfg )

	$cps.each do |cp|
		begin

			puts "Handling cp #{ec2.convert ec1.convert cp.caption}"
			$logger.info "Handling cp #{ec2.convert ec1.convert cp.caption}"

			cur_cp_day_vals = $cp_data_rcv.load_day_vals_for( cp.address ).map{|v| v[:value] = v[:value].round(1); v}

			@graph_calculator ||= GraphCalculator.new

			@graph_calculator.input = cur_cp_day_vals
			@graph_calculator.execute

			generate_graph_for  cp.caption, @graph_calculator.output, "#{cp.address}", NEED_PNG

		rescue Exception => ex
			puts "Error: #{ex.message}"
			$logger.error ex.message
			$logger.error $@
		end
	end

	puts "\nrefresh_cpoints_graphs: complete"
	$logger.info "\nrefresh_cpoints_graphs: complete"
end


# == Generate graph for specified table
# @parms
#   caption - of graph
#   vals - array of hashes (:datetime, :value)
#   file_name - to write
#   need_png - if we need to compile
#	width, height - dimension of image to generate (default: 600x400)
#
def generate_graph_for( caption , vals, file_name, need_png = false, width = 600, height = 400 )    

	mychart = GerbilCharts::Charts::LineChart.new( 
						      :width => width, 
						      :height => height, 
						      :style => 'embed:brushmetal.css'
						     )

	model = GerbilCharts::Models::TimeSeriesGraphModel.new(:title => caption)

	vals = vals.map{|v| [v[:datetime].to_time, v[:value]] }

	model.add_tuples vals

	group = GerbilCharts::Models::GraphModelGroup.new(caption)
	group << model

	mychart.modelgroup=group
	mychart.render("./static/images/#{file_name}.svg")

	convert_to_png("./static/images/#{file_name}") if need_png
rescue 
	$logger.error $!.message
	$logger.ap $@

	ap $!.message
	ap $@
end


# == Convert to png specified svg file
#
def convert_to_png( file_name )

	svg = RSVG::Handle.new_from_file( file_name + ".svg" )
	surface = Cairo::ImageSurface.new(Cairo::FORMAT_ARGB32, 600, 400 )
	context = Cairo::Context.new(surface)
	context.render_rsvg_handle(svg)
	surface.write_to_png("#{file_name}.png")

rescue 
	$logger.error $!.message
	$logger.ap $@

	ap $!.message
	ap $@
ensure
	svg.close

	context = nil
	svg = nil
	surface = nil
end

####################
## INITIALIZATION ##
####################

$logger = Logger.new('cpgrr.log', 'daily', :log_level => :info)

if OS.linux?
	cp_cfg = File.read("#{File.dirname(__FILE__)}/cfg/points.cfg.yaml")[5..-1]
	$points = YAML.load( StringIO.new(cp_cfg ))
elsif OS.windows? || OS.mingw?
	$points = YAML.load( File.read("#{File.dirname(__FILE__)}/cfg/points.cfg.yaml"))
else
	puts "Error: Unsupported OS"
	exit
end

$cps = $points.to_a.flatten!.each_slice(2).to_a.map do |key, caption| 
	key = key[5..-1] if key =~ /---/

	CPoint.new( :address => key, :caption => caption ) 
end


###############
## MAIN LOOP ##
###############

$logger.info "Refresher started"
begin
	refresh_cpoints_graphs
rescue
	$logger.error $!.to_s
	$logger.info $@

	ap $!.message
	ap $@
end

$logger.info "Refresher finished"

exit

