# -*- coding: utf-8 -*-
#
#  class: FlowMeterDataReceiver
#
#  goal: Read flowmeter data values from KIPQ4S server
#
#  methods:
#		* connect_to_meters_db - connect to kipq4s server sql2000 db 
#			(synonym: connect_to_kipq4s_db)
#		* connect_to_dest_db - connect to cpoint server sql2000 db
#			(synonym: connect_to_cpoint_db)
#		* decrypt - decode values from config (passwords)
#		* read_meter - read meter (min,max) values from @date_start to @date_end
#		* read_meter_for_period - set period from kind and read meter values (min,max)
#		* make_meter_for - read values from db hash and create FlowMeter by it
#		* get_meter_info - find meter in db and return it as FlowMeter 
#		* get_meter_dict - find all meters from project and return it as db hash 
#		* get_meter_dict_with_root - find all meters from project that have integer sn  
#		* read_flow_meters_table - 
class FlowMeterDataReceiver
  attr_accessor :date_start, :date_end
  attr_accessor :project_id
  attr :fmeters
  

  def initialize( cfg )
    @cfg = cfg
  end



    # == Connect to cpoint meters db
    #
    def connect_to_meters_db
      @meters_db_cache ||= 
        Sequel.tinytds(
					username: 'kipserver', password: 'Win3Ts',
					dataserver: '192.168.10.17',
					database: 'counters',
					encoding: 'cp1251'
				)
    end



    # == Connect to dest db
    #
    # Configuration specifiec which db to use
    #
    def connect_to_dest_db
      @dest_db_cache ||=
        Sequel.tinytds(
                :user=>'puser',
                :password=>'Win3Ts',
                :host=>'cpoint',
								:port=>1433,
                :database=>'counters',
                :encoding=>"cp1251")
    end

    def connect_to_cpoint_db
      @dest_db =
        Sequel.tinytds(
                :user=>'puser',
                :password=>'Win3Ts',
                :host=>'cpoint',
								:port=>1433,
                :database=>'counters',
                :encoding=>"cp1251")
    end

		def connect_to_kipq4s_db
      @dest_db ||=
        Sequel.tinytds(
                :user=>'kipserver',
                :password=>'Win3Ts',
                :host=>'192.168.10.17',
								:port=>1433,
                :database=>'counters',
                :encoding=>"cp1251")
		end

    # == Decrypt the value
    #
    # @param
    #   val - encrypted value
    #
    # @return
    #   unencrypted value
    #
    def decrypt( val, cfg_part = "dest" )
      c = OpenSSL::Cipher::Cipher.new("aes-256-cbc")

      c.decrypt
      c.key = @cfg[cfg_part]["key"]
      c.iv = Base64.decode64( @cfg[cfg_part]["iv"] )

      uval = Base64.decode64( val )

      res = c.update( uval )
      res << c.final

      res
    end



     # == Read the meter date start/end vals
     #
     def read_meter( meter, proj_id = 33 )
        sql1 = SQLStrings.max_meter_values
        sql2 = SQLStrings.min_meter_values

        @dest_db ||= connect_to_dest_db()
        @main_fm = load_flowmeters( proj_id )


        val_max = @dest_db[sql1,date_end.strftime("%Y-%m-%d"), @date_start.strftime("%Y-%m-%d"), meter.address].all

        val_min = @dest_db[sql2,date_end.strftime("%Y-%m-%d"), @date_start.strftime("%Y-%m-%d"), meter.address].all

				@dest_db.disconnect unless @dest_db.nil?
				@dest_db = nil

        vals = []
        dates = []

        # order must be saved [min,max] 

        if val_min && ( val_min.size > 0 )
          vals << val_min[0][:value]
          dates << val_min[0][:dt].strftime("%d-%m-%Y")
        else
          vals << 0
          dates << @date_start.strftime("%d-%m-%Y")
        end

      if val_max && ( val_max.size > 0 )
        vals << val_max[0][:value]
        dates << val_max[0][:dt].strftime("%d-%m-%Y")
      else
        vals << 0
        dates << date_end.strftime("%d-%m-%Y")
      end


      meter.values << FlowMeterValue.make( :meter => meter, :value => vals[0], :datetime => dates[0] )
      meter.values << FlowMeterValue.make( :meter => meter, :value => vals[1], :datetime => dates[1] )

      meter
   end


   # == Read meter min/max data for specified period
   #
   def read_meter_for_period( meter, proj_id, period_kind )

     @date_start, @date_end = parse_period_kind( period_kind )

     read_meter( meter, proj_id )
   end


   # == Make meter from db meter hash
   #
   def make_meter_for( dbm )

     return NullFlowMeter.instance if dbm.nil?

     FlowMeter.new( 
                   address: dbm[:sn].to_i, 
                   caption: "#{dbm[:city]} \t- #{dbm[:street]} \t- #{dbm[:house]} \t- #{dbm[:flat]}".force_encoding("cp1251").encode("UTF-8"))   
   end   
   

   # == Load the meter info (caption)    
   #
   def get_meter_info( meter_id, proj_id = 33 )

     @dest_db ||= connect_to_dest_db

     make_meter_for(
         @dest_db[ :counters ]
           .join( :clients, client_id: :client_id )
           .select( :sn, :city, :street, :house, :flat )
           .filter('clients.project_id = ?', proj_id )
           .filter( sn: meter_id )
           .first
    )
				@dest_db.disconnect unless @dest_db.nil?
				@dest_db = nil
   end


   # == Load the dictionary of meters
   #
   #  Receive the array of hashes (for each row) of meter ids and captions
   #
   def get_meter_dict( proj_id = 33 )

     @dest_db ||= connect_to_dest_db

     @dest_db[:counters]
       .join(:clients, :client_id => :client_id)
       .select(:sn, :city, :street, :house, :flat )
       .filter('clients.project_id = ?', proj_id)
       .all

				@dest_db.disconnect unless @dest_db.nil?
				@dest_db = nil
   end


   # == Get meters dict with additional info 
   #
   def get_meter_dict_with_root( proj_id )
	 begin

     @dest_db ||= connect_to_dest_db

		 vals = @dest_db['
						 select *, [MAINCOUNTER_ID], [ADDRESS]  from
						 (
							SELECT 
							(select case when isNumeric([SN]) = 1 then cast([SN] as int) else NULL end) as SNV, 
							[CITY], [STREET], [HOUSE], [FLAT]
							FROM [COUNTERS] 
							INNER JOIN [CLIENTS] ON ([CLIENTS].[CLIENT_ID] = [COUNTERS].[CLIENT_ID]) 
							where (clients.project_id = ?)
						 ) as VAL, 
						 ParamAccounts
						 WHERE 
						 (Val.SNV = ParamAccounts.ADDRESS )', proj_id]
						 .all
#todo: Move to sqlstrings

				@dest_db.disconnect unless @dest_db.nil?
				@dest_db = nil

     vals.map! do |v| 
       {
         :sn => v[:snv].to_i,
         :city => v[:city],
         :street => v[:street],
         :house => v[:house],
         :flat => v[:flat],
         :maincounter_id => v[:maincounter_id].to_i,
         :address => v[:address]
       }
     end

     vals
		 rescue 
			ap $!.message
			ap $@[0]

			 $logger.error $!.message
			 $logger.ap $@[0..4]
			 nil 
		 end
   end


   # == Load the table of flowmeters values for current day
   #
   # @params
   #   proj_id - from which flowmeters to load
   # @return
   #   array of pairs [flowmeter, array of values]
   #
   def read_flow_meters_table( proj_id = 33, fms = $fm )

     @db = connect_to_meters_db

     @fmeters = []

     fms.each do |fm|

       vals = @db[:_data].filter( :address => fm.address ).order(:datetime).last(10)

       fm.values.clear

       @fm = fm
       
       prev_val = 0

       vals.each_with_index do |v, i|
         if i == 0
             prev_val = v[:value]
             next
         end

         @fm.values << FlowMeterValue.make( :meter => @fm, 
                                            :datetime => v[:datetime], 
                                            :value => prev_val - v[:value] )
         prev_val = v[:value]
       end
  
       @fmeters << @fm
     end

		 @db.disconnect unless @db.nil?
		 @db = nil

     @fmeters
    ensure
     @db.disconnect if @db
     @db = nil
   end  



   # == Load and cache the flow meters data from db
   #
   # The flow meters loaded with their data for period @date_start..@date_end and
   # stored internally for more fast processing for report of flowmeters for
   # specified project id.
   #
   # Data contains all project meter consumption for specified period
   #
   def load_cached_flowmeters( proj_id = 33, date_format = "%d-%m-%Y" )

#todo: refactor this long method

     @dest_db ||= connect_to_dest_db()

     meter_dict = get_meter_dict( proj_id )

     meter_codes = meter_dict.map{|data| data[:sn].to_i }

     data_dict = @dest_db[:_data]
       .filter('datetime between ? and ?', @date_start, @date_end)
       .where(:address => meter_codes)
       .select(:address, :value, :datetime)
       .to_hash_groups(:address)

				@dest_db.disconnect unless @dest_db.nil?
				@dest_db = nil


     @meters = []

     meter_dict.each do |dbm|
       id = dbm[:sn].to_i

       meter =  make_meter_for( dbm )

       @meters << meter

       vals = []
       dates = []

        meter_vals = data_dict[id]

        if meter_vals.nil?

          vals << 0 << 0
          dates << @date_start << @date_end
        else

            val_min = meter_vals.min{|a,b| a[:datetime] <=> b[:datetime] }
            val_max = meter_vals.max{|a,b| a[:datetime] <=> b[:datetime] }

            # order must be saved [min,max] 

            if val_min && ( val_min.size > 0 )

              vals << val_min[:value]
              dates << val_min[:datetime]
            else

              vals << 0
              dates << @date_start
            end

            if val_max && ( val_max.size > 0 )

              vals << val_max[:value]
              dates << val_max[:datetime]
            else

              vals << 0
              dates << @date_end
            end
        end

      meter.values << FlowMeterValue.make( :meter => meter, :value => vals[0], :datetime => dates[0] )
      meter.values << FlowMeterValue.make( :meter => meter, :value => vals[1], :datetime => dates[1] )     
     end

     @meters.sort!{|a,b| a.address <=> b.address }
   end



    CURRENT_DAY = 0
    PREVIOUS_DAY = 1
    CURRENT_MONTH = 2
    PREVIOUS_MONTH = 3
    UNIVERSAL_PERIOD = 4

   ALL_VALS = 0
   HALFHOUR_VALS = 1
   HOUR_VALS = 2
   DAILY_VALS = 3

   # == Parse representation of date period kind to date pair
   #
   def parse_period_kind( period_kind )
     cur_date = DateTime.now.to_date

     case period_kind
       when CURRENT_DAY
         date_start = cur_date
         date_end = cur_date + 1

       when PREVIOUS_DAY
         date_start = cur_date - 1
         date_end = cur_date

       when CURRENT_MONTH
         date_start = Date.new( cur_date.year, cur_date.month, 1 ).to_datetime
         date_end = Date.new( cur_date.year, cur_date.month, -1 ).to_datetime
         
       when PREVIOUS_MONTH
         date_start = Date.new( cur_date.year, cur_date.month - 1, 1 ).to_datetime
         date_end = Date.new( cur_date.year, cur_date.month - 1, -1 ).to_datetime #last day of month

       when UNIVERSAL_PERIOD
         date_start = self.date_start
         date_end = self.date_end + 1
     end

   return date_start, date_end
   end 


   # == Load the meter consumption for period
   #
   # Load the consumption for specified flowmeter. This contains hourly values
   # for daily report, or daily values for weekly and monthly report.
   #
   # @params:
   #   proj_id - the project, data of what to load
   #   meter_id - id of meter to load
   #   period_kind - is integer of (0: current day, 1: previous day, 2: current month, 3:previous month, 4: universal, that uses date_start..date_end)
   #   interval_kind - is integer of (0: all vals, 1: half hour values, 2: hourly values, 3: daily values)
   #
   #
   # @returns:
   #   array of meter values in format: [date_time, value, flow], date_start, date_end
   #
   def load_meter_consumption( proj_id, meter_id, period_kind, interval_kind )
     @dest_db ||= connect_to_dest_db

     #todo: Make hash with strategies of date generation
     #todo: Refactor this long method

    date_start, date_end = parse_period_kind( period_kind )

    vals = @dest_db[:_data].where( datetime: date_start.to_date..date_end.to_date )
        .filter( address: meter_id )
        .select( :datetime, :value )
        .order( :datetime )
        .all 

				@dest_db.disconnect unless @dest_db.nil?
				@dest_db = nil

     # prepare data for report (take only needed values)
     result = case interval_kind
       when ALL_VALS
         vals.map{|v| [v[:datetime], v[:value]]}

       when HALFHOUR_VALS
                vals.map{|v| [v[:datetime], v[:value]]  if ((v[:datetime].min - 30).abs < 1)||(v[:datetime].min == 0)}
                                                                                 
       when HOUR_VALS
                vals.map{|v| [v[:datetime], v[:value]] if (v[:datetime].min < 1)}
       when DAILY_VALS
                gvals = vals.group_by{|v| v[:datetime].to_date}
                gvals.map{|v| v[1][0].map{|r|r[1]}}  # [1][0] - array of hashes, r[1] - value for date              
     end.compact.uniq

     # make flowmeter with vals for array of vals
     result.map! do |r| 
       meter =  FlowMeter.new( address: meter_id, caption: "<none>")

       meter.values << FlowMeterValue.make( :meter => meter, :value => r[1], :datetime => r[0] )
       meter
     end

     return result, date_start, date_end
   end


   # == Load the meter submeters consumption for period
   #
   # Load the consumption for specified flowmeter. This contains hourly values
   # for daily report, or daily values for weekly and monthly report.
   #
   # @params:
   #   proj_id - the project, data of what to load
   #   meter_id - id of meter to load
   #   period_kind - is integer of (0: current day, 1: previous day, 2: current month, 3:previous month, 4: universal, that uses date_start..date_end)
   #
   #
   # @returns:
   #   array of submeter values in format: [address, date_time, flow]
   #
   def load_submeters_consumption( proj_id, meter_id, period_kind )
     @dest_db ||= connect_to_dest_db
     #todo: Make hash with strategies of date generation
     #todo: Refactor this long method

    date_start, date_end = parse_period_kind( period_kind )

#todo: refactor methods: remove duplication 

    vals = @dest_db[:_data].where( datetime: date_start.to_date..date_end.to_date )
      .join( :paramaccounts, :counter_id => :address )
        .filter( maincounter_id: meter_id )
        .select(Sequel.qualify(:_data, :address), :datetime, :value)
        .order( :datetime )
        .all 

				@dest_db.disconnect unless @dest_db.nil?
				@dest_db = nil

     gvals = vals.group_by{|v| v[:address]}

     result  = gvals.map do |gv| 

      min_val, max_val = gv[1].map{|a| [a[:datetime], a[:value]] }.minmax_by{|v| v[0] }

      ary = [gv[0], min_val, max_val] #gv[0] - address, val - [ date, val]

        ary
     end.compact.uniq

     # make flowmeter with vals for array of vals
     result.map! do |r| 
     
       meter =  get_meter_info( r[0], proj_id )

       meter.values << FlowMeterValue.make( :meter => meter, :value => r[1][1], :datetime => r[1][0] )
       meter.values << FlowMeterValue.make( :meter => meter, :value => r[2][1], :datetime => r[2][0] )

       meter
     end


     return result
   end


   #
   # Receive caption for specified (integer) interval kind
   #
   def interval_caption( interval_kind )
     case interval_kind
       when ALL_VALS
         "Все значения"

       when HALFHOUR_VALS
         "Получасовые значения"
                                                                                 
       when HOUR_VALS
         "Часовые значения"

       when DAILY_VALS
         "Дневные значения"
    end
   end

   
   # == Load root flowmeters from db
   #
   # All flowmeters with maincounter_id => nil is root meters, that can  represent
   #  tree of submeters.
   #
   def load_root_fm( proj_id = 33 ) #todo: replace 33 with PROJ_KAIPAS

     @dest_db ||= connect_to_dest_db()

     fds = @dest_db[:paramaccounts]
        .join(:counters, :sn => :counter_id )
        .join(:clients, :client_id => :client_id)


     db_meters = fds.select(:sn, :city, :street, :house, :flat)
          .filter('clients.project_id = ?', proj_id)
          .where( :maincounter_id => nil )
          .all

				@dest_db.disconnect unless @dest_db.nil?
				@dest_db = nil

     meters = []

     db_meters.each do |dbm|
       meters << make_meter_for( dbm )
     end

     meters.sort!{|a,b| a.address <=> b.address }
   end


   # == Load the base meters trees in list
   #
   # Load the forest of tress of root (base) meters. Exclude the foliage meters
   #
   def load_base_fm_forest( proj_id )

     root_fm = load_root_fm( proj_id )

     @meters = get_meter_dict_with_root( proj_id )

     #make trees for each root fm
     root_fm.each do |rfm|
       rfm.submeters = make_submeters_tree_for( rfm )       
     end

     #traverse trees and remove foliage
     root_fm.each do |rfm|
       rfm.submeters = remove_foliage( rfm )
     end
     
     root_fm
   end


   
   # == Remove foliage for meters branch
   #
   # @returns: submeters tree without foliage:
   #   meters without submeters removed
   #
   def remove_foliage( meter )
     return nil if meter.nil? or meter.submeters.nil?
     return [] if meter.submeters.size == 0


     res_meters = []
     meter.submeters.each do |sm|
       next if sm.submeters.nil?

       if sm.submeters.size > 0 
         sm.submeters = remove_foliage( sm )
         res_meters << sm
       end
     end

     res_meters
   end

   # == Make tree of submeters for specified meter
   # 
   # === Usage
   #  1. Must be set @meters field for meters, what scanned for owning
   #
   def make_submeters_tree_for( meter )
     submeters = @meters.select{|m| m[:maincounter_id] == meter.address }

     return nil if submeters.size == 0
     
     submeters.map!{|m| make_meter_for(m)}

     submeters.each do |sm|
       sm.submeters = make_submeters_tree_for(sm)
     end
     
     submeters
   end
   

   # == Load the flow meters from db
   #
   def load_flowmeters( proj_id = 33 )

     @dest_db ||= connect_to_dest_db()

     fds = @dest_db[:counters].join(:clients, :client_id => :client_id)

     db_meters = fds.select(:sn, :city, :street, :house, :flat).filter('clients.project_id = ?', proj_id).all

				@dest_db.disconnect unless @dest_db.nil?
				@dest_db = nil

     meters = []

     db_meters.each do |dbm|
       meters << make_meter_for( dbm )
     end

     meters.sort!{|a,b| a.address <=> b.address }
   end

end
