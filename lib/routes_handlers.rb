# encoding: utf-8
module AppPages

				def main_page 
						begin 
							erb :msws_main
						rescue 
							$logger.error $!.message	     
							$logger.ap $@[0..4]
						end
					end


				mskip_page = lambda do
						erb :mskip
					end


				fmakbay_wells_page = lambda do
						begin
								@cp_data_rcv.read_flow_meters_table( NO_PROJECT, $akbay_wells_fm)

								last_modified Time.now

								erb :fm_akbay_wells
						rescue
							ap $!.message
							ap $@[0..5]
							$logger.error $!
							$logger.info $@
						end
					end


				fmakbay_wells_ms_page = lambda do
						begin
								@cp_data_rcv.read_flow_meters_table( NO_PROJECT, $akbay_wells_fm)

								last_modified Time.now

								erb :fm_akbay_wells_ms
						rescue
							ap $!.message
							ap $@[0..5]
							$logger.error $!
							$logger.info $@
						end
					end


				cpmodes_page = lambda do
						resp = read_modes_from_service()
						resp.body	
					end


				cpmodes_post_page = lambda do
						resp = read_modes_from_service()
						resp.body	
					end


				control_points_page = lambda do
						begin		
							@cp_data_rcv.get_cpoints_vals

							last_modified Time.now

							erb :cp
						rescue 
							$logger.error $!.message
							$logger.ap $@[0..4]
						end
					end


				cp_int_page = lambda do
						begin		
							@cp_data_rcv.get_cpoints_vals

							last_modified Time.now

							erb :cp_int
						rescue 
							$logger.error $!.message
							$logger.ap $@[0..4]
						end
					end


				cp_wml_page = lambda do
						begin
							@cp_data_rcv.get_cpoints_vals

							last_modified Time.now

							erb :"cp.wml"
						rescue 
							$logger.error $!.message
							$logger.ap $@[0..4]
						end
					end


				flowmeter_ms_page = lambda do
						begin
							@cp_data_rcv.read_flow_meters_table

							last_modified Time.now

							erb :fm
						rescue 
							$logger.error $!.message
							$logger.ap $@[0..4]
						end
					end


				rep_flowmeter_energomera_page = lambda  do
						begin
							@cp_data_rcv.connect_to_cpoint_db()
							@rep_main_fm = @cp_data_rcv.load_root_energomera_meters 
							erb :rep_fm_energomera_form
						rescue 
							ap $!.message
							ap $@[0..5]

							$logger.error $!.message
							$logger.ap $@
						end
					end


				rep_fm_energomera_post_page = lambda do
						begin
							@date_start = get_parms_date_start
							@date_end = get_parms_date_end

							@cp_data_rcv.connect_to_cpoint_db()

							@ergo_fm_vals = @cp_data_rcv.load_energomera_single_meter_consumption( params[:form][:mainfm], params[:period_kind].to_i, params[:interval_kind].to_i, @date_start, @date_end )

							last_modified Time.now

							ap "Energomera for #{@date_start} to #{@date_end} has #{@ergo_fm_vals.count} meters with #{@ergo_fm_vals[0].values.count} values"

							erb :rep_fm_energomera
						rescue 
							ap $!.message
							ap $@[0..5]

							$logger.error $!.message
							$logger.ap $@
						end
					end


				siphon_fm_page = lambda do
						begin
							@cp_data_rcv.read_flow_meters_table( 33, $siphon_fm )

							last_modified Time.now

							erb :siphon_fm
						rescue 
							$logger.error $!.message
							$logger.ap $@[0..4]
						end
					end


				rep_cons_page = lambda do
						begin
							@main_fm = @fm_data_rcv.load_flowmeters( PROJ_PROM_GROUP )

							erb :rep_cons
						rescue 
							$logger.error $!.message
							$logger.ap $@[0..4]
						end
					end


				rep_cons_post_page = lambda do
						begin
							fmeter = params[:mainfm]

							@fm_data_rcv.date_start = get_parms_date_start
							@fm_data_rcv.date_end = get_parms_date_end

							meter = FlowMeter.new( :address => fmeter, :caption => "meter" )

							@meter = @fm_data_rcv.read_meter( meter, PROJ_PROM_GROUP )

							erb :rep_cons_gen
						rescue 
							$logger.error $!.message
							$logger.ap $@[0..4]
						end 
					end


				rep_all_vzljot_page = lambda do
						begin

							@fm_vzljot = @cp_data_rcv.read_vzljot_flowmeters_table

							$logger.ap @fm_vzljot

							erb :vzljot_fm_ms

						rescue
							$logger.error $!.to_s
							$logger.ap $@[0..4]
							'Sorry. Server error occured'
							$!.message + "\n" + $@[0]
						end
					end


				siphon_fm_ms_page = lambda do
						begin

							@fmeters = @cp_data_rcv.read_flow_meters_table( 33, $siphon_fm )

							erb :siphon_flowmeters_ms

						rescue
							$logger.error $!.to_s
							$logger.ap $@[0..4]
							'Sorry. Server error occured'
						end
					end


				fm_ms_page = lambda do
						begin

							@fmeters = @cp_data_rcv.read_flow_meters_table

							erb :flowmeter_ms

						rescue
							$logger.error $!.to_s
							$logger.ap $@[0..4]
							'Sorry. Server error occured'
						end
					end


				water_map_page = lambda do
						begin
							@cp_data_rcv.get_cpoints_vals

							erb :'water-city-map.svg'
						rescue 
							$logger.error $!.message
							$logger.ap $@[0..4]
						end 
					end


				water_map_google_page = lambda do
						begin
							@cp_data_rcv.get_cpoints_vals

							erb :'water-city-google-map'

						rescue 
							$logger.error $!.message
							$logger.ap $@[0..4]
						end
					end


				ms_compact_page = lambda do
						begin
							@cp_data_rcv.get_cpoints_vals

							erb :'water-city-google-map'

						rescue 
							$logger.error $!.message
							$logger.ap $@[0..4]
						end
					end


				cp_graphs_page = lambda do
						begin
							cp_code = params[:code].to_i

							file_name = "/images/#{cp_code}.png"

							redirect file_name

						rescue 
							$logger.error $!.message
							$logger.error $@.inspect
						end
					end


				cp_gr_page = lambda do
						begin
							@selected_cp = params[:cp] || 0
							@selected_cp = @selected_cp.to_i
							erb :"cp-gr-select"

						rescue 
							$logger.error $!.message
							$logger.ap $@[0..4]
						end
					end


				cp_gr_fl_page = lambda do
						begin
							ap "Generate cpoint graphs: #{params}"

							@date_start = DateTime.now - 1 
							@date_end = DateTime.now + 1 


							cps_addr = params[:cps].to_i

							@sel_cp = $cps.detect{ |cp| cp.address == cps_addr }

							@cp_mode_min = get_cp_mode(cps_addr)[:min]
							@cp_mode_max = get_cp_mode(cps_addr)[:max]

							halt 400 if @sel_cp.nil?

							@cur_cp_period_vals = @cp_data_rcv.load_cp_period_vals_for( @sel_cp.address, @date_start, @date_end ).map{|v| v[:value] = v[:value].round(1); v}
					
							ap 'generating'

							erb :cp_gr_rep_min

						rescue 
							$logger.error $!.message
							$logger.ap $@[0..4]
						end
					end


				cp_gr_post_page = lambda do
						begin
							ap "Generate cpoint graphs: #{params}"

							@date_start = get_parms_date_start
							@date_end = get_parms_date_end


							cps_addr = params[:cps].to_i

							@sel_cp = $cps.detect{ |cp| cp.address == cps_addr }

							@cp_mode_min = get_cp_mode(cps_addr)[:min]
							@cp_mode_max = get_cp_mode(cps_addr)[:max]



							halt 404 if @sel_cp.nil?

							@cur_cp_period_vals = @cp_data_rcv.load_cp_period_vals_for( @sel_cp.address, @date_start, @date_end ).map{|v| v[:value] = v[:value].round(1); v}


							$logger.ap @cp_data_rcv.load_cp_period_vals_for( @sel_cp.address, @date_start, @date_end )


							erb :cp_gr_rep

						rescue 
							$logger.error $!.message
							$logger.ap $@[0..4]
						end
					end


				cp_tab_page = lambda do
						begin
							@selected_cp = params[:cp] || 0
							@selected_cp = @selected_cp.to_i

							erb :"cp-tab-select"

						rescue 
							$logger.error $!.message
							$logger.ap $@[0..4]
						end
					end


				cp_tab_post_page = lambda do
						begin
							@date_start = get_parms_date_start
							@date_end = get_parms_date_end

							cps_addr = params[:cps].to_i

							@sel_cp = $cps.detect{ |cp| cp.address == cps_addr }

							halt 404 if @sel_cp.nil?

							@cur_cp_period_vals = @cp_data_rcv.load_cp_period_vals_for @sel_cp.address, @date_start, @date_end

							erb :"cp-tab-gen"

						rescue 
							$logger.error $!.message
							$logger.ap $@[0..4]
						end
					end


				rep_all_cp_page = lambda do
						begin
							erb :rep_cp_all 
						rescue
							$logger.error $!.message
							$logger.ap $@[0..4]
						end
					end


				rep_all_cp_post_page = lambda do
						begin
							@date_start = get_parms_date_start
							@date_end = get_parms_date_end
							@period_kind = params[:period_kind].to_i
							@interval_kind = params[:interval_kind].to_i
							
							change_date_range_for_period_kind

							#//todo: 2. Filter data for interval kind
							temp_vals = {}

							$cps.each do |cp|

								cur_cp_val = @cp_data_rcv.load_cp_period_vals_for cp.address, @date_start, @date_end

								cur_cp_val = filter_by_date @interval_kind, cur_cp_val

								cur_cp_val.each do |cv|

									temp_vals[cv[:datetime]] ||= {}
									temp_vals[cv[:datetime]][cp] = cv[:value]
								end
							end

							@all_cp_vals = []
							temp_vals.keys.sort.each do |date_key|

								value = [date_key, {}]

								temp_vals[date_key].keys.sort{|a,b| a.address <=> b.address }.each do |cp_key|

									value[1][cp_key] = temp_vals[date_key][cp_key]
								end

								@all_cp_vals << value
							end


							erb :'rep-all-cp-gen'

						rescue
							$logger.error $!.message
							$logger.ap $@[0..4]
						end
					end


				rep_ergomera_analysis_page = lambda do
						begin
							if( @curr_time.nil? )or(( DateTime.now.to_date - @curr_time ) > 0)
								@curr_time ||= DateTime.now.to_date

								@fm_data_rcv.connect_to_cpoint_db

								@rep_main_fm = @fm_data_rcv.load_base_fm_forest( PROJ_ERGOMERA )      
							end

							erb:"rep-ergomera-analysis-form"
						rescue 
							$logger.error $!.message
							$logger.ap $@[0..4]
						end
					end


				rep_ergomera_analysis_post_page = lambda do
						begin
							@date_start = get_parms_date_start
							@date_end = get_parms_date_end

							@fm_data_rcv.date_start = @date_start

							@fm_data_rcv.date_end = @date_end

							@meter_id = params[:form][:mainfm].to_i

							@period_kind = params[:form][:period_kind].to_i

							@rep_meter = @fm_data_rcv.get_meter_info( @meter_id, PROJ_ERGOMERA)

							@rep_main_data = @fm_data_rcv.read_meter_for_period( @rep_meter, PROJ_ERGOMERA, @period_kind )

							@date_start = @fm_data_rcv.date_start
							@date_end = @fm_data_rcv.date_end

							@rep_main_cons = @rep_main_data.values[1].value - @rep_main_data.values[0].value

							@rep_fms = @fm_data_rcv.load_submeters_consumption( PROJ_ERGOMERA, @meter_id, @period_kind )           

							puts "Ergomera Report ***** >>\n"
							puts @rep_fms.map{|r| [r.address,r.caption] }.sort

							@rep_scons_tot = @rep_fms.map{|r| r.values[1].value - r.values[0].value }.reduce(:+)
							@rep_scons_tot ||= 0


							@rep_fms.sort!{|a,b| a.address <=> b.address }

							@rep_fms.each do |fm|
								fm.start_value = fm.values[0].value
								fm.end_value = fm.values[1].value
								fm.flowrate = (fm.values[1].value - fm.values[0].value)
							end


							ToXls::Writer.new(@rep_fms, 
										:name => "#{@rep_meter.caption}", 
										:columns => [:address, :caption, :start_value, :end_value, :flowrate],
										:headers => ['Адрес', 'Наименование', 'Начало', 'Конец', 'Расход']

									 )
							.write_io(REP_ERGOMERA_FILE)

							erb :"rep-ergomera-analysis-gen"
						rescue 
							$logger.error $!.message
							$logger.ap $@[0..14]
						end
					end


				rep_meters_base_page = lambda do
						begin
							load_base_meters
							erb :rep_base_meters_form
						rescue 
							$logger.error $!.message
							$logger.ap $@[0..4]
						end
					end


				rep_meters_base_post_page = lambda do
						begin
							"Main meters base"	
						rescue 
							$logger.error $!.message
							$logger.ap $@[0..4]
						end
					end


				rep_meters_ergomera_page = lambda do
						begin
							load_ergomera_meters
							erb :rep_ergomera_meters_form
						rescue 
							$logger.error $!.message
							$logger.ap $@[0..4]
						end
					end


				rep_meters_ergomera_post_page = lambda do
						begin
							params.to_s
						rescue 
							$logger.error $!.message
							$logger.ap $@[0..4]
						end
					end


				ms_kaipas2_status_page = lambda do
						begin
							load_kaitpas_meters_status

							erb :"ms-kaitpas2"
						rescue
							$logger.error $!.message
							$logger.ap $@[0..4]
						end
					end


				ms_taraz_status_page = lambda  do
						begin
							load_taraz_meters_status

							erb :"ms-taraz"
						rescue
							$logger.error $!.message
							$logger.ap $@[0..4]
						end
					end


				ms_kaipas_status_page = lambda do
						begin
							load_kaitpas_meters_status

							erb :"kaitpas-ms"
						rescue
							$logger.error $!.message
							$logger.ap $@[0..4]
						end
					end


				ms_bam_status_page = lambda  do
						begin
							load_bam_meters_status
							erb :"bam-ms"
						rescue 
							$logger.error $!.message
							$logger.ap $@[0..4]
						end
					end


				ms_notrar_status_page = lambda do
						begin
							load_notrar_meters_status
							erb :ms_notrar_status
						rescue 
							$logger.error $!.message
							$logger.ap $@[0..4]
						end
					end


				manometers_all_page = lambda  do
						erb :'manometer-work-all'
					end


				meter_post_value_json = lambda do
						@cp_data_rcv.get_cpoint_values([params[:code]]).to_json
					end


				meter_value_json = lambda do
						@cp_data_rcv.get_cpoint_values([params[:code]]).to_json
					end


				meters_post_json = lambda  do
						begin
							vals = @cp_data_rcv.get_cpoint_values( params[:codes].split(',') )
							vals.to_json
						rescue
							$logger.error $!.message
							$logger.ap $@[0..4]
							ap $!.message
							ap $@[0..4]
						end
					end


				meters_json = lambda do
						begin
							$logger.debug params
							vals = @cp_data_rcv.get_cpoint_values( params[:codes].split(',') )
							vals.to_json
						rescue
							$logger.error $!.message
							$logger.ap $@[0..4]
							ap $!.message
							ap $@[0..4]
						end
					end


				edit_modes_page = lambda do

								redirect '/auth/login' unless env['warden'].authenticated?

								@current_user = env['warden'].user

								erb :'edit-modes'
						end


				login_page = lambda  do
								erb :login2
						end


				login_post_page = lambda do
								env['warden'].authenticate!

								if session[:return_to].nil?
										redirect '/edit-modes'
								else 
										redirect session[:return_to]
								end 
						end


				logout_page = lambda do
								env['warden'].authenticate!

								if session[:return_to].nil?
										redirect '/edit-modes'
								else 
										redirect session[:return_to]
								end 
						end


				unauth_post_page = lambda do
								session[:return_to] = env['warden.options'][:attempted_path]
								puts env['warden.options'][:attempted_path]
								redirect '/auth/login'
						end


				ergomera_page = lambda do
						begin
							@ergo_name = { :kabisko => "Кабиско", :karasu => "Карасу 3", :kyzylsay => "Кызыл-Сай" }
							@ergo_vals = @cp_data_rcv.get_ergomera_vals

							erb :ergomera_view
							#@ergo_vals.to_s
						rescue
							$logger.error $!.message
							$logger.ap $@[0..4]
							ap $!.message
							ap $@[0..4]
						end
					end


				pulsar_page = lambda do
						begin
							$logger.debug "Params #{params}"
							query_body = request.body.read
							$logger.debug query_body

							json_body = JSON.parse query_body
							@pulsar_saver ||= PulsarDataSaver.new
							@pulsar_saver.logger = $logger
							if @pulsar_saver.store( json_body ) 
								[200,"Pulsar data saved"]
							else
								[500, @pulsar_saver.last_error]
							end
						rescue
							$logger.error 'Error:: ' + $!.message
							ap $@[0..7]
							$logger.error 'Message:: ' + $@[0..12]
						end
					end


				latch_pressure_page = lambda  do
						status 200
						body LatchManager.get_pressure.to_s
					end


				latch_pressure_lim_page = lambda do
						status 200
						body LatchManager.get_pressure_lim.to_s
					end

				akbay_meter_page = lambda do
						status 200
						body "akbay meters"
					end

				akbay_meter_post_page = lambda do
						puts "Akbay meter::"

						reqbody = request.body.read


						json = JSON.parse reqbody

						puts "Request body: #{reqbody} class: #{reqbody.class}"
						puts "date: #{json['date']} value: #{json['value']} meter: #{json['meter']}"

						date = DateTime.parse(json['date'])
						value = json['value'].to_i
						meter = json['meter'].to_i

						save_cpoint_meter( meter, value, date )

						status 200
						body "post akbay meter state"
					end


				tank_param_page = lambda do
						cross_origin
						ap params
						tank_code = params["code"].to_i
						tank_command =  params["command"].to_i

						res = read_last_tank_param( tank_code, tank_command )

						status 200
						body res.to_s
					end


				tank_param_post_page = lambda do
						cross_origin

						save_tank_params(params)

						status 200
						body "Tank params received: #{params}"
					end
end


