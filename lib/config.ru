#user Rack::Static, urls: ['/styles', '/scripts'], root: 'static'

require './msws-lv'
require './report_app'
require './server_app'
require './data_app'

map '/' do
	run Application
end

map "/report" do
	run ReportApp
end

map '/server' do
	run ServerApp
end

map '/data' do
	run DataApp
end
