Currilicum Vitae

Contact information

Afonin Vladimir 

st. Chapaeva 55, Shymkent, Kazakhstan, 160002
cell phone: 8(775)823-56-87
e-mail: stormarchitextor@gmail.com


Personal information

Date of birth: 11.07.2014
Place of birth: Shymkent
Citizenship: Kazakhstan
Visa status: n/a
Sex: male

Marital status: Married
Spouse name: Julya
Children: boy 8 y.o and girl newborn


Employment history

1999-2001: Smart systems
  position: software programer
  technology: Delphi, MySQL
  achievments: Developed and installed document workflow system 

2001-2002: Kazakhtelekom
  position: software engineer
  technology: Delphi, Oracle\PL-SQL
  achievments: Developed, installed and supported SCADA for phone network state management on S12/Oracle

2002-2004: South Polymetall
  position: software engineer
  technology: Delphi, MySQL
  achievments: Developed, installed and supported document workflow system

2004-2014: Water service
  position: software engineer/system administrator/db administrator
  technology: Delphi, C#, Ruby/Sinatra, Javascript/AJAX, DHTML/HTML5, C/PLC Rabbit, MS SQL 2000
  achievments:
    * support for SCADA for water pump facility
    * support for Service/DB of data query application for local block of houses: water meters flow values
    * written (3 versions: Delphi, C#, Ruby) SCADA for control water pressure in city
    * support of web portal for SCADA and reports of water control
    * written and modified microcontroller software for transmission of water flowmeters values and management of remote valves
    * written central SCADA for management of water in entire town
    * written mnemoscheme for control of water flow in three blocks


2009-2014: Heat and electricity facility (part time)
  position: software engineer
  technology: Delphi, Postgresql, C#, WPF, Ruby/Rails, Apache, OOP/OOA
  achievments:
    * written, upgraded and supported SCADA for heat control in town
    * written small site for external view of SCADA
    * written SCADA system for water pumps usage
    * written report application for legacy electro-technic SCADA
    * written SCADA for control of turbines/boilers



Education
  South Kazakhstan University
  1996-2001: Software and automation engineer


Professional Qualifications
  * Delphi - 15 years
  * OOP/OOA - 12 years
  * C# - 6 years
  * C - 6 years
  * Ruby - 5 years
  * PostgreSQL - 5 years
  * System architecture - 3 years
  * MS SQL 2000 - 3 years
  * MySQL - 1 year
  * Javascript/AJAX - 1 year


Interests
  * Read science fiction books
  * Read software books and implement new technologies
  * Walk down a street
  * Carpentry and wood working
  * Drawing

