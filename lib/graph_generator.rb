class GraphGenerator

  #   caption - of graph
  #   vals - array of hashes (:datetime, :value)
  #   file_name - to write
	attr_accessor :caption, :vals, :file_name
  #   need_png - if we need to compile
	attr_accessor :need_png
  #	width, height - dimension of image to generate (default: 600x400)
	attr_accessor :width, :height

	def initialize

		@need_png = false
		@width = 600
		@height = 400
	end

  # == Generate graph for specified table
  # @parms
  #
  def execute

		ap "----------------------------------------\n\n\n"

    mychart = GerbilCharts::Charts::LineChart.new( 
                                                  :width => @width, 
                                                  :height => @height, 
                                                  :style => 'embed:brushmetal.css'
                                                 )

    model = GerbilCharts::Models::TimeSeriesGraphModel.new(:title => @caption)

    vals = @vals.map{|v| [v[:datetime].to_time, v[:value]] }

    model.add_tuples vals

    group = GerbilCharts::Models::GraphModelGroup.new(@caption)
    group << model

    mychart.modelgroup=group
    mychart.render("./static/images/#{file_name}.svg")

    convert_to_png("./static/images/#{file_name}") if need_png
  rescue 
    $logger.error $!.message
    $logger.ap $@
  end

	private

  # == Convert to png specified svg file
  #
  def convert_to_png( file_name )

    svg = RSVG::Handle.new_from_file( file_name + ".svg" )
    surface = Cairo::ImageSurface.new(Cairo::FORMAT_ARGB32, @width, @height)
    context = Cairo::Context.new(surface)
    context.render_rsvg_handle(svg)
    surface.write_to_png("#{file_name}.png")
  rescue 
    $logger.error $!.message
    $logger.ap $@
  end

end
