# encoding: utf-8



COLOR_LOW = '"#FFD42A"'
COLOR_HIGH = '"#FF0000"'
COLOR_NORMAL = '"#5FD38D"'

LOW = 0
HIGH = 1


#*************************************************#
# METERS
#*************************************************#

#define meter codes
$meters = {
	 5689294=> {color: '4d4d61'},		
	 5756852=> {color: '4d4d60'},
	 7067116=> {color: '4d4d5c'},
	 7070866=> {color: '4d4d58'},
	 7071889=> {color: '4d4d53'},
	 7073303=> {color: '4d4d54'},
	 7074308=> {color: '4d4d5a'},
	 7078072=> {color: '4d4d50'},
	 7162740=> {color: '4d4d5e'},
	 7163704=> {color: '4d4d73'},
	 7164834=> {color: '4d4d55'},
	 7166730=> {color: '4d4d71'},
	 7168196=> {color: '4d4d70'},
	11114072=> {color: '4d4d51'},
	11331614=> {color: '4d4d5b'},
	11347190=> {color: '4d4d57'},
	11347198=> {color: '4d4d66'},
	11348322=> {color: '4d4d5d'},
	11600320=> {color: '4d4d59'},
	11600620=> {color: '4d4d5f'},
	11600804=> {color: '4d4d4e'},
	11601785=> {color: '4d4d52'},
	11615973=> {color: '4d4d4f'}
}




# load csv file (param str 0)
source_file = ARGV[0]

puts "Converter from svg to erb\n"
puts "Loading file..."
source_string = File.open( source_file, 'r+:utf-8' ){|f| f.read }


# convert file

puts "\nConvert meters..."

$meters.each_pair do |m,v|
	puts "\nConvert #{m}..."
	source_string.gsub!( v[:color], "<%= 'DED71F' if @kaitpas_meters[#{m}].is_counter_stoped?%><%= 'DE1414' if @kaitpas_meters[#{m}].is_counter_stalling?%><%= '2023D6' if @kaitpas_meters[#{m}].is_normal_state?%>" )
end



# export erb file (param str 1)
puts "\n Export result file"

dest_file = ARGV[1]
File.open( dest_file, 'w+' ){|f| f.write( source_string ) }

puts "Done."
