// Manometer.js
//
// goal: Draw manometer with upper and lower alarm bounds.
//		When bounds is reached central view of manometer begin flashing
//		with bounds color: lower - yellow, upper - red
//
// copyright (c) 2014-2015 by Afonin Vladimir (mailto:stormarchitextor@gmail.com)
//

	const DEF_SCALE_X = 1.0;
	const DEF_SCALE_Y = 1.0;

        const WIDTH = 300;
        const HEIGHT = 300;
        const CENTER_WIDTH = 30;
        const CENTER_HEIGHT = 30;

        const RADIUS = 150;
        const CENTER_RADIUS = 70;

        const MAJOR_TICK_LEN = 30;
        const MINOR_TICK_LEN = 15;
        const MICRO_TICK_LEN = 8;

        const DEGREE = Math.PI/180;
        const DEGREES_30 = 30*DEGREE;
        const DEGREES_60 = 60*DEGREE; 

        const COUNT_MAJOR_TICKS = 8;
        const COUNT_MINOR_TICKS = 7;
        const COUNT_MICRO_TICKS = 60;

        const MAJOR_TICKS_ANGLE = 275/COUNT_MAJOR_TICKS*DEGREE;
        const MINOR_TICKS_ANGLE = MAJOR_TICKS_ANGLE;

        const ARROW_HEAD_LEN = 35;
        const ARROW_SHIFT_LEN = 50;

        const MAX_PRESSURE = 14;
        const MIN_PRESSURE = 0;

        const SECOND = 1000;
        const MINUTE = 60*SECOND;
        const MINUTES = 60*SECOND;

        const FILLED = true;

	const OUTER_CIRCLE_STROKE = 'black';
	const OUTER_CIRCLE_FILL = "aqua";

	const RED_GAUGE_STROKE = "red";
	const RED_GAUGE_FILL = "red";

	const NORMAL_GAUGE_STROKE = "green";
	const NORMAL_GAUGE_FILL = 'green';

	const MIN_GAUGE_STROKE = "#FFD175";
	const MIN_GAUGE_FILL = 'yellow';

        window.Meters = {};

//--------------------------------------------------------
// PressureMeter
//
// class: display manometer with range values
//
Meters.PressureMeter = function(type){

  this.type = type;

  this.point_value = 2.34;
	this.prev_value = 0.0;

	this.scale = 2;

  this.time = "12:30";
  this.point_caption = "Нет";

  this.min_val = 2;
  this.max_val = 5;

	this.min_day_val = 0.0;
	this.max_day_val = 1.0;
	this.min_hour_val = 0.0;
	this.max_hour_val = 1.0;

  this.start_angle = 150*DEGREE;
  this.shift_angle = 34.4*DEGREE;

  this.address = 0;
  this.blink_filled = false;
  this.canvas = null;

	this.scale_x = DEF_SCALE_X;
	this.scale_y = DEF_SCALE_Y;

	//----------------------------------------------
	// draw
	//
	// goal: draw the manometer
	//
  this.draw = function(){

        this.context = this.canvas.getContext('2d');

				this.context.scale( this.scale_x, this.scale_y );
        this.draw_outer_circle();

        this.draw_min_gauge();
        this.draw_normal_gauge();
        this.draw_red_gauge();

        this.draw_outer_shadow_circle();

        this.show_major_ticks();
				this.context.scale( this.scale_x, this.scale_y );
        this.show_minor_ticks();

				this.context.scale( this.scale_x, this.scale_y );
        this.draw_internal_value_circle();

				this.draw_prev_value_arrow();


        this.draw_ticks_values();

        this.show_arrow('green', this.point_value);
      }


			//------------------------------------------------
			// draw_outer_circle
			//
			// goal: display container circle
			//
      this.draw_outer_circle = function(){

        this.context.lineWidth = 2;
        this.context.strokeStyle = OUTER_CIRCLE_STROKE;
        this.context.fillStyle = OUTER_CIRCLE_FILL;

        this.context.beginPath();
        this.context.arc(WIDTH/2, HEIGHT/2, RADIUS - 5, 0, 2*Math.PI, false);
        this.context.fill();
        this.context.stroke();
      }



			//----------------------------------------------
			// draw_min_gauge
			//
			// goal: draw gauge with yellow color from (0..min)
			//
      this.draw_min_gauge = function(){

        this.context.shadowColor = "none";
        this.context.shadowBlur = 0;
        this.context.shadowOffsetX = 0;
        this.context.shadowOffsetY = 0;


        var max_angle = this.start_angle + this.shift_angle * this.min_val/this.scale;
        var min_angle = this.start_angle;

        this.context.strokeStyle = MIN_GAUGE_STROKE;
        this.context.fillStyle = MIN_GAUGE_FILL;
        this.context.beginPath();
        this.context.arc(WIDTH/2, HEIGHT/2, RADIUS - 8, min_angle, max_angle, false );
        this.context.arc(WIDTH/2, HEIGHT/2, RADIUS - MAJOR_TICK_LEN, max_angle, min_angle, true );


        this.context.stroke();
        this.context.fill();
      }


			//-----------------------------------------------------
			// draw_normal_gauge
			//
			// goal: draw green gauge (min-max)
			//
      this.draw_normal_gauge = function(){

        var min_angle = this.start_angle + this.shift_angle * this.min_val/this.scale;
        var max_angle = min_angle + this.shift_angle * (this.max_val - this.min_val)/this.scale;

        this.context.strokeStyle = NORMAL_GAUGE_STROKE;
        this.context.fillStyle = NORMAL_GAUGE_FILL;
        this.context.beginPath();
        this.context.arc(WIDTH/2, HEIGHT/2, RADIUS - 8, min_angle, max_angle, false );
        this.context.arc(WIDTH/2, HEIGHT/2, RADIUS - MAJOR_TICK_LEN, max_angle, min_angle, true );


        this.context.stroke();
        this.context.fill();

        this.context.strokeStyle = "black";
      }


			//------------------------------------------------
			// draw_red_gauge
			//
			// goal: draw red gauge (max-manometer max)
			//
      this.draw_red_gauge = function(){

        var min_angle = this.start_angle + this.shift_angle * this.max_val/this.scale;
        var max_angle = min_angle + this.shift_angle * (MAX_PRESSURE - this.max_val)/this.scale;

        this.context.strokeStyle = RED_GAUGE_STROKE;
        this.context.fillStyle = RED_GAUGE_FILL;
        this.context.beginPath();
        this.context.arc(WIDTH/2, HEIGHT/2, RADIUS - 8, min_angle, max_angle, false );
        this.context.arc(WIDTH/2, HEIGHT/2, RADIUS - MAJOR_TICK_LEN, max_angle, min_angle, true );


        this.context.stroke();
        this.context.fill();

        this.context.strokeStyle = "black";
      }


			//-------------------------------------------------
			// draw_outer_shadow_circle 
			//
			// goal: draw shadow effect for volume
			//
      this.draw_outer_shadow_circle = function (){

        this.context.lineWidth = 2;
        this.context.strokeStyle = 'black';

        this.context.shadowColor = "#5D4D4E";
        this.context.shadowBlur = 5;
        this.context.shadowOffsetX = 5;
        this.context.shadowOffsetY = 5;

        this.context.beginPath();
        this.context.arc(WIDTH/2, HEIGHT/2, RADIUS - 5, 0, 2*Math.PI, false);
        this.context.stroke();
      }


			//--------------------------------------------------
			// show_major_ticks
			//
			// goal: display major ticks (big lines) for int values
			//
      this.show_major_ticks = function(){

        this.context.shadowColor = "#5D4D4E";
        this.context.shadowBlur = 5;
        this.context.shadowOffsetX = 5;
        this.context.shadowOffsetY = 5;

        this.context.translate( WIDTH/2, HEIGHT/2 );
        this.context.rotate( DEGREES_60 );

        this.context.lineWidth = 3;
        for( var i = 0; i < COUNT_MAJOR_TICKS; i++ ){

          this.context.beginPath();
          this.context.moveTo( 0, RADIUS - 5 );
          this.context.lineTo( 0, RADIUS - MAJOR_TICK_LEN );
          this.context.stroke();
          this.context.fill();

          this.context.rotate( MAJOR_TICKS_ANGLE );
        }

        this.context.setTransform(1,0,0,1,0,0);
      }


			//-----------------------------------------------------
			// show_minor_ticks
			//
			// goal: show half value ticks (inbetween int)
			//
      this.show_minor_ticks = function(){

        this.context.translate( WIDTH/2, HEIGHT/2 );
        this.context.rotate( 77*DEGREE );

        this.context.lineWidth = 3;
        for( var i = 0; i < COUNT_MINOR_TICKS; i++ ){

          this.context.beginPath();
          this.context.moveTo( 0, RADIUS - 5 );
          this.context.lineTo( 0, RADIUS - MINOR_TICK_LEN );
          this.context.stroke();
          this.context.fill();

          this.context.rotate( MINOR_TICKS_ANGLE );
        }

        this.context.setTransform(1,0,0,1,0,0);
      }


			//------------------------------------------------------
			// draw_prev_value_arrow
			//
			// goal: Display changes from prev value as arrow: 
			//		raise - up arrow, lower - down arrow
			//
			this.draw_prev_value_arrow = function(){
				this.context.lineWidth = 1;
				
				if( this.prev_value < this.point_value ){

				//up arrow
					this.context.fillStyle = '#0000FF';
					this.context.strokeStyle = '#0000FF';
					this.context.save();
					this.context.translate(0,-10);
					this.context.beginPath();
					this.context.moveTo( 5, HEIGHT - 40 );
					this.context.lineTo( 20, HEIGHT - 55 );
					this.context.lineTo( 35, HEIGHT - 40 );
					this.context.closePath();
					this.context.fill();
					this.context.restore();
				}else{

					//down arrow
					this.context.fillStyle = '#FF0000';
					this.context.strokeStyle = '#FF0000';

					this.context.beginPath();
					this.context.moveTo( 5, HEIGHT - 15 );
					this.context.lineTo( 20, HEIGHT );
					this.context.lineTo( 35, HEIGHT - 15 );
					this.context.closePath();
					this.context.fill();
				};

        this.context.fillStyle = "#000000";
        this.context.textAlign = 'center';
        this.context.font = "28px Arial";
        this.context.fillText(this.prev_value, 30, HEIGHT - 20);

			};

			//------------------------------------------------------
			// draw_internal_value_circle
			//
			// goal: Draw a circle with bounds color and value text
			//
      this.draw_internal_value_circle = function(){

        this.context.lineWidth = 1.5;

        var circle_color = 'lightgreen';

        if( !this.blink_filled ){

          if( this.point_value < this.min_val ){
            circle_color = 'yellow';
          }else if( this.point_value > this.max_val ){
            circle_color = 'red';
          }
        }

        this.context.fillStyle = circle_color;
        this.context.beginPath();
        this.context.arc(WIDTH/2, HEIGHT/2, CENTER_RADIUS, 0, 2*Math.PI, false);
        this.context.stroke();
        this.context.fill();

        this.show_value();
      }


			//----------------------------------------------------
			// has_alert
			//
			// goal: check if value is in lower/upper bounds
			//
      this.has_alert = function() {

        var alert_state = (this.point_value <= this.min_val)||(this.point_value >= this.max_val);

        if( !alert_state )
          this.blink_filled = alert_state;

        return alert_state;
      }


      this.clear = function(){
        this.context.setTransform(1,0,0,1,0,0);
        this.context.clearRect( 0, 0, WIDTH, HEIGHT + 20 );
      }


      this.blink = function(){
        this.blink_filled = !this.blink_filled;
        this.redraw();
      }


      this.redraw = function(){
        this.clear();
        this.draw();
      }


			//------------------------------------------------------
			// show_value
			//
			// goal: Display value for manometer
			//
      this.show_value = function(){

        this.context.shadowColor = "none";
        this.context.shadowBlur = 0;
        this.context.shadowOffsetX = 0;
        this.context.shadowOffsetY = 0;

        this.context.fillStyle = "#000000";
        this.context.textAlign = 'center';
        this.context.font = "68px Arial";
        this.context.fillText(this.point_value,WIDTH/2, HEIGHT/2 + 23);

        this.context.font = "30px Arial";
        this.context.fillText(this.point_caption, WIDTH/2, HEIGHT + HEIGHT/10);

        this.context.font = "35px Arial";
        this.context.fillText(this.time, WIDTH/2, HEIGHT/2 + 117 );

				this.context.beginPath();
				this.context.moveTo( 100, 180 );
				this.context.lineTo( 200, 180 );
				this.context.strokeStyle = '#0000FF';
				this.context.stroke();

				this.context.font = "20px Arial";
				this.context.fillStyle = 'yellow';
				this.context.fillText(this.min_val, 120, 200);
				this.context.fillStyle = 'red';
				this.context.fillText(this.max_val, 180, 200);

        this.context.textAlign = 'left';
      }


			//------------------------------------------------
			// draw_ticks_values
			//
			// goal: display ticks for major values
			//
      this.draw_ticks_values = function(){

				this.context.fillStyle = '#000000';

				var ticks_x = [50,33,60,110,173,227,250,240];
				var ticks_y = [215,153,95,60,60,98,153,215];

        this.context.font = "30px Arial";

				for( var i = 0; i < ticks_x.length; i++ )
				{
					this.context.fillText( i*this.scale, ticks_x[i], ticks_y[i] );
				}
      }



			//-----------------------------------------------------
			// show_arrow
			//
			// goal: show arrow for current value with angle from center
			//
      this.show_arrow = function( arrow_col, value, shift_x, shift_y ){

				shift_x = shift_x || 0
				shift_y = shift_y || 0

				var arrow_color = arrow_col;


        if( value < this.min_val ){
          arrow_color = 'yellow';
        }else if( value > this.max_val ){
          arrow_color = 'red';
        }

				this.context.save();
				this.context.setTransform(1,0,0,1,0,0);


        this.context.fillStyle = arrow_color; //'#5ce68a';
        this.context.strokeStyle = 'black';
        this.context.globalAlpha = 0.65;
        this.context.lineWidth = 3;

        this.context.shadowColor = "#5D4D4E";
        this.context.shadowBlur = 5;
        this.context.shadowOffsetX = 5;
        this.context.shadowOffsetY = 5;

        angle = this.get_angle_for( Math.max(0, Math.min(7*this.scale,value) ) );

				this.context.scale( this.scale_x, this.scale_y );
        this.context.translate( WIDTH/2, HEIGHT/2 );
        this.context.rotate( angle ); 
        this.context.translate( ARROW_SHIFT_LEN + shift_x, ARROW_SHIFT_LEN + shift_y );
        this.context.beginPath();
        this.context.lineTo( ARROW_HEAD_LEN, ARROW_HEAD_LEN );
        this.context.lineTo( 0, ARROW_HEAD_LEN/2 );
        this.context.lineTo( ARROW_HEAD_LEN/2, 0 );
        this.context.closePath();
        this.context.fill();
        this.context.stroke();

				this.context.restore();
      }

			this.show_prev_arrow = function(){
				this.show_arrow( 'aqua', this.prev_value, shift_x = 10, shift_y = 10 );
			}

			//---------------------------------------------------
			// get_angle_for
			// 
			// goal: calculate angle for value from (min manometer-max manometer)
			//
      this.get_angle_for = function ( value ){
        return value*34.4*DEGREE/this.scale + 105*DEGREE;
      }
};
