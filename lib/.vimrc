version 6.0
if &cp | set nocp | endif
let s:cpo_save=&cpo
set cpo&vim
nmap gx <Plug>NetrwBrowseX
nnoremap <silent> <Plug>NetrwBrowseX :call netrw#NetrwBrowseX(expand("<cWORD>"),0)
nmap <F10> :SCCompileRun
nmap <F9> :SCCompile
nmap <F8> :TagbarToggle
let &cpo=s:cpo_save
unlet s:cpo_save
set autoindent
set autoread
set background=dark
set cindent
set expandtab
set fileencodings=ucs-bom,utf-8,default,latin1
set helplang=en
set laststatus=2
set nomodeline
set ruler
set shiftwidth=2
set smartindent
set softtabstop=2
set statusline=%{fugitive#statusline()}%c,%l/%L\ %P
set tabstop=2
" vim: set ft=vim :
