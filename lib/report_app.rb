# encoding: utf-8
require 'sinatra/base'
require 'ap'
require 'memoist'
require 'sinatra/contrib'
require_relative 'strict_tsv'
require_relative 'cpoint-data-receiver'
require_relative 'meter_receiver'

Encoding.default_external = Encoding::UTF_8

def decode_rus( value )
	return value
		$ecr1 ||= Encoding::Converter.new "UTF-8","CP1251",:invalid=>:replace,:undef=>:replace,:replace=>""
		$ecr2 ||= Encoding::Converter.new "CP1251","UTF-8",:invalid=>:replace,:undef=>:replace,:replace=>""

		$ecr1.convert $ecr2.convert value
end

class ReportApp < Sinatra::Base
	extend Memoist

	configure :development do
		register Sinatra::Reloader
	end

	disable :show_exceptions

#### CONSTANTS ##
#################

#PULSAR_WEB_SERVICE = 'http://192.168.10.11:3089'
PULSAR_WEB_SERVICE = 'http://46.36.145.7:3089' # deploy
PULSAR_METERS_JSON = '/last-values.json'

#### MNEMOSCHEMES ##
####################
	
#### REPORTS ####
#################
	
	#== PivZavod report
	get '/beerfac' do
		erb :beerfac_main
	end

	ERGOMERA_PROJ = 101
	PIVZAVOD_METER = 150160

	post '/beerfac' do
	begin
		@fm_address = PIVZAVOD_METER
		puts params
		case params["report"]
			when "daily" then #Daily report 
				@flowdata = get_flowmeter_daily_data( ERGOMERA_PROJ, PIVZAVOD_METER )
				@fm_caption = "Пив Завод"
				erb :flowmeter_daily_report

			when "monthly" then #"Monthly report"
				@flowdata = get_flowmeter_monthly_data( ERGOMERA_PROJ, PIVZAVOD_METER )
				@fm_caption = "Пив Завод"
				erb :flowmeter_monthly_report
			when "period" then "Period report"
		end
		rescue Exception => ex
			[500, ex.to_s + "\n" + $@.join("\n") ]
		end
	end
	
	#== Report for pulsar states (daily) ==#

	# Get params from user
	get '/pulsar/daily' do
		erb :pulsar_daily_report_form
	end

	#	Generate report by params: day,month,year
	post '/pulsar/daily' do
		@day = params['day']
		@month = params['month']
		@year = params['year']

		@last_pulsar_meters = read_pulsar_meters(date: "#{@day}.#{@month}.#{@year}")	

		if @last_pulsar_meters.nil? 
			[204, "No data found"] # 204 - no content
		else
			status 200
			erb :pulsar_daily_report
		end

	end

	# cellular daily report
	get '/cellular/daily' do
		erb :cellular_daily_report
	end

	post '/cellular/generate/daily' do
		puts "Cellular report generation:: #{params[:filename]}"
		file_name = ".\\tmp\\upload-#{params[:filename]}"
		File.open(file_name, 'w:UTF-8') do |f|
			#f.binmode
			ec1 = Encoding::Converter.new "UTF-8","UTF-16",:invalid=>:replace,:undef=>:replace,:replace=>""
			ec2 = Encoding::Converter.new "UTF-16","UTF-8",:invalid=>:replace,:undef=>:replace,:replace=>""
			while buffer = request.body.read(51200) # read in 50 kB chunks
				f << (ec1.convert ec2.convert buffer )
			end
		end

		tsv = StrictTsv.new(file_name)

		@lost_meters = []
		tsv.parse do |row|
			@lost_meters << row
		end
		@lost_meters.select!{|v| TimeDifference.between(DateTime.parse(v["Время считывания"]), DateTime.now).in_days > 2 } 

		[200,{'Content-Type' => 'text/html', 'Accept-Charset' => 'utf-8'}, erb( :lost_cell_meters)]
	end

	get '/cellular/range' do
		erb :cellular_period_report
	end

	post '/cellular/generate/range' do
	begin
		file_name = ".\\tmp\\upload-#{params[:filename]}"
		File.open(file_name, 'wb:UTF-16LE') do |f|
			f.binmode
			while buffer = request.body.read(51200) # read in 50 kB chunks
				@buf = buffer

				f << (@buf)
			end
		end

		text = File.open(file_name,"rb:UTF-16LE"){|file| file.readlines}.join.encode("utf-8")

		@empty_meters = []
		tsv = StrictTsv.new('')
		tsv.parse_str( text )do |row|
			@empty_meters << row
		end

		[200,{'Content-Type' => 'text/html', 'Accept-Charset' => 'utf-8'}, erb( :empty_cell_meters)]
		rescue Exception => ex
			puts "Error:: #{ex}"
			puts $@[0..6]
			$logger.error ex
			[500, {'Content-Type' => 'text/plain'}, "Error:: #{ex}"]
		end
	end

## UTILITIES ##
###############

	def read_pulsar_meters( args )
		params = ""
		ap args
		unless( args[:date].nil? )
			params = "?date=" + args[:date]
			ap "Query for #{params}"

		end
		JSON.parse( open( PULSAR_WEB_SERVICE + PULSAR_METERS_JSON + params, read_timeout: 300 ).read )["meters"]
	end
	memoize :read_pulsar_meters


PROJ_CPOINTS = 28

	def get_flowmeter_daily_data( proj, fm_code )
		@cfg ||= load_cfg
		@cp_receiver ||= CPointDataReceiver.new( @cfg )
		@cp_receiver.connect_to_meters_db()
		@cp_receiver.load_meters_consumption_ex( proj, MeterReceiver::PREVIOUS_DAY, MeterReceiver::ALL_VALS, DateTime.now - 30, DateTime.now + 1 )
	end

	def get_flowmeter_monthly_data( proj, fm_code )
		@cfg ||= load_cfg
		@cp_receiver ||= CPointDataReceiver.new( @cfg )
		@cp_receiver.connect_to_meters_db()
		@cp_receiver.load_meters_consumption_ex( proj, MeterReceiver::CURRENT_MONTH, MeterReceiver::ALL_VALS, DateTime.now - 30, DateTime.now )
	end
	# == Load the settings from yaml config
	#
	def load_cfg
		@cfg = YAML.load_file("#{File.dirname(__FILE__)}/cfg/ms.cfg.yaml")
	end
end
