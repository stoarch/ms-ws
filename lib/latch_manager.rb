# encoding: utf-8

require 'socket'

class LatchManager
		@hostname = '192.168.10.5'
		@port = 5202

	class << self

		def read_val( msg )
			socket = TCPSocket.open(@hostname, @port)

			socket.write msg

			recv = socket.recvfrom(4)

			socket.close

			aval = recv[0].bytes.to_a
			arev = aval.reverse

			hexv = arev.pack('C*').unpack('H*')

			fval = [hexv[0].to_i(16)].pack('L').unpack('F')[0]
		end

		def get_pressure
			read_val "pget\x0A"	
		end

		def get_pressure_lim
			read_val "psget\x0A"
		end
	end
end
