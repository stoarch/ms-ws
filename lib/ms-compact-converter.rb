# encoding: utf-8

#define control point codes
$control_points = [

	CP_KARASU_900 = 'cksu 900',
	CP_KARASU_1200 = 'cksu 1200',
	CP_KYZYLSAY_VHOD = 'cksvxod',
	CP_KYZYLSAY_POSLE_ZADV = 'ckspzd',
	CP_NURSAT = 'cnursat',
	CP_SAMAL2 = 'csamal2',
	CP_SAMAL3 = 'csamal3',
	CP_ALFARABI_POSLE_REG = 'calf psreg',
	CP_ALFARABI_DO_REG = 'calf doreg',
	CP_POJARKA = 'cpojarka',
	CP_SEVER = 'csever',
	CP_FOSFOR = 'cfosfor',
	CP_SPORTIV = 'csportiv',
	CP_ALFARABI = 'cplalph',
	CP_NEKRASOVA_216 = 'cnek 216',
	CP_LUMUMBA = 'clumumba',
	CP_KOROTKI = 'ckorotki',
	CP_KAZGYRT = 'ckazgyrt',
	CP_SAULE = 'csaule',
	CP_BSV_SAULE_VHOD = 'cbsv sa vx',
	CP_BSV_SAULE_VYHOD = 'cbsv sa vyh',
	CP_BSV_COLLECTOR = 'cbsv kol',
	CP_KRYG_NPZ = 'ckrygnpz',
	CP_TEC3 = 'ctec3',
	CP_DOSTYK = 'cdostyk',
	CP_TURLANOVKA = 'cturlanv',
	CP_TELMANA_POSLE_REG = 'ctelpre',
	CP_TELMANA_DO_REG = 'cteldre',
	CP_KYZYLSAY_VH = 'cksayvx'
]

# codes to points
$cp_codes = Hash[ 
	CP_KARASU_900, 123003,
	CP_KARASU_1200, 123001,
	CP_KYZYLSAY_VHOD, 100150,
	CP_KYZYLSAY_POSLE_ZADV, 100150,
	CP_NURSAT, 66666,
	CP_SAMAL2, 88888,
	CP_SAMAL3, 99990,
	CP_ALFARABI_POSLE_REG, 123007,
	CP_ALFARABI_DO_REG, 125001,
	CP_POJARKA, 100190,
	CP_SEVER, 400960,
	CP_FOSFOR, 9904014,
	CP_SPORTIV, 861922,
	CP_ALFARABI, 100180,
	CP_NEKRASOVA_216, 123006,
	CP_LUMUMBA, 9903999,
	CP_KOROTKI, 9902405,
	CP_KAZGYRT, 123008,
	CP_SAULE, 400970,
	CP_BSV_SAULE_VHOD, 123004,
	CP_BSV_SAULE_VYHOD, 123002,
	CP_BSV_COLLECTOR, 123005,
	CP_KRYG_NPZ, 100177,
	CP_TEC3, 55555,
	CP_DOSTYK, 44444,
	CP_TURLANOVKA, 77777, 
	CP_TELMANA_POSLE_REG, 123007,
	CP_TELMANA_DO_REG, 125001,
	CP_KYZYLSAY_VH, 100150 
]
$cp_codes.default = 0

$cpoint_dates = Hash[ 
	CP_FOSFOR , "c_fosfor_time",
	CP_SEVER , "c_sever_time",
	CP_SPORTIV , "c_sportiv_time",
	CP_TEC3 , "c_tec_time",
	CP_LUMUMBA , "c_lumumba_time",
	CP_KOROTKI , "c_korotki_time",
	CP_NURSAT , "c_nursat_time",
	CP_SAULE , "c_saule_time",
	CP_BSV_SAULE_VYHOD , "c_bsvsavyh_time",
	CP_BSV_SAULE_VHOD , "c_bsvsavx_time",
	CP_BSV_COLLECTOR , "c_bsvkol_time",
	CP_DOSTYK, "c_dostyk_time",
	CP_KARASU_1200 , "c_ksu1200_time",
	CP_KARASU_900 , "c_ksu900_time",
	CP_NEKRASOVA_216 , "c_nek_time",
	CP_KAZGYRT , "c_kazgyrt_time",
	CP_SAMAL2 , "c_samal2_time",
	CP_SAMAL3 , "c_samal3_time",
	CP_ALFARABI , "c_plalph_time",
	CP_POJARKA , "c_pojarka_time",
	CP_TELMANA_POSLE_REG , "c_telpre_time",
	CP_TELMANA_DO_REG , "c_teldre_time",
	CP_TURLANOVKA , "c_turlanv_time",
	CP_KRYG_NPZ , "c_kryg_time",
	CP_KYZYLSAY_POSLE_ZADV , "c_kspzd_time"
]
$cpoint_dates.default = 0

$cpoint_colors = Hash[ 
	CP_FOSFOR , "#f00001",
	CP_SEVER , "#f00003",
	CP_SPORTIV , "#f00004",
	CP_TEC3 , "#f00005",
	CP_LUMUMBA , "#f00006",
	CP_KOROTKI , "#f00007",
	CP_NURSAT , "#f00008",
	CP_SAULE , "#f00009",
	CP_BSV_SAULE_VYHOD , "#f0000A",
	CP_BSV_SAULE_VHOD , "#f0000B",
	CP_BSV_COLLECTOR , "#f0000C",
	CP_DOSTYK, "#f0000D",
	CP_KARASU_1200 , "#f0000E",
	CP_KARASU_900 , "#f0000F",
	CP_NEKRASOVA_216 , "#f00010",
	CP_KAZGYRT , "#f00011",
	CP_SAMAL2 , "#f00012",
	CP_SAMAL3 , "#f00013",
	CP_ALFARABI , "#f00014",
	CP_POJARKA , "#f00015",
	CP_TELMANA_POSLE_REG , "#f00016",
	CP_TELMANA_DO_REG , "#f00017",
	CP_TURLANOVKA , "#f00018",
	CP_KRYG_NPZ , "#f00019",
	CP_KYZYLSAY_POSLE_ZADV , "#f0001A"
]
$cpoint_colors.default = 0

$cp_mode_range = Hash[ 
	CP_FOSFOR , [2.0,2.9],
	CP_SEVER , [1.6,3.0],
	CP_SPORTIV , [2.0,2.5],
	CP_TEC3 , [0.8,2.0],
	CP_LUMUMBA , [2.0,2.5],
	CP_KOROTKI , [2.0,3.0],
	CP_NURSAT , [3.5,4.5],
	CP_SAULE , [0.1,1.5],
	CP_BSV_SAULE_VYHOD , [2.5,7.5],
	CP_BSV_SAULE_VHOD , [2.6,4.0],
	CP_BSV_COLLECTOR , [1.2,2.0],
	CP_DOSTYK, [2.2,3.5],
	CP_KARASU_1200 , [0.2,1.8],
	CP_KARASU_900 , [1.6,2.3],
	CP_NEKRASOVA_216 , [1.8, 2.5],
	CP_KAZGYRT , [0,8],
	CP_SAMAL2 , [2.5,3.0],
	CP_SAMAL3 , [3.0,3.9],
	CP_ALFARABI , [2.5,4.5],
	CP_POJARKA , [2.2,3.0],
	CP_TELMANA_POSLE_REG , [1.0,2.0],
	CP_TELMANA_DO_REG , [1.5,5.0],
	CP_TURLANOVKA , [2.5,3.5],
	CP_KRYG_NPZ , [1.4,3.4],
	CP_KYZYLSAY_POSLE_ZADV ,[0.5,3.0] 
]
$cp_mode_range.default = [0,8]


COLOR_LOW = '"#FFD42A"'
COLOR_HIGH = '"#FF0000"'
COLOR_NORMAL = '"#5FD38D"'

LOW = 0
HIGH = 1


#*************************************************#
# METERS
#*************************************************#

#define meter codes
$meters = [
	METER_KARASU_3N = 'karsu3',
	METER_KARASU_900 = 'karsu900',
	METER_KARASU_1200 = 'karsu1200',
	METER_KARASU_800 = 'karsu800',
	METER_KYZYL_TU_700 = 'ktu700',
	METER_KYZYL_TU_600 = 'ktu600',
	METER_KALININA_1200 = 'kal1200',
	METER_KALININA_900 = 'kal900',
	METER_TASSAY2 = 'tassay2',
	METER_KYZYL_SAY_3N = 'ksay3n',
	METER_KYZYL_SAY_500_LEV = 'ksay500l',
	METER_KYZYL_SAY_1000 = 'ksay1k',
	METER_KYZYL_SAY_500_PRAV = 'ksay500p',
	METER_KABISKO_800_PRAV = 'kab800p',
	METER_KABISKO_800_LEV = 'kab800l',
	METER_KABISKO_PAHTAKOR_800 = 'pah800',
	METER_PIVZAVOD_250_PRAV = 'piv250p',
	METER_PIVZAVOD_250_LEV = 'piv250l',
	METER_RESERVUAR_1000m3_400_PRAV = 'res1k400p',
	METER_RESERVUAR_1000m3_400_LEV = 'res1k400l',
	METER_ZABADAM_300 = 'zbd300',
	METER_TEHOHRANA_700 = 'tehoh700',
	METER_BSV_VHOD_1000 = 'bsvvh1k',
	METER_BSV_SAULE_300 = 'bsvsa300',
	METER_BSV_VYHOD_1000 = 'bsvvyh1k',
	METER_KYZYLTU_600 = 'ktu600',
	METER_NPZ = 'npz',
	METER_TEC3 = 'tec3',
	METER_TEC3_800 = 'tecd800',
	METER_TEC3_500_LEV = 'tecd500l',
	METER_TEC3_500_PRAV = 'tecd500p',
	METER_TEC3_400 = 'tecd400',
	METER_SEVER_1200 = 'sev1200',
	METER_OCISTNIE = 'ocistnie',
	METER_SIPHON1 = 'siphon1',
	METER_SIPHON2 = 'siphon2',
	METER_SIPHON3 = 'siphon3',
	METER_SIPHON4 = 'siphon4',
	METER_SIPHON5 = 'siphon5',
	METER_SIPHON6 = 'siphon6',
	METER_SIPHON7 = 'siphon7',
	METER_SIPHON8 = 'siphon8',
	METER_KBULAK = 'kbulak'
]

# codes to meters
$meter_codes = Hash[
	METER_KARASU_3N, 700650,
	METER_KARASU_900, 409441,
	METER_KARASU_1200, 409440,
	METER_KARASU_800, 700650,
	METER_KYZYL_TU_700, 0,
	METER_KYZYL_TU_600, 500560, 
	METER_KALININA_1200, 500500,
	METER_KALININA_900, 500501,
	METER_TASSAY2, 0,
	METER_KYZYL_SAY_3N, 700750,
	METER_KYZYL_SAY_500_LEV, 0,
	METER_KYZYL_SAY_500_PRAV, 0,
	METER_KABISKO_800_PRAV, 0,
	METER_KABISKO_800_LEV, 0,
	METER_KABISKO_PAHTAKOR_800, 0,
	METER_PIVZAVOD_250_LEV, 0,
	METER_PIVZAVOD_250_PRAV, 0,
	METER_RESERVUAR_1000m3_400_LEV, 0,
	METER_RESERVUAR_1000m3_400_PRAV, 0,
	METER_ZABADAM_300, 0,
	METER_TEHOHRANA_700, 0,
	METER_BSV_VHOD_1000, 0,
	METER_BSV_VYHOD_1000, 0,
	METER_BSV_SAULE_300, 0,
	METER_KYZYLTU_600, 500560, 
	METER_NPZ, 316750,
	METER_TEC3, 402624,
	METER_TEC3_800, 800753,
	METER_TEC3_500_LEV, 800750,
	METER_TEC3_500_PRAV, 800751,
	METER_TEC3_400, 0,
	METER_OCISTNIE, 0,
	METER_SEVER_1200, 0,
	METER_SIPHON1, 330750, 
	METER_SIPHON2, 340750, 
	METER_SIPHON3, 317750, 
	METER_SIPHON4, 318750, 
	METER_SIPHON5, 350750, 
	METER_SIPHON6, 360750, 
	METER_SIPHON7, 319750, 
	METER_SIPHON8, 320750,
	METER_KBULAK, 0
]
$meter_codes.default = 0


$meter_date_ranges = Hash[
	METER_KARASU_1200, ['ks12st', 'ks12et'],
	METER_KARASU_900, ['ks9st', 'ks9et'],
  METER_KARASU_800, ['ks8st', 'ks8et'],
	METER_TEC3, ['tecst', 'tecet'],
	METER_NPZ, ['npst', 'npet'],
	METER_KBULAK, ['kbulst', 'kbulet'],
	METER_TEC3_800, ['tec8st', 'tec8et'],
	METER_TEC3_500_LEV, ['tec5lst', 'tec5let'],
	METER_TEC3_500_PRAV, ['tec5pst', 'tec5pet'],
	METER_TEC3_400, ['tec4st', 'tec4et'],
	METER_SIPHON1, ['sip1st', 'sip1et'],
	METER_SIPHON2, ['sip2st', 'sip2et'],
	METER_SIPHON3, ['sip3st', 'sip3et'],
	METER_SIPHON4, ['sip4st', 'sip4et'],
	METER_SIPHON5, ['sip5st', 'sip5et'],
	METER_SIPHON6, ['sip6st', 'sip6et'],
	METER_SIPHON7, ['sip7st', 'sip7et'],
	METER_SIPHON8, ['sip8st', 'sip8et'],
	METER_BSV_VHOD_1000, ['bsvvhst', 'bsvvhet'],
	METER_BSV_SAULE_300, ['bsvs300st', 'bsvs300et'],
	METER_BSV_VYHOD_1000, ['bsvvyhst', 'bsvvyhet']
]
$meter_date_ranges.default = 0


# load csv file (param str 0)
source_file = ARGV[0]

puts "Converter from svg to erb\n"
puts "Loading file..."
source_string = File.open( source_file, 'r+:utf-8' ){|f| f.read }


# convert file
puts "\nConvert control points...\n"

$control_points.each do |cp|
	puts "\nConvert #{cp}..."
	source_string.gsub!( cp, "<%= val = get_cp_value( #{$cp_codes[cp]} ); val.round(1) if val %>" )
end

puts "\nConvert meters..."

$meters.each do |m|
	puts "\nConvert #{m}..."
	source_string.gsub!( m, "<%= val = get_flowmeter_value( #{$meter_codes[m]} ); val.round(0) if val%>" )
end

puts "\n Convert meter date ranges..."
$meter_date_ranges.each_pair do |m, dr|
	puts "\nConvert #{m} - #{dr}"
	source_string.gsub!(dr[0], "<%= val = get_flowmeter_range( #{$meter_codes[m]} ); v = val[:start].to_datetime.strftime('%H:%M') if val; v ||= '---'; v  %> ")
	source_string.gsub!(dr[1], "<%= val = get_flowmeter_range( #{$meter_codes[m]} ); v = val[:finish].to_datetime.strftime('%H:%M') if val; v ||= '---'; v %>")
end

puts "\n Convert dates for points..."
$cpoint_dates.each_pair do |cp, date_key|
	source_string.gsub!( date_key, "<%= val = get_cp_date( #{$cp_codes[cp]} ) || 0; val.strftime('%H:%M') if val.is_a?( Time ) %>" )
end


puts "\n Convert mode colors for points..."
$cpoint_colors.each_pair do |cp, color|
	source_string.gsub!( color, "<%= val = get_cp_value( #{$cp_codes[cp]} ) || 0; col = if val < #{$cp_mode_range[cp][LOW]}; #{COLOR_LOW}; elsif val > #{$cp_mode_range[cp][HIGH]}; #{COLOR_HIGH}; else ; #{COLOR_NORMAL}; end %>" )
end

# export erb file (param str 1)
puts "\n Export result file"

dest_file = ARGV[1]
File.open( dest_file, 'w+' ){|f| f.write( source_string ) }

puts "Done."
