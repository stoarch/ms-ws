class PulsarDataSaver

	attr :last_error
	attr :status
	attr_accessor :logger

	def initialize
		@last_error = ''
	end

	def store( json_data )
		@status = 'STORE PULSAR DATA:(.ok !err):'
	
		@db = connect_to_pulsar_db

		json_data.each do |v|
			save_item( v )
		end

		puts @status

		@db.disconnect if @db
		@db = nil
		true
	end


	def connect_to_pulsar_db
		Sequel.tinytds(
				user: 'site', password: 'Win3Ts', host: '192.168.10.8', database: 'pulsar', 
				encoding: 'cp1251'
			      )
	end

	def save_item( value )

		meter_id =  @db[:meters].where( plc_id: value['id'] ).where( prp_id: value['prp_id'] ).first
		 if meter_id.nil? 
				@status += '?'

				name = value['name'].encode('cp1251')
				name_group = value['name_group'].encode('cp1251')

				@logger.info "New meter: #{value}"
				@logger.info "Name: #{name.encode('utf-8')} Group: #{name_group.encode('utf-8')}"

				@db[:meters].insert( caption: name + ' (' + name_group + ')', plc_id: value['id'], prp_id: value['prp_id'] )

				meter_id =  @db[:meters].where( plc_id: value['id'] ).where( prp_id: value['prp_id'] ).first
		 else
				@status += '.'
		 end 

		 #@logger.ap meter_id

		@db[:values].insert( meter_id: meter_id[:id], date_query: value['date_query'], typ_arh: value['typ_arh'], value: value['value'].to_f )

	end

end
