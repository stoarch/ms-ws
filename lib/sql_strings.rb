 class SQLStrings 
    class << self
        def min_meter_values
          sql = <<-SQL
          select distinct _data_1.value, maxDateTable.dt
          from
                  (select address as myAddress, min(dateTime) as dt
                   from _data
                   where (dateTime <= ? and dateTime >= ?)
                   group by address
                  )as maxDateTable
          inner join _data as _data_1 on maxdateTable.myAddress = _data_1.address
                  and maxDateTable.dt = _data_1.DateTime
          inner join counters on _data_1.address = counters.sn
          where _data_1.address = ?
        SQL

        sql
    end

    def max_meter_values
          sql1 = <<-SQL
        select distinct _data_1.value, maxDateTable.dt
        from 
                ( select address as myaddress, max(datetime) as dt 
                        from _data
                        where (dateTime <= ? and DateTime >= ?)
                        group by address 
                ) as maxDateTable
        inner join _data as _data_1 on maxDateTable.myAddress = _Data_1.address 
                and maxDateTable.dt = _data_1.dateTime
        inner join counters on _data_1.address = counters.sn
        where _data_1.address = ?
      SQL

          sql1
        end
 end
end

