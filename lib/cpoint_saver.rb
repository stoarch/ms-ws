class CPointSaver
	
	def store_meter( meter, value, date )
		begin
			db = connect_to_cpoint_db()

			insert_ds = db['INSERT INTO counters.dbo._Data (address, value, cvalue, factor_id, datetime) values (?,?,?,6,?)', meter, value, value, date ]

			insert_ds.insert

			db.disconnect
		rescue
				ap $@[0..6]
				ap $!.message

				$logger.error $!
				$logger.info $@
		end
	end

	def connect_to_cpoint_db
		Sequel.tinytds(
			user: 'puser', password: 'Win3Ts', host: '192.168.10.8', database: 'tanks', encoding: 'cp1251' 
			)
	end
end
