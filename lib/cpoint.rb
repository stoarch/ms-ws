require_relative 'cpoint_value'

class CPoint
  attr :address, :caption

  attr :value

  def initialize( args )
    @address = args[:address]
    @caption = args[:caption]
    @value = CPointValue.new(self)
  end

  def value=(val)
     @value.datetime = val[:datetime]
     @value.value = val[:value]
  end

	def to_s
		"CPoint: #{address} #{caption}  #{value.to_s}"
	end
end
