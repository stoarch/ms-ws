require 'data_mapper'
require 'dm-sqlite-adapter'
require 'bcrypt'

DataMapper.setup(:default, "sqlite://#{File.expand_path(File.dirname(__FILE__))}/db/users.db")

class User
    include DataMapper::Resource
    include BCrypt

    property :id, Serial, key: true
    property :username, String, length: 3..50
    property :password, BCryptHash

    def authenticate(attempted_password)
         self.password == attempted_password
    end
end

DataMapper.finalize
DataMapper.auto_upgrade!
