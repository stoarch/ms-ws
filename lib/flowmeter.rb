require 'singleton'

require_relative "flowmeter_value"
require_relative "flowmeter_value_range"

class FlowMeter
  attr_accessor :caption, :address
	attr_accessor :point_id # control point for this meter

  attr :root_id
  attr :value
  attr :value_range
  attr :values
  attr :submeters

  attr_accessor :start_value
  attr_accessor :end_value
  attr_accessor :flowrate
	attr_accessor :near_point_value

  def initialize( args )
    @caption = args[:caption] || 'Unknown'
    @address = args[:address] || 'No address'
    @root_id = args[:root_id] || 'No root'
		@point_id = args[:point_id] || -1

    @value = FlowMeterValue.new(self)
    @value_range = FlowMeterValueRange.new(self)

    @values = []
    @submeters = []
  end

  def submeters=(value)
    @submeters = value
  end

  def value=(val)
     @value.datetime = val[:datetime]
     @value.value = val[:value]
  end

  def to_s
    "FlowMeter: #{address} - #{caption} - #{root_id} \n #{values.join(',')} near: #{near_point_value}"
  end

  def is_counter_stalling?
	  return true if is_invalid_values? 
	  return true if @values[1].value == @values[0].value && @values[1].value == 0

	  false
  end

  def is_invalid_values?
	  return true if @values.nil?
	  return true if @values.size < 2 # because we identify this states as stalling, so counter must be in one state: stallin/stopped/working
	  false
  end

  def is_counter_stoped?
  	return false if is_invalid_values?

	  return false if @values[1].value == 0 # this state also 'stalling'

	  @values[1].value == @values[0].value
  end

  def is_normal_state?
  	return false if is_invalid_values?
	return false if @values[1].value == 0
  	values_changed = (@values[1].value - @values[0].value > Float::EPSILON)
	last_val_in_hour = -(@values[1].datetime - DateTime.now ).round <= 1.5.hour #sec

	val =  -(@values[1].datetime - DateTime.now ).round 

	values_changed && last_val_in_hour
  end

  def has_daily_vals?
	  return false if @values.nil?
	  return false if @values.size < 2

	  #return false if (@values[1].datetime - @values[0].datetime).day > 1

	  @values[1].value - @values[0].value > Float::EPSILON 
  end
end

class NullFlowMeter < FlowMeter
  include Singleton

  def initialize
    @caption = "NullMeter"
    @root_id = -1
    @address = -1
    @value = NullFlowMeterValue.instance
    @value_range = NullFlowMeterValueRange.instance
    @values = []
    @submeters = []
  end

  # redefine each setting method for ignoring input - null object must have one value ever

  def submeters=(value)
  end

  def value=(val)
  end

  def start_value=(val)
  end

  def end_value=(val)
  end

  def flowrate=(val)
  end

  def has_daily_vals?
  	false
  end

  def is_counter_stalling?
	  false
  end

  def is_invalid_values?
		true 
  end

  def is_counter_stoped?
		true
  end

  def is_normal_state?
		false
  end

  def has_daily_vals?
		false
  end


	def method_missing(m, *args, &block)
		false
	end
end

# init singlet in normal order
NullFlowMeterValue.instance
NullFlowMeterValueRange.instance

NullFlowMeter.instance

