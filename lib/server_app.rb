require 'sinatra/base'
require 'sinatra/contrib'
require 'ap'
require 'json'
require 'time_difference'

class RingBuffer < Array
	attr_reader :max_size

	def initialize(max_size, enum = nil)
		@max_size = max_size
		enum.each{|e| self << e } if enum
	end

	def <<(el)
		if(self.size < @max_size || @max_size.nil?)
			super
		else 
			self.shift
			self.push(el)
		end
	end

	alias :push :<<
end

## MAIN SERVER APP
##################
class ServerApp < Sinatra::Base
    configure :development do
        register Sinatra::Reloader
    end

	attr_accessor :servers

	def initialize( app = nil )
		super(app)
		@servers = {}
	end


#### MNEMOSCHEMES ##
####################
	MAX_STATUS_BUF = 10

	get '/ms/status' do

		erb :server_status

	end

### UTILITIES
#############
	get '/status' do
		[200, @servers.to_json]
	end

	post '/status' do
		name = params["name"]
		status = params["status"]
		interval = params["interval"].to_i #seconds

		status_pack = {"code" => status, "time" => Time.now}

		@servers[name] ||= {"status" => RingBuffer.new( MAX_STATUS_BUF )}
		@servers[name]["status"] << status_pack 
		@servers[name]["last_status"] = status_pack 
		@servers[name]["interval"] = interval

		[200,"Stored"]
	end

#### REPORTS ####
#################

	
end
