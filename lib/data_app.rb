require 'sinatra/base'
require 'sinatra/contrib'
require 'ap'
require 'json'
require 'time_difference'

## MAIN DATA APP
##################
class DataApp < Sinatra::Base
    configure :development do
        register Sinatra::Reloader
    end

	attr_accessor :servers

	def initialize( app = nil )
		super(app)
		@servers = {}
	end


	# read all meters vals, specified by their codes and date range
	# params: :codes - comma separated array of meters
	#		:date_start - string of start date in format yyyy-mm-dd
	#		:date_end - same for end date
	#	returns:
	#		json in format [{"datetime":"2016-03-05T14:47:00+06:00", "value":123443.3, code:"344333"},...]
	#
	get '/meters' do
		puts params

	end

### UTILITIES
#############
	get '/status' do
		[200, @servers.to_json]
	end

	post '/status' do
		name = params["name"]
		status = params["status"]
		interval = params["interval"].to_i #seconds

		status_pack = {"code" => status, "time" => Time.now}

		@servers[name] ||= {"status" => RingBuffer.new( MAX_STATUS_BUF )}
		@servers[name]["status"] << status_pack 
		@servers[name]["last_status"] = status_pack 
		@servers[name]["interval"] = interval

		[200,"Stored"]
	end

#### REPORTS ####
#################

	
end
