class StrictTsv
	attr_reader :filepath
	def initialize(filepath)
		@filepath = filepath 
	end


	def parse_str(str) 
			headers = str.each_line.to_a[0].strip.split("\t").map{|s| decode_rus(s) }
			str.each_line do |line|
				fields = Hash[headers.zip(line.split("\t"))]
				yield fields
			end
	end

	def parse
		puts "Parse #{@filepath}"
		open(@filepath, "r:UTF-8") do |f|
			headers = f.gets.strip.split("\t").map{|s| decode_rus(s) }
			f.each do |line|
				fields = Hash[headers.zip(line.split("\t"))]
				yield fields
			end
		end
	end
end
