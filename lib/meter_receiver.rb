module MeterReceiver

    CURRENT_DAY = 0
    PREVIOUS_DAY = 1
    CURRENT_MONTH = 2
    PREVIOUS_MONTH = 3
    UNIVERSAL_PERIOD = 4

   ALL_VALS = 0
   HALFHOUR_VALS = 1
   HOUR_VALS = 2
   DAILY_VALS = 3

   # == Load root flowmeters from db
   #
   # All flowmeters with maincounter_id => nil is root meters, that can  represent
   #  tree of submeters.
	 #
	 # Params: 
	 #	1. db - to which need to connect,
	 #	2. proj_id - project to scan
	 #
	 # RetVal:
	 #	List of FlowMeter's from this db and project
   #
   def load_root_fm( db, proj_id  ) 

     fds = db[:paramaccounts]
        .join(:counters, :sn => :counter_id )
        .join(:clients, :client_id => :client_id)


     db_meters = fds.select(:sn, :city, :street, :house, :flat)
          .filter('clients.project_id = ?', proj_id)
          .where( :maincounter_id => nil )
          .all

     meters = []

     db_meters.each do |dbm|
       meters << make_meter_for( dbm )
     end

     meters.sort!{|a,b| a.address <=> b.address }
   end



   # == Parse representation of date period kind to date pair
   #
   def parse_period_kind( period_kind )
     cur_date = DateTime.now.to_date

     case period_kind
       when CURRENT_DAY
         date_start = cur_date
         date_end = cur_date + 1

       when PREVIOUS_DAY
         date_start = cur_date - 1
         date_end = cur_date

       when CURRENT_MONTH
         date_start = Date.new( cur_date.year, cur_date.month, 1 ).to_datetime
         date_end = Date.new( cur_date.year, cur_date.month, -1 ).to_datetime
         
       when PREVIOUS_MONTH
         date_start = Date.new( cur_date.year, cur_date.month - 1, 1 ).to_datetime
         date_end = Date.new( cur_date.year, cur_date.month - 1, -1 ).to_datetime #last day of month

       when UNIVERSAL_PERIOD
         date_start = self.date_start
         date_end = self.date_end + 1
     end

		 return date_start, date_end
   end 

	 DATETIME = 0
	 VALUE = 1
	 ADDRESS = 2


   # == Load the meter consumption for period
   #
   # Load the consumption for specified flowmeter. This contains hourly values
   # for daily report, or daily values for weekly and monthly report.
   #
   # @params:
   #   meters_id - array of ids of meter to load
   #   period_kind - is integer of (0: current day, 1: previous day, 2: current month, 3:previous month, 4: universal, that uses date_start..date_end)
   #   interval_kind - is integer of (0: all vals, 1: half hour values, 2: hourly values, 3: daily values)
   #
   #
   # @returns:
   #   array of FlowMeter with values in format: [FlowMeter(date_time, value, flow)], date_start, date_end
   #
   def load_meters_consumption( db, meter_ids, period_kind, interval_kind )
     #todo: Make hash with strategies of date generation
     #todo: Refactor this long method

    date_start, date_end = parse_period_kind( period_kind )

    vals = db[:_data].where( datetime: date_start.to_date..date_end.to_date )
        .filter( address: meter_ids )
        .select( :datetime, :value, :address )
        .order( :datetime )
        .all 

     # prepare data for report (take only needed values)
     result = case interval_kind
       when ALL_VALS
         vals.map{|v| [v[:datetime], v[:value], v[:address]]}

       when HALFHOUR_VALS
                vals.map{|v| [v[:datetime], v[:value], v[:address]]  if ((v[:datetime].min - 30).abs < 1)||(v[:datetime].min == 0)}
                                                                                 
       when HOUR_VALS
                vals.map{|v| [v[:datetime], v[:value], v[:address]] if (v[:datetime].min < 1)}
       when DAILY_VALS
                gvals = vals.group_by{|v| v[:datetime].to_date}
                gvals.map{|v| v[1][0].map{|r|r[1]}}  # [1][0] - array of hashes, r[1] - value for date              
     end.compact.uniq

     # make flowmeter with vals for array of vals
		 meters = {}
     result.each do |r| 
				meter = nil 
				if( meters[r[ADDRESS]] )
					meter = meters[r[ADDRESS]]
				else
				  meter =  FlowMeter.new( address: r[ADDRESS], caption: "<none>")
					meters[r[ADDRESS]] = meter
			  end

       meter.values << FlowMeterValue.make( :meter => meter, :value => r[VALUE], :datetime => r[DATETIME] )
     end

     return meters.to_a.map!{|v|v[1]}, date_start, date_end
   end


# == Load the dictionary of meters
#
#  Receive the array of hashes (for each row) of meter ids and captions
#
# Params:
#		1. db - from which to get values
#		2. proj_id - of meters to work with
#
# Returns:
#		List of hash values {sn: 1, city: 'Shymkent', street: 'Oki', house: '22', flat: 22}
#
   def get_meter_dict( db, proj_id )

     db[:counters]
       .join(:clients, :client_id => :client_id)
       .select(:sn, :city, :street, :house, :flat )
       .filter('clients.project_id = ?', proj_id)
       .all
   end


   # == Make meter from db meter hash
   #
   def make_meter_for( dbm )

     return NullFlowMeter.instance if dbm.nil?

     FlowMeter.new( 
                   address: dbm[:sn].to_i, 
                   caption: "#{dbm[:city]} \t- #{dbm[:street]} \t- #{dbm[:house]} \t- #{dbm[:flat]}".force_encoding("cp1251").encode("UTF-8"))   
   end   
end
