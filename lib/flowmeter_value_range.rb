require 'singleton'

class FlowMeterValueRange 
  attr_accessor :start, :final

  def initialize( meter )
    @start = FlowMeterValue.new(meter)
    @final = FlowMeterValue.new(meter)
  end
end

class NullFlowMeterValueRange < FlowMeterValueRange
  include Singleton

  def initialize
    @start = NullFlowMeterValue.instance
    @final = NullFlowMeterValue.instance
  end

  def start=(val)
  end

  def final=(val)
  end
end

# initialize singleton

NullFlowMeterValueRange.instance

