require 'thin'
require 'sinatra'
require 'sinatra-websocket'
require 'ap'
require 'logger'


$logger = Logger.new('akbay.log', 'daily')

set :server, 'thin'
set :sockets, []
set :port, 9012
set :public_dir, 'static'

get '/' do
	if !request.websocket?
		erb :akbay_ms
	else
		request.websocket do |ws|
			ws.onopen do
				ws.send "Connected"
				settings.sockets << ws
				ap "Connected client"
				$logger.info ws.inspect
			end

			ws.onmessage do |msg|
				EM.next_tick do 
					settings.sockets.each do |s|
						next if s == ws
						puts "Sent to #{s}"
						s.send(msg)
					end
				end

				puts "Message received: '" + msg + "' from " + ws.to_s
			end

			ws.onclose do
				warn("websocket closed")
				settings.sockets.delete(ws)
				ap "Closed socket"
			end
		end
	end
end
