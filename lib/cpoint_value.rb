class CPointValue
  attr :cpoint
  
  attr_accessor :value, :datetime

  def initialize( cp )
    @cpoint = cp
  end

	def to_s
		"CPointValue: #{value} #{datetime}"
	end
end
