# encoding: utf-8

#define control point codes
$control_points = [

	CP_KARASU_900 = 'cksu 900',
	CP_KARASU_1200 = 'cksu 1200',
	CP_KYZYLSAY_VHOD = 'cksvxod',
	CP_KYZYLSAY_POSLE_ZADV = 'ckspzd',
	CP_NURSAT = 'cnursat',
	CP_SAMAL2 = 'csamal2',
	CP_SAMAL3 = 'csamal3',
	CP_ALFARABI_POSLE_REG = 'calf psreg',
	CP_ALFARABI_DO_REG = 'calf doreg',
	CP_POJARKA = 'cpojarka',
	CP_SEVER = 'csever',
	CP_FOSFOR = 'cfosfor',
	CP_SPORTIV = 'csportiv',
	CP_ALFARABI = 'cplalph',
	CP_NEKRASOVA_216 = 'cnek 216',
	CP_LUMUMBA = 'clumumba',
	CP_KOROTKI = 'ckorotki',
	CP_KAZGYRT = 'ckazgyrt',
	CP_SAULE = 'csaule',
	CP_BSV_SAULE_VHOD = 'cbsv sa vx',
	CP_BSV_SAULE_VYHOD = 'cbsv sa vyh',
	CP_BSV_COLLECTOR = 'cbsv kol',
	CP_KRYG_NPZ = 'ckrygnpz',
	CP_TEC3 = 'ctec3',
	CP_DOSTYK = 'cdostyk',
	CP_TURLANOVKA = 'cturlanv'
]


#define meter codes
$meters = [
	METER_KARASU_3N = 'karsu3',
	METER_KARASU_900 = 'karsu900',
	METER_KARASU_1200 = 'karsu1200',
	METER_KYZYL_TU_700 = 'ktu700',
	METER_KYZYL_TU_600 = 'ktu600',
	METER_KALININA_1200 = 'kal1200',
	METER_KALININA_900 = 'kal900',
	METER_TASSAY2 = 'tassay2',
	METER_KYZYL_SAY_3N = 'ksay3n',
	METER_KYZYL_SAY_500_LEV = 'ksay500l',
	METER_KYZYL_SAY_1000 = 'ksay1k',
	METER_KYZYL_SAY_500_PRAV = 'ksay500p',
	METER_KABISKO_800_PRAV = 'kab800p',
	METER_KABISKO_800_LEV = 'kab800l',
	METER_KABISKO_PAHTAKOR_800 = 'pah800',
	METER_PIVZAVOD_250_PRAV = 'piv250p',
	METER_PIVZAVOD_250_LEV = 'piv250l',
	METER_RESERVUAR_1000m3_400_PRAV = 'res1k400p',
	METER_RESERVUAR_1000m3_400_LEV = 'res1k400l',
	METER_ZABADAM_300 = 'zbd300',
	METER_TEHOHRANA_700 = 'tehoh700',
	METER_BSV_VHOD_1000 = 'bsvvh1k',
	METER_BSV_SAULE_300 = 'bsvsa300',
	METER_BSV_VYHOD_1000 = 'bsvvyh1k',
	METER_KYZYLTU_600 = 'ktu600',
	METER_TEC3_800 = 'tec3d800',
	METER_TEC3_500_LEV = 'tec3d500l',
	METER_TEC3_500_PRAV = 'tec3d500p',
	METER_TEC3_400 = 'tec3d400',
	METER_SEVER_1200 = 'sev1200',
	METER_OCISTNIE = 'ocistnie'
]

# codes to points
$cp_codes = Hash[ 
	CP_KARASU_900, 123003,
	CP_KARASU_1200, 123001,
	CP_KYZYLSAY_VHOD, 100150,
	CP_KYZYLSAY_POSLE_ZADV, 100160,
	CP_NURSAT, 66666,
	CP_SAMAL2, 88888,
	CP_SAMAL3, 99990,
	CP_ALFARABI_POSLE_REG, 123007,
	CP_ALFARABI_DO_REG, 125001,
	CP_POJARKA, 100190,
	CP_SEVER, 400960,
	CP_FOSFOR, 9904014,
	CP_SPORTIV, 861922,
	CP_ALFARABI, 100180,
	CP_NEKRASOVA_216, 123006,
	CP_LUMUMBA, 9903999,
	CP_KOROTKI, 9902405,
	CP_KAZGYRT, 123008,
	CP_SAULE, 400970,
	CP_BSV_SAULE_VHOD, 123004,
  CP_BSV_SAULE_VYHOD, 123002,
	CP_BSV_COLLECTOR, 123005,
	CP_KRYG_NPZ, 100177,
	CP_TEC3, 55555,
	CP_DOSTYK, 0,
	CP_TURLANOVKA, 0
]
$cp_codes.default = 0




# codes to meters
$meter_codes = Hash[
	METER_KARASU_3N, 700650,
	METER_KARASU_900, 409441,
	METER_KARASU_1200, 409440,
	METER_KYZYL_TU_700, 0,
	METER_KYZYL_TU_600, 500560, 
	METER_KALININA_1200, 500500,
	METER_KALININA_900, 500501,
	METER_TASSAY2, 0,
	METER_KYZYL_SAY_3N, 700750,
	METER_KYZYL_SAY_500_LEV, 0,
	METER_KYZYL_SAY_500_PRAV, 0,
	METER_KABISKO_800_PRAV, 0,
	METER_KABISKO_800_LEV, 0,
	METER_KABISKO_PAHTAKOR_800, 0,
	METER_PIVZAVOD_250_LEV, 0,
	METER_PIVZAVOD_250_PRAV, 0,
	METER_RESERVUAR_1000m3_400_LEV, 0,
	METER_RESERVUAR_1000m3_400_PRAV, 0,
	METER_ZABADAM_300, 0,
	METER_TEHOHRANA_700, 0,
	METER_BSV_VHOD_1000, 0,
	METER_BSV_VYHOD_1000, 0,
	METER_BSV_SAULE_300, 0,
	METER_KYZYLTU_600, 500560, 
	METER_TEC3_800, 800753,
	METER_TEC3_500_LEV, 800750,
	METER_TEC3_500_PRAV, 800751,
	METER_TEC3_400, 0,
	METER_OCISTNIE, 0,
	METER_SEVER_1200, 0
]
$meter_codes.default = 0


# load csv file (param str 0)
source_file = ARGV[0]

puts "Converter from svg to erb\n"
puts "Loading file..."
source_string = File.open( source_file, 'r+:utf-8' ){|f| f.read }


# convert file
puts "\nConvert control points...\n"

$control_points.each do |cp|
	puts "\nConvert #{cp}..."
	source_string.gsub!( cp, "<%= get_cp_value( #{$cp_codes[cp]} ) %>" )
end

puts "\nConvert meters..."

$meters.each do |m|
	puts "\nConvert #{m}..."
	source_string.gsub!( m, "<%= get_flowmeter_value( #{$meter_codes[m]} ) %>" )
end

# export erb file (param str 1)
puts "\n Export result file"

dest_file = ARGV[1]
File.open( dest_file, 'w+' ){|f| f.write( source_string ) }

puts "Done."
