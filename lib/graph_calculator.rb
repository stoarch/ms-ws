#
# GraphCalculator
#
# goal: Calculate hourly vals and interpolate it as cubic for continuous series
#		and linear for broken series 
#
class GraphCalculator

	FAIL_VALUE = -1.0

	attr_accessor :input, :output


	def initialize
		@input = []
		@output = []
	end

	def execute()


		@input = @input.uniq{|cv| cv[:datetime]}.sort{|a,b| a[:datetime] <=> b[:datetime] }

		vals = []

		first_date = @input[0][:datetime]
		

		max_hour = @input.map{|i| i[:datetime].hour }.max

		1.upto max_hour do |h|

			found = @input.detect{|r| r[:datetime].hour == h }

			vals << found if found 		
			vals << { 
				datetime: DateTime.new( first_date.year, first_date.month, first_date.day, h, 0, 0 ).to_time,
				value: FAIL_VALUE 
			} unless found 
		end


		@input = vals.sort{|a,b| a[:datetime] <=> b[:datetime] }


		times = []
		values = []

		@input.each do |cv|

			times << cv[:datetime].to_f
			values << cv[:value]
		end

		table = Interpolator::Table.new( times, values )


		if @input.count{|v| v[:value] == FAIL_VALUE } > 0
			table.style = Interpolator::Table::LINEAR
			$logger.error @failed_intervals
		else
			table.style = Interpolator::Table::CUBIC
		end


		@interpolated = []

		curr = Time.at(times[0])
		@interpolated << { datetime: Time.at(times[0]), value: values[0]}

		until curr > Time.at( times[-2] ) do

			curr += 5.minutes.to_f

			@interpolated << { datetime: curr, value: table.read( curr.to_f ) }
		end
		@interpolated << { datetime: Time.at(times[-1]), value: values[-1] }

		@output = @interpolated
	end


	private

	def fill_failed_intervals

		 @failed_intervals = @input.each_cons(2).select do |s| 

			 (s[1][:datetime] - s[0][:datetime]).to_f > 1.2 * HOUR rescue false
		 end

		 @failed_intervals.each do |s|

			 1.upto( s[1][:datetime].hour - s[0][:datetime].hour - 1) do |h|

				 @input << { 
					 datetime: s[0][:datetime].to_datetime + h/HOUR_IN_DAY, 
					 value: 0.0 
				 }
			 end
		 end
	end
end
