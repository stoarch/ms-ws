require 'bundler/setup'

require 'nokogiri'
require 'sinatra'

def load_cfg
	YAML.load_file("#{File.dirname(__FILE__)}/cfg/ms.cfg.yaml")
end

@cfg = load_cfg

Bundler.require :default, :test

require 'rspec'
RSpec.configure do |config|
  config.before(:each) {  }
end

