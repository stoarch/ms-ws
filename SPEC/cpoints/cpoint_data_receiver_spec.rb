require 'flowmeter'
require 'sequel'
require 'cpoint-data-receiver.rb'

CODES = [9333_333, 9333_334, 9333_335, 9333_336, 9333_343, 9333_344, 9333_345, 9333_346, 9333_353, 9333_354, 9333_355] 

RESULT = {}
RESULT[9333333] = { value: 1848, datetime: "2014-09-11 13:53:00 +0600" }
RESULT[9333334] = { value: 1018, datetime: "2014-09-11 13:53:00 +0600" }
RESULT[9333335] = { value: 32, datetime: "2014-09-11 13:53:00 +0600" }
RESULT[9333336] = { value: 0, datetime: "2014-09-11 13:53:00 +0600" }

RESULT[9333343] = { value: 1950, datetime: "2014-09-11 13:53:00 +0600" }
RESULT[9333344] = { value: 0, datetime: "2014-09-11 13:53:00 +0600" }
RESULT[9333345] = { value: 24, datetime: "2014-09-11 13:53:00 +0600" }
RESULT[9333346] = { value: 0, datetime: "2014-09-11 13:53:00 +0600" }

RESULT[9333353] = { value: 1552, datetime: "2014-09-11 13:53:00 +0600" }
RESULT[9333354] = { value: 1006, datetime: "2014-09-11 13:53:00 +0600" }
RESULT[9333355] = { value: 43, datetime: "2014-09-11 13:53:00 +0600" }
RESULT[9333356] = { value: 29, datetime: "2014-09-11 13:53:00 +0600" }

describe CPointDataReceiver do

	subject { receiver }
	let( :receiver ) { CPointDataReceiver.new( @cfg ) }

	describe '.get_ergomera_vals' do
		it 'reads values validly' do
			vals = receiver.get_ergomera_vals

			date = DateTime.new( 2014, 9, 11, 13, 53, 00 ).strftime("%H:%M %d-%m-%Y")

			vals.should == { 
				:kabisko=>{
					:flowrate=>[
						{:code=>9333353, :value=>1552.0, :date=>date}, 
						{:code=>9333354, :value=>1006.0, :date=>date}
					], 
					:pressure=>[
						{:code=>9333355, :value=>0.43, :date=>date}, 
						{:code=>9333356, :value=>0.29, :date=>date}
					]
				}, 
				:karasu=>{
					:flowrate=>[
						{:code=>9333343, :value=>1950.0, :date=>date}, 
						{:code=>9333344, :value=>0.0, :date=>date} 
					], 
					:pressure=>[
						{:code=>9333345, :value=>0.24, :date=>date},
						{:code=>9333346, :value=>0.0, :date=>date}
					]
				}, 
				:kyzylsay=>{
					:flowrate=>[
						{:code=>9333333, :value=>1848.0, :date=>date}, 
						{:code=>9333334, :value=>1018.0, :date=>date}
					], 
					:pressure=>[
						{:code=>9333335, :value=>0.32, :date=>date}, 
						{:code=>9333336, :value=>0.0, :date=>date}
					]
				}
			}
		end
	end


	describe ".load_fm_value" do


		it "reads values properly" do

			@value = receiver.load_fm_value( 9333335, "demo" )

			@value.should be_kind_of( FlowMeter )

			@value.values.size.should == 1

			@value.address.should == 9333335
			@value.caption.should == 'demo'

			@value.values[0].value.should == 32
			@value.values[0].datetime.to_s.should == "2014-09-11 13:53:00 +0600"
		end

		it "run faster 0.1s" do
			
			start_time = Time.now

			@value = receiver.load_fm_value( 9333335, "demo" )

			end_time = Time.now

			( end_time - start_time ).should < 0.01 #sec
		end

		it "can load repeatedly faster 1s" do

			start_time = Time.now

			values = (9333333..9333356).to_a

			values.each do |v|
				value = receiver.load_fm_value( v, "demo" )
			end

			end_time = Time.now

			( end_time - start_time ).should < 1 #sec
		end

		it "can read all values properly" do


			CODES.each do |c|
				value = receiver.load_fm_value( c, "demo" )

				value.values.size.should == 1
				value.values[0].value.should == RESULT[c][:value]
				value.values[0].datetime.to_s.should == RESULT[c][:datetime]
			end
		end

	end
end
