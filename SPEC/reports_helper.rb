# encoding: utf-8

require File.dirname(__FILE__) + '/spec_helper'

require File.dirname(__FILE__) + '/../lib/msws-lv'

disable :run

require 'capybara'
require 'capybara/dsl'

Application.environment = :test 

Capybara.app = Application
Capybara.default_driver = :webkit

module RequestMacros

 def select_from_chosen(item_text, options)
    field = find_field(options[:from])
    option_value = page.evaluate_script("$(\"##{field[:id]} option:contains('#{item_text}')\").val()")
    page.execute_script("value = ['#{option_value}']\; if ($('##{field[:id]}').val()) {$.merge(value, $('##{field[:id]}').val())}")
    option_value = page.evaluate_script("value")
    page.execute_script("$('##{field[:id]}').val(#{option_value})")
    page.execute_script("$('##{field[:id]}').trigger('liszt:updated').trigger('change')")
  end 

  def select_by_id(id, options = {})
    field = options[:from]
    option_xpath = "//*[@id='#{field}']/option[@value='#{id}']"
    option_text = find(:xpath, option_xpath).text
    select option_text, :from => field
  end

  def select_by_name(name, options = {})
    select_from_chosen name, options
  end

  def select_date(date, options = {})
    field = options[:from]
    select date.year.to_s,   :from => "#{field}_1i"
    select_by_id date.month, :from => "#{field}_2i"
    select date.day.to_s,    :from => "#{field}_3i"  
  end
end



RSpec.configure do |config|
  config.include Capybara::DSL
  config.include RequestMacros
end


#todo: Make this more task specific

def make_report_for store

  visit "#{store[:report]}"

  second_option_xpath = "//*[@id='form_mainfm']/option[@value='#{store[:address]}']"
  second_option = find(:xpath, second_option_xpath ) #, match: :exact)

  second_option.select_option

  select store[:period_kind], with: 'form[period_kind]'

  find( :xpath, "//*[@id='#{store[:start_date_selectors][:year]}']/option[@value='#{store[:start_date].year.to_s}']").select_option 
  find( :xpath, "//*[@id='#{store[:start_date_selectors][:month]}']/option[@value='#{store[:start_date].month.to_s}']").select_option 
  find( :xpath, "//*[@id='#{store[:start_date_selectors][:day]}']/option[@value='#{store[:start_date].day.to_s}']").select_option

  
  find( :xpath, "//*[@id='#{store[:end_date_selectors][:year]}']/option[@value='#{store[:end_date].year.to_s}']").select_option 
  find( :xpath, "//*[@id='#{store[:end_date_selectors][:month]}']/option[@value='#{store[:end_date].month.to_s}']").select_option 
  find( :xpath, "//*[@id='#{store[:end_date_selectors][:day]}']/option[@value='#{store[:end_date].day.to_s}']").select_option

  click_link_or_button 'Создать отчёт'
end

def selector string
  find :css, string
end

Capybara.add_selector(:link) do
  xpath {|rel| ".//a[@rel='#{rel}']"}
end
